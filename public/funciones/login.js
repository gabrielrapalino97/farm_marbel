var iniciar = function () {

    var usuario = $("[name=nombreUsu]").val();
    var pass = $("[name = pass]").val();

    if (usuario != "" && pass != "") {
        $.ajax({
            url: BASE_URL + 'Login/inicio_sesion',
            type: 'POST',
            data: {
                usu: usuario,
                pass: pass
            },
            dataType: 'JSON',
            success: function (resp) {
                var retorno = resp.retorno;
                if (retorno) {
                    window.location.href = BASE_URL + '';
                } else {
                    if (resp.type === "danger") {

                        mostrarError(resp.msg);
                    } else {
                        mostrarOki(resp.msg);
                    }
                }
            }
        }); // fin ajax
    } else {
        generaNotificacion('Complete todos los campos', 'danger');
    }
}
$(document).ready(function () {
    $("[name = btnInciar]").click(function () {
        iniciar();
    });
    $("[name = pass]").keypress(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            iniciar();
            return false;
        };
    });
});