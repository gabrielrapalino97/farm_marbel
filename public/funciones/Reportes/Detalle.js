var cuerpoTablaProductosSugeridos = $("[name=cuerpoTablaProductosSugeridos]");


var reporteGeneral = function() {
    var fecha = $("[name=fechaReporte]").val();
    var modelFila =
        '<tr>' +
        '   <td>{5}</td>' +
        '   <td>{0}</td>' +
        '   <td>{1}</td>' +
        '   <td>{3}</td>' +
        '   <td>{4}</td>' +
        '   <td>{2}</td>' +
        '   <td>{6}</td>' +
        '</tr>';
    showLoading();
    $.ajax({
        url: BASE_URL + "Reportes/Detalle/reporteGeneral",
        type: 'POST',
        data: {
            fecha: fecha
        },
        dataType: 'JSON',
        success: function(r) {
            var data = r.data;
            if (data.length > 0) {
                cuerpoTablaProductosSugeridos.empty();
                for (var i = 0; i < data.length; i++) {
                    cuerpoTablaProductosSugeridos.append(modelFila.format(
                        data[i]['PRONOMBRE'], //0
                        data[i]['PRODETALLE'], //1
                        data[i]['CANTIDAVENDIDA'], //2
                        data[i]['LABNOMBRE'], //3
                        data[i]['BODNOMBRE'], //4
                        data[i]['VENCOD'], //5
                        data[i]['USUNOMBRE'], //6                       
                        i + 1 //7
                    ));
                }
            } else {
                cuerpoTablaProductosSugeridos.empty();
            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}

$(document).ready(function() {
    $("[name=btnCargarReporte]").click(function() {
        reporteGeneral();
    });
});