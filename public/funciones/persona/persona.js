//añadir persona------------------------------------------------------------------------------------------------

var cargarPersonas = function () {
	var cuerpo = $("[name = cuerpoTabla]");
	var listado = '<tr>' +
		'               <td>{7}</td>' +
		'               <td>{2}</td>' +
		'               <td>{1}</td>' +
		'               <td>{4}</td>' +
		'               <td>{3}</td>' +
		'               <td>{5}</td>' +
		'               <td>' +
		'<span name="btnEditar" title="Editar" style="cursor:pointer;" data-cod="{0}" data-nit="{1}" data-razon="{2}" data-correo="{3}" data-direc="{4}" data-tele="{5}" data-contac="{6}"  class="icon icon-pencil text-primary"> </span>' +
		'<span name="btnEliminar" data-cod="{0}" title="Eliminar" style="cursor:pointer;" data-cod="{0}" class="icon icon-bin2 text-danger"></span>' +
		'               </td>' +
		'            </tr>';
	$.ajax({
		url: BASE_URL + 'Persona/Persona/obtenerPersonas',
		dataType: 'JSON',
		success: function (resp) {
			var data = resp.data;
			if (data.length > 0) {
				cuerpo.empty();
				for (var i = 0; i < data.length; i++) {
					cuerpo.append(listado.format(
						data[i]['PERCOD'],//0
						data[i]['PERNIT'],//1
						data[i]['PERRAZON'],//2
						data[i]['PERCORREO'],//3
						data[i]['PERDIRECCION'],//4
						data[i]['PERTELEFONO'],//5
						data[i]['PERCONTACTO'],//6
						i + 1,//7
					));
				}
				cuerpo.find("[name=btnEliminar]").on("click", eliminarPersona);
				cuerpo.find("[name=btnEditar]").on("click", editarPersona);
			} else {
				mostrarOki('No se encontr&oacute; datos', 'danger');
			}
		}
	}); // fin ajax
}

//buscar-------------------------------------------------------------------------------------------------

function filtarTablaPersonas() {
	// Declare variables
	var input, filter, table, tr, td, i, txtValue;
	input = document.getElementById("busqueda");
	filter = input.value.toUpperCase();
	table = document.getElementById("tablaPersonas");
	tr = table.getElementsByTagName("tr");

	// Loop through all table rows, and hide those who don't match the search query
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[1];
		if (td) {
			txtValue = td.textContent || td.innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
}

$(document).ready(function () {
	cargarPersonas();
});

$("[name =  guardarPersona]").click(function () {
	var validate = $("[name = formularioNuevaPersona]").formValidation({
		returnData: true
	});
	if (validate.error === true) {
		mostrarError(validate.message);
	} else {
		var data = validate.data;
		borrarmodalPersona();
		$.ajax({
			url: BASE_URL + 'Persona/Persona/ingresarPersona',
			type: 'POST',
			data: {
				dataEv: data
			},
			dataType: 'JSON',
			success: function (resp) {
				var data = resp.data;
				if (data.type == "danger") {
					mostrarError(data.msg);
				} else {
					mostrarOki(data.msg);
					$("#modalPersona").modal("hide");
					cargarPersonas();
				}
			}
		}); // fin ajax
	}
});


$("#busqueda").keypress(function (event) {
	filtarTablaPersonas();
});

//editar-------------------------------------------------------------------------------------------------

var editarPersona = function () {
	var cod = $(this).data("cod");
	var nit = $(this).data("nit");
	var razon = $(this).data("razon");
	var correo = $(this).data("correo");
	var direc = $(this).data("direc");
	var tele = $(this).data("tele");
	var contac = $(this).data("contac");
	$("[name= cod]").val(cod);
	$("[name= nombreEditarPersona]").val(razon);
	$("[name= nitEditarPersona]").val(nit);
	$("[name= correoEditarPersona]").val(correo);
	$("[name= direccionEditarPersona]").val(direc);
	$("[name= telefonoEditarPersona]").val(tele);
	$("[name= contactoEditarPersona]").val(contac);
	$("#modalEditarPersona").modal('show');
}

$("[name=actualizarEditarPersona]").click(function () {
	var validate = $("[name = formularioEditarPersona]").formValidation({
		returnData: true
	});
	if (validate.error === true) {
		mostrarError(validate.message);
	} else {
		var data = validate.data;
		$.ajax({
			url: BASE_URL + 'Persona/Persona/actualizarPersona',
			type: 'POST',
			data: {

				dataEv: data
			},
			dataType: 'JSON',
			success: function (resp) {

				var data = resp.data;
				if (data.type == "danger") {
					mostrarError(data.msg);
				} else {
					mostrarOki(data.msg);
					$("#modalEditarPersona").modal("hide");
					borrarmodalEditPersona();
					cargarPersonas();
				}

			}
		}); // fin ajax
	}
});

//eliminar-------------------------------------------------------------------------------------------------

var eliminarPersona = function () {
	var idPersona = $(this).data("cod");
	cuepor = $("[name=cuerpoTabla]");
	bootbox.confirm({
		message: "&iquest;Est&aacute; seguro que desea eliminar el registro?",
		buttons: {
			confirm: {
				label: "Si",
				className: "btn-success"
			},
			cancel: {
				label: "No",
				className: "btn-danger"
			}
		},
		callback: function (result) {
			if (result === true) {
				$.ajax({
					// la URL para la petici&oacute;n
					url: BASE_URL + "Persona/Persona/eliminarPersona",
					type: "POST",
					data: {
						idPersona: idPersona
					},
					dataType: "JSON",
					success: function (resp) {
						if (resp.data.type = "success") {
							mostrarOki(resp.data.msg);
							cuepor.empty();
							cargarPersonas();
						} else {
							mostrarError(resp.msg);
						}

					}
				});
			};
		}
	});
}

var borrarmodalPersona = function () {
	$("[name=nombreRazon]").val('');
	$("[name=nit]").val('');
	$("[name=correo]").val('');
	$("[name=direccion]").val('');
	$("[name=telefono]").val('');
	$("[name=telefono]").val('');
}