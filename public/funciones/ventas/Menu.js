var compBodega = $("[name=sBodega]");
var compFacturasAll = $("[name=btnCargarFacturasAll]");
var compFacturasHoy = $("[name=btnCargarFacturasHoy]");
var compMisFacturas = $("[name=btnCargarMisFacturas]");
var cuerpoTablaProductos = $("[name=cuerpoTablaProductos]");
var cuerpoTablaProductosVenta = $("[name=cuerpoTablaProductosVenta]");
var cuerpoTablaResumenProductosVenta = $("[name=cuerpoTablaResumenProductosVenta]");

var cuerpoTablaVentas = $("[name=cuerpoTablaVentas]");

var modelFilaSeleccione = '<option selected="true" value="">Seleccione</option>';
var modelFilaSelect = '<option value="{0}">{1}</option>';

var crearVenta = function() {
    bootbox.confirm({
        title: "Confirmaci&oacute;n del Sistema",
        message: "&iquest;Est&aacute; seguro que desea crear la venta para hoy?",
        buttons: {
            confirm: {
                label: "Si",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result === true) {
                showLoading();
                $.ajax({
                    url: BASE_URL + "Ventas/Menu/crearVenta",
                    dataType: 'JSON',
                    success: function(r) {
                        if (r.data.success) {
                            mostrarOki("Se ha creado la venta");
                            cargarVentas(3);
                        } else {
                            mostrarError(r.data.msg);
                        }
                        hideLoading();
                    }
                }).fail(function() {
                    mostrarError('Error al procesar, por favor intente nuevamente');
                }).always(function() {

                });
            };
        }
    });
}

var cargarVentas = function(funcion) {
    var modelFila =
        '<tr style="background:#{6}">' +
        '   <td>{0}</td>' +
        '   <td>{2}</td>' +
        '   <td>{3}</td>' +
        '   <td>{4}</td>' +
        '   <td><span class="icon icon-file-text2 text-success" title="Reanudar Venta" style="text-decoration:none; cursor:pointer; font-size:17px;" name="reanudarVenta" data-cod="{0}" data-observacion="{1}" data-fecha="{2}" data-usunombre="{3}" data-estado="{4}"></span></td>' +
        '</tr>';
    showLoading();
    $.ajax({
        url: BASE_URL + "Ventas/Menu/cargarVentas",
        type: 'POST',
        data: {
            funcion: funcion
        },
        dataType: 'JSON',
        success: function(r) {
            var data = r.data;
            if (data.length > 0) {
                cuerpoTablaVentas.empty();
                for (var i = 0; i < data.length; i++) {
                    (data[i]['ESTADO'] == "NO FACTURADO") ? background = "FFE1E1": background = "BBF0B4";
                    cuerpoTablaVentas.append(modelFila.format(
                        data[i]['VENCOD'], //0
                        data[i]['VENOBSERVACION'], //1
                        data[i]['VENFECHA'], //2
                        data[i]['USUNOMBRE'], //3
                        data[i]['ESTADO'], //4
                        i + 1, //5
                        background //6
                    ));
                }
                cuerpoTablaVentas.find("[name=reanudarVenta]").on("click", modalVenta);
            } else {
                cuerpoTablaVentas.empty();
                if (funcion == 1) {
                    mostrarError("No se encontró facturas.");
                }
                if (funcion == 2) {
                    mostrarError("No se encontró facturas en el dia de hoy.");
                }
                if (funcion == 3) {
                    mostrarError("No se encontró facturas.");
                }

            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}

var modalVenta = function() {
    $("[name=fechaVenta]").val($(this).data(''));
    $("[name=ventaUsuario]").val($(this).data(''));
    $("[name=ventaTotal]").val($(this).data(''));
    $("[name=ventaEstado]").val($(this).data(''));
    $("[name=textObservacion]").val($(this).data(''));

    $("[name=modalCrearVenta]").modal("show");
    var estado = $(this).data("estado");
    if (estado == "FACTURADO") {
        $("[name=btnAgregarFactura]").css('display', 'none');
        $("[name=ventaCod]").val($(this).data("cod"));
        $("[name=fechaVenta]").val($(this).data("fecha"));
        $("[name=ventaUsuario]").val($(this).data("usunombre"));
        $("[name=ventaEstado]").val($(this).data("estado"));
        $("[name=textObservacion]").val($(this).data("observacion"));
        $("[name=ventaCodVentaProducto]").val($(this).data("cod"));
        cargarProductosVenta($(this).data("cod"));
    } else {
        $("[name=btnAgregarFactura]").css('display', '');
        $("[name=ventaCod]").val($(this).data("cod"));
        $("[name=fechaVenta]").val($(this).data("fecha"));
        $("[name=ventaUsuario]").val($(this).data("usunombre"));
        $("[name=ventaEstado]").val($(this).data("estado"));
        $("[name=textObservacion]").val($(this).data("observacion"));
        $("[name=ventaCodVentaProducto]").val($(this).data("cod"));
        cargarBodegas();
        cargarProductosVenta($(this).data("cod"));
    }
}

var cargarBodegas = function() {
    showLoading();
    $.ajax({
        url: BASE_URL + "Gestion/Productos/cargarBodegas",
        dataType: 'JSON',
        success: function(r) {
            var data = r.data;
            if (data.length > 0) {
                compBodega.empty();
                compBodega.append(modelFilaSeleccione);
                for (var i = 0; i < data.length; i++) {
                    compBodega.append(modelFilaSelect.format(
                        data[i]['BODID'],
                        data[i]['BODNOMBRE'],
                    ));
                }
            } else {
                mostrarError("No se encontró información de Bodegas");
                compBodega.empty();
            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}

var cargarProductosBodega = function() {
    var codBodega = compBodega.val();
    if (codBodega == "") {
        mostrarError("Por favor seleccione una Bodega.");
        cuerpoTablaProductos.empty();
        return;
    }
    var modelFilaProductos =
        '<tr>' +
        '   <td>{8}</td>' +
        '   <td>{1} - {3}</td>' +
        '   <td>{4}</td>' +
        '   <td>{5}</td>' +
        '   <td>{2}</td>' +
        '   <td>{6}</td>' +
        '   <td><span class="icon icon-redo2 text-success" title="Agregar a factura" style="text-decoration:none; cursor:pointer; font-size:17px;" name="agregarProducto" data-proinfo="{1} {3} - {2}" data-cod="{0}" data-nombre="{1}" data-nombrelab="{2}" data-prodetalle="{3}" data-procodigo="{4}" data-procantidad="{5}" data-proprecio="{6}" data-proiva="{7}"></span></td>' +
        '</tr>';
    showLoading();
    $.ajax({
        url: BASE_URL + "Ventas/Menu/cargarProductosBodega",
        type: 'POST',
        data: {
            codBodega: codBodega
        },
        dataType: 'JSON',
        success: function(r) {
            var data = r.data;
            if (data.length > 0) {
                cuerpoTablaProductos.empty();
                for (var i = 0; i < data.length; i++) {
                    cuerpoTablaProductos.append(modelFilaProductos.format(
                        data[i]['PROID'], //0
                        data[i]['PRONOMBRE'], //1
                        data[i]['LABNOMBRE'], //2
                        data[i]['PRODETALLE'], //3
                        data[i]['PROCODIGO'], //4
                        data[i]['PROCANTIDAD'], //5
                        data[i]['PROPRECIOVENTA'], //6    ((data[i]['PROIVA'] * data[i]['PROPRECIOVENTA']) / 100) + parseInt(data[i]['PROPRECIOVENTA'], 10), //6
                        data[i]['PROIVA'], //7
                        i + 1 //8
                    ));
                }
                cuerpoTablaProductos.find("[name=agregarProducto]").on("click", modalCantidad);
            } else {
                mostrarError("No se encontró información de Productos");
                cuerpoTablaProductos.empty();
            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}

var modalCantidad = function() {
    $("[name=proCod]").val($(this).data("cod"));
    $("[name=proinfo]").val($(this).data("proinfo"));
    $("#modalCantidad").modal("show");
    setTimeout(() => { $('#cantidadVenta').focus() }, 1000);


}

var agregarProducto = function() {
    var validate = $("[name = formularioAgregarProducto]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        if (parseInt(validate.data['cantidadVenta']) <= 0) {
            mostrarError("La cantidad no puede ser menor o igual a 0");
            return;
        }
        $.ajax({
            url: BASE_URL + "Ventas/Menu/agregarProducto",
            type: 'POST',
            data: {
                datos: validate.data
            },
            dataType: 'JSON',
            success: function(r) {
                if (r.data.success) {
                    mostrarOki(r.data.msg);
                    $("[name=modalCantidad]").modal("hide");
                    cargarProductosBodega();
                    cargarProductosVenta(validate.data['ventaCodVentaProducto']);
                    $("[name=cantidadVenta]").val('');
                    $("[name=inputBuscarProductoBodega]").val('');

                } else {
                    mostrarError(r.data.msg);
                }
                hideLoading();
            }
        }).fail(function() {
            mostrarError('Error al procesar, por favor intente nuevamente');
        }).always(function() {

        });
    }
}

var cargarProductosVenta = function(codVenta) {
    var modelFilaProductos =
        '<tr>' +
        '   <td>{6}</td>' +
        '   <td>{1}</td>' +
        '   <td>{2}</td>' +
        '   <td>{4}</td>' +
        '   <td>{3}</td>' +
        '   <td><span class="icon icon-bin text-danger" title="Eliminar" style="text-decoration:none; cursor:pointer; font-size:17px;" name="eliminarProducto" data-cod="{0}" data-cantidad="{2}" data-procod="{5}"></span></td>' +
        '</tr>';
    showLoading();
    $.ajax({
        url: BASE_URL + "Ventas/Menu/cargarProductosVenta",
        type: 'POST',
        data: {
            codVenta: codVenta
        },
        dataType: 'JSON',
        success: function(r) {
            var data = r.data.retorno;
            if (data.length > 0) {
                $("[name=ventaTotal]").val(r.data.totalVenta);
                cuerpoTablaProductosVenta.empty();
                for (var i = 0; i < data.length; i++) {
                    cuerpoTablaProductosVenta.append(modelFilaProductos.format(
                        data[i]['DETCOD'], //0
                        data[i]['PRONOMBRE'], //1
                        data[i]['DETCANTIDADVENTA'], //2
                        data[i]['DETTOTALVENTA'], //3
                        data[i]['PROPRECIOVENTA'], //4  ((data[i]['PROIVA'] * data[i]['PROPRECIOVENTA']) / 100) + parseInt(data[i]['PROPRECIOVENTA'], 10), //4
                        data[i]['PROID'], //5
                        i + 1 //6
                    ));
                }
                cuerpoTablaProductosVenta.find("[name=eliminarProducto]").on("click", eliminarProductosVenta);
            } else {
                cuerpoTablaProductosVenta.empty();
            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}

var eliminarProductosVenta = function() {
    var estado = $("[name=ventaEstado]").val();
    if (estado == "FACTURADO") {
        mostrarError("No se puede realizar ninguna acción en una venta facturada.");
        return;
    }
    var codVenta = $("[name=ventaCod]").val();
    var cantidad = $(this).data("cantidad");
    var detCod = $(this).data("cod");
    var proCod = $(this).data("procod");
    showLoading();
    $.ajax({
        url: BASE_URL + "Ventas/Menu/eliminarProductosVenta",
        type: 'POST',
        data: {
            cantidad: cantidad,
            detCod: detCod,
            proCod: proCod
        },
        dataType: 'JSON',
        success: function(r) {
            if (r.data.success) {
                mostrarOki(r.data.msg);
                cargarProductosBodega();
                cargarProductosVenta(codVenta);
            } else {
                mostrarError(r.data.msg);
            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}

var guardarVenta = function() {
    bootbox.confirm({
        title: "Confirmaci&oacute;n del Sistema",
        message: "&iquest;Est&aacute; seguro que desea guardar la venta de hoy?",
        buttons: {
            confirm: {
                label: "Si",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result === true) {
                var codVenta = $("[name=ventaCod]").val();
                var observacion = $("[name=textObservacion]").val();
                showLoading();
                $.ajax({
                    url: BASE_URL + "Ventas/Menu/guardarVenta",
                    type: 'POST',
                    data: {
                        codVenta: codVenta,
                        observacion: observacion
                    },
                    dataType: 'JSON',
                    success: function(r) {
                        if (r.data.success) {
                            mostrarOki(r.data.msg);
                            $("[name=modalCrearVenta]").modal("hide");
                            cargarVentas(3);
                        } else {
                            mostrarError(r.data.msg);
                        }
                        hideLoading();
                    }
                }).fail(function() {
                    mostrarError('Error al procesar, por favor intente nuevamente');
                }).always(function() {

                });
            };
        }
    });
}



var cargarResumenProductosVenta = function() {
    var modelFilaProductos =
        '<tr>' +
        '   <td>{6}</td>' +
        '   <td>{1}</td>' +
        '   <td>{2}</td>' +
        '   <td>{4}</td>' +
        '   <td>{3}</td>' +
        '</tr>';
    showLoading();
    $.ajax({
        url: BASE_URL + "Ventas/Menu/cargarResumenProductosVenta",
        type: 'GET',
        dataType: 'JSON',
        success: function(r) {
            var data = r.data.retorno;
            if (data.length > 0) {
                $("[name=resumenventaTotal]").val(r.data.totalVenta);
                cuerpoTablaResumenProductosVenta.empty();
                for (var i = 0; i < data.length; i++) {
                    cuerpoTablaResumenProductosVenta.append(modelFilaProductos.format(
                        data[i]['DETCOD'], //0
                        data[i]['PRONOMBRE'], //1
                        data[i]['DETCANTIDADVENTA'], //2
                        data[i]['DETTOTALVENTA'], //3
                        data[i]['PROPRECIOVENTA'], //4
                        data[i]['PROID'], //5
                        i + 1 //6
                    ));
                }
            } else {
                cuerpoTablaResumenProductosVenta.empty();
            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}


var resumenVenta = function() {
    $("[name=modalResumenVenta]").modal("show");
    $("[name=resumenfechaVenta]").val($(this).data(''));
    $("[name=resumenventaUsuario]").val($(this).data(''));
    $("[name=resumenventaTotal]").val($(this).data(''));
    $("[name=resumenventaEstado]").val($(this).data(''));
    $("[name=resumentextObservacion]").val($(this).data(''));
    cargarResumenProductosVenta();
}

$(document).ready(function() {

    $("[name=btnCrearVenta]").click(function() {
        crearVenta();
    });

    $("[name=resumen]").click(function() {
        resumenVenta();
    });

    compBodega.change(function() {
        cargarProductosBodega();
    });

    $("[name=btnAgregarProducto]").click(function() {
        agregarProducto();
    });

    $("[name=btnAgregarFactura]").click(function() {
        guardarVenta();
    });

    $("[name=btnCargarFacturasAll]").click(function() {
        cargarVentas(1);
    });

    $("[name=btnCargarFacturasHoy]").click(function() {
        cargarVentas(2);
    });

    $("[name=btnCargarMisFacturas]").click(function() {
        cargarVentas(3);
    });

    $('#cantidadVenta').keypress(function(e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
            agregarProducto();
            e.preventDefault();
            return false;
        }
    });

    cargarVentas(2);

});

function pulsar(e) {
    if (e.keyCode === 13 && !e.shiftKey) {
        e.preventDefault();
        var codigo = document.getElementById("inputBuscarProductoBodega").value
        var codBodega = compBodega.val();

        if (codigo == "" || codBodega == "") {
            mostrarError("Seleccionar una Bodega y un Producto")
        } else {
            showLoading();
            $.ajax({
                url: BASE_URL + "Ventas/Menu/consultarCodPro",
                type: 'POST',
                data: {
                    codigo: codigo,
                    codBodega: codBodega
                },
                dataType: 'JSON',
                success: function(r) {
                    var data = r.data;
                    if (data.length == 1) {
                        $("[name=proCod]").val(data[0]['PROID']);
                        $("[name=proinfo]").val(data[0]['INFO']);
                        $("[name=modalCantidad]").modal("show");
                        setTimeout(() => { $('#cantidadVenta').focus() }, 1000);
                    } else {
                        mostrarError("Error en la seleccion del producto");
                    }
                    hideLoading();
                }
            })

        }
    }
}