compRol = $("[name=rol]");
modelFilaSeleccione = '<option selected="true" value="">Seleccione</option>';
modelFilaSelect = '<option value="{0}">{1}</option>';
cuerpoRoles = $("[name=cuerpoRoles]");
cuerpoPermisosRolSeleccionar = $("[name=cuerpoPermisosRolSeleccionar]");

var cargarPermisosRol = function() {
    var fillaPermisos = '<tr class="clickable-row">' +
        '    <td>' +
        '     <span class="icon icon-bin text-danger" name="eliminarPermisoRol" data-cod="{2}" style="font-size:20px; cursor:pointer;" title="Inhabilitar"' +
        '     name=""></span>' +
        '    </td>' +
        '    <td style="margin-top:5px;"><center>{1}</center></td>' +
        '</tr>';
    $(this).addClass('active').siblings().removeClass('active');
};

var restablacerClave = function() {
    cod = $(this).data("cod");
    showLoading('Cargando');
    $.ajax({
        url: BASE_URL + "Usuarios/Usuarios/restablacerClave",
        type: 'POST',
        data: {
            cod: cod
        },
        dataType: 'JSON',
        success: function(r) {
            if (r.data.success) {
                mostrarOki(r.data.msg);
            } else {
                mostrarError(r.data.msg);
            }
            hideLoading();
        }
    }).fail(function() {
        hideLoading();
        mostrarError('Error al procesar, por favor intente nuevamente');
    })
}

var inhabilitarClave = function() {
    cod = $(this).data("cod");
    showLoading('Cargando');
    $.ajax({
        url: BASE_URL + "Usuarios/Usuarios/inhabilitarClave",
        type: 'POST',
        data: {
            cod: cod
        },
        dataType: 'JSON',
        success: function(r) {
            if (r.data.success) {
                mostrarOki(r.data.msg);
            } else {
                mostrarError(r.data.msg);
            }
            hideLoading();
        }
    }).fail(function() {
        hideLoading();
        mostrarError('Error al procesar, por favor intente nuevamente');
    })
}

var buscarUsuario = function() {
    var filaUsuario = '<tr class="clickable-row filausuario" style="background:#d1d1d1;">' +
        '    <td>' +
        '     <span class="icon icon-cross text-danger" data-cod="{0}" name="iClave" style="font-size:20px; cursor:pointer;" title="Inhabilitar"' +
        '     name=""></span>' +
        '     <span class="icon icon-loop2 text-primary" data-cod="{0}" name="rClave" style="font-size:20px; cursor:pointer;" title="Restablecer clave"' +
        '     name=""></span>' +
        '    </td>' +
        '    <td style="margin-top:5px;"><center>{3}-{1} {2}</center></td>' +
        '</tr>';
    var validate = $("[name = formularioBuscarUsuario]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        showLoading();
        $.ajax({
            url: BASE_URL + "Usuarios/Usuarios/buscarUsuario",
            type: 'POST',
            data: {
                datos: validate.data
            },
            dataType: 'JSON',
            success: function(r) {
                var data = r.data;
                $("[name=cuerpoUsuario]").empty();
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        $("[name=cuerpoUsuario]").append(filaUsuario.format(
                            data[i]['USUCOD'],
                            data[i]['USUNOMBRE'],
                            data[i]['USUAPELLIDO'],
                            data[i]['USUCEDUlA']
                        ));
                    }
                    $("[name=cuerpoUsuario]").find("[name=rClave]").on('click', restablacerClave);
                    $("[name=cuerpoUsuario]").find("[name=iClave]").on('click', inhabilitarClave);
                } else {
                    mostrarError("No se encontr&oacute; informaci&oacute;n");
                    $("[name=cuerpoUsuario]").empty();
                }
                hideLoading();
            }
        }).fail(function() {
            hideLoading();
            mostrarError('Error al procesar, por favor intente nuevamente');
        })
    }
}

var cargarRol = function() {
    var filaRol = '<tr class="clickable-row fila" data-cod="{0}">' +
        '    <td>' +
        '    </td>' +
        '    <td style="margin-top:5px;"><center>{1}</center></td>' +
        '</tr>';
    $.ajax({
        url: BASE_URL + "Usuarios/Usuarios/cargarRol",
        dataType: 'JSON',
        success: function(r) {
            var data = r.data;
            $("[name=cuerpoRoles]").empty();
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    $("[name=cuerpoRoles]").append(filaRol.format(
                        data[i]['ROLCOD'],
                        data[i]['ROLDETALLE']
                    ));
                }
                $("[name=cuerpoRoles]").find('.fila').on('click', cargarPermisosRol);
            } else {
                mostrarError("No se encontr&oacute; informaci&oacute;n de Roles");
                $("[name=cuerpoRoles]").empty();
            }
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {});
}


$(document).ready(function() {
    cargarRol();

    $("[name=btnModal]").click(function() {
        $.ajax({
            url: BASE_URL + "Usuarios/Usuarios/cargarRol",
            dataType: 'JSON',
            success: function(r) {
                var data = r.data;
                if (data.length > 0) {
                    compRol.empty();
                    compRol.append(modelFilaSeleccione);
                    for (var i = 0; i < data.length; i++) {
                        compRol.append(modelFilaSelect.format(
                            data[i]['ROLCOD'],
                            data[i]['ROLDETALLE']
                        ));
                    }
                } else {
                    mostrarError("No se encontr&oacute; informaci&oacute;n de Roles");
                    compRol.empty();
                }
            }
        }).fail(function() {
            mostrarError('Error al procesar, por favor intente nuevamente');
        }).always(function() {});
    });

    $("[name=btnGuardarUsuario]").click(function() {
        var validate = $("[name = formularioAgregarUsuario]").formValidation({
            returnData: true
        });
        if (validate.error === true) {
            mostrarError(validate.message);
        } else {
            showLoading();
            $.ajax({
                url: BASE_URL + "Usuarios/Usuarios/insertarUsuario",
                type: 'POST',
                data: {
                    datos: validate.data
                },
                dataType: 'JSON',
                success: function(r) {
                    if (r.data.success) {
                        mostrarOki(r.data.msg);
                        $("[name=modalAgregarUsuario]").modal('hide');
                    } else {
                        mostrarError(r.data.msg);
                    }
                    hideLoading();
                }
            }).fail(function() {
                hideLoading();
                mostrarError('Error al procesar, por favor intente nuevamente');
            }).always(function() {});
        }
    });

    $("[name=btnBuscarUsuario]").click(function() {
        buscarUsuario();
    });

    $("#cedula").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            buscarUsuario();
            return false;
        }
    });
});