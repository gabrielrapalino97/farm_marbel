$(document).ready(function () {
    $("[name=miCuenta]").click(function () {
        $.ajax({
            url: BASE_URL + "Usuarios/Usuarios/cargarDatosUsuarios",
            dataType: 'JSON',
            success: function (r) {
                var data = r.data;
                if (data.length > 0) {
                    $("[name=formularioMiCuenta]").find("[name=nombre]").val(data[0]['USUNOMBRE']);
                    $("[name=formularioMiCuenta]").find("[name=apellido]").val(data[0]['USUAPELLIDO']);
					$("[name=formularioMiCuenta]").find("[name=cedula]").val(data[0]['USUCEDUlA']);
					$("[name=formularioMiCuenta]").find("[name=cedu]").val(data[0]['USUCEDUlA']);
                    $("[name=formularioMiCuenta]").find("[name=email]").val(data[0]['USUEMAIL']);
                    $("[name=formularioMiCuenta]").find("[name=telefono]").val(data[0]['USUTELEFONO']);
                    $("[name=formularioMiCuenta]").find("[name=cod]").val(data[0]['USUCOD']);
                } else {
                    mostrarError("No se encontr&oacute; informaci&oacute;n");
                    compRol.empty();
                }
            }
        }).fail(function () {
            mostrarError('Error al procesar, por favor intente nuevamente');
        }).always(function () {
        });
    });
    //formularioMiCuenta
    $("[name=btnActualizarUsuario]").click(function () {
        var validate = $("[name = formularioMiCuenta]").formValidation({
            returnData: true
        });
        if (validate.error === true) {
            mostrarError(validate.message);
        } else {
            showLoading('Cargando');
            $.ajax({
                url: BASE_URL + "Usuarios/Usuarios/actualizarUsuario",
                type: 'POST',
                data: {
                    datos: validate.data
                },
                dataType: 'JSON',
                success: function (r) {
                    if (r.data.success) {
                        $("[name=modalAgregarUsuario]").modal('hide');
                        mostrarOki('Por favor, ingrese nuevamente, su información ha sido actualizada.');
                        setTimeout(function () { window.location = BASE_URL + 'Login/logout'; }, 2000);
                    } else {
                        mostrarError(r.data.msg);
                    }
                    hideLoading();

                }
            }).fail(function () {
                mostrarError('Error al procesar, por favor intente nuevamente');
            }).always(function () {

            });
        }
    });

    $("[name=btnCambiar]").click(function () {
        var validate = $("[name = formularioCambioClave]").formValidation({
            returnData: true
        });
        if (validate.error === true) {
            mostrarError(validate.message);
        } else {
            showLoading('Cargando');
            $.ajax({
                url: BASE_URL + "Usuarios/Usuarios/cambiarClave",
                type: 'POST',
                data: {
                    datos: validate.data
                },
                dataType: 'JSON',
                success: function (r) {
                    if (r.data.success) {
                        $("[name=modalCambioClave]").modal('hide');
                        mostrarOki('Por favor, ingrese nuevamente, su clave ha sido actualizada.');
                        setTimeout(function () { window.location = BASE_URL + 'Login/logout'; }, 2000);
                    } else {
                        mostrarError(r.data.msg);
                    }
                    hideLoading();

                }
            }).fail(function () {
                mostrarError('Error al procesar, por favor intente nuevamente');
            }).always(function () {

            });
        }
    });
});
