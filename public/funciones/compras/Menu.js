var compBodega = $("[name=sBodega]");
var compFacturasAll = $("[name=btnCargarFacturasAll]");
var compFacturasHoy = $("[name=btnCargarFacturasHoy]");
var compMisFacturas = $("[name=btnCargarMisFacturas]");
var cuerpoTablaProductos = $("[name=cuerpoTablaProductos]");
var cuerpoTablaProductosVenta = $("[name=cuerpoTablaProductosVenta]");
var cuerpoTablaVentas = $("[name=cuerpoTablaVentas]");

var modelFilaSeleccione = '<option selected="true" value="">Seleccione</option>';
var modelFilaSelect = '<option value="{0}">{1}</option>';

var crearVenta = function() {
    bootbox.confirm({
        title: "Confirmaci&oacute;n del Sistema",
        message: "Solicitud para iniciar una entrada",
        buttons: {
            confirm: {
                label: "Si",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result === true) {
                showLoading();
                $.ajax({
                    url: BASE_URL + "Compras/Menu/crearVenta",
                    dataType: 'JSON',
                    success: function(r) {
                        if (r.data.success) {
                            mostrarOki("Se ha creado la venta");
                            cargarVentas(3);
                        } else {
                            mostrarError(r.data.msg);
                        }
                        hideLoading();
                    }
                }).fail(function() {
                    mostrarError('Error al procesar, por favor intente nuevamente');
                }).always(function() {

                });
            };
        }
    });
}

var cargarVentas = function(funcion) {
    var modelFila =
        '<tr style="background:#{7}">' +
        '   <td>{6}</td>' +
        '   <td>{2}</td>' +
        '   <td>{3}</td>' +
        '   <td>{5}</td>' +
        '   <td>{4}</td>' +
        '   <td><span class="icon icon-file-text2 text-success" title="Reanudar Venta" style="text-decoration:none; cursor:pointer; font-size:17px;" name="reanudarVenta" data-cod="{0}" data-observacion="{1}" data-fecha="{2}" data-usunombre="{3}" data-estado="{4}" data-proveedor="{5}"></span></td>' +
        '</tr>';
    $.ajax({
        url: BASE_URL + "Compras/Menu/cargarVentas",
        type: 'POST',
        data: {
            funcion: funcion
        },
        dataType: 'JSON',
        success: function(r) {
            var data = r.data;
            if (data.length > 0) {
                cuerpoTablaVentas.empty();
                for (var i = 0; i < data.length; i++) {
                    (data[i]['ESTADO'] == "NO FACTURADO") ? background = "FFE1E1": background = "BBF0B4";
                    cuerpoTablaVentas.append(modelFila.format(
                        data[i]['COMCOD'], //0
                        data[i]['COMOBSERVACION'], //1
                        data[i]['COMFECHA'], //2
                        data[i]['USUNOMBRE'], //3
                        data[i]['ESTADO'], //4
                        data[i]['COMPROVEEDOR'], //5
                        i + 1, //6
                        background //7
                    ));
                }
                cuerpoTablaVentas.find("[name=reanudarVenta]").on("click", modalVenta);
            } else {
                cuerpoTablaVentas.empty();
                if (funcion == 1) {
                    mostrarError("No se encontró facturas.");
                }
                if (funcion == 2) {
                    mostrarError("No se encontró facturas en el dia de hoy.");
                }
                if (funcion == 3) {
                    mostrarError("No se encontró facturas.");
                }

            }
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}

var modalVenta = function() {
    $("[name=fechaVenta]").val($(this).data(''));
    $("[name=ventaUsuario]").val($(this).data(''));
    $("[name=ventaTotal]").val($(this).data(''));
    $("[name=ventaEstado]").val($(this).data(''));
    $("[name=venproveedor]").val($(this).data(''));
    $("[name=textObservacion]").val($(this).data(''));

    $("[name=modalCrearVenta]").modal("show");
    var estado = $(this).data("estado");
    codVenta = $(this).data("cod");
    if (estado == "FACTURADO") {
        $("[name=btnAgregarFactura]").css('display', 'none');
        $("[name=ventaCod]").val($(this).data("cod"));
        $("[name=fechaVenta]").val($(this).data("fecha"));
        $("[name=ventaUsuario]").val($(this).data("usunombre"));
        $("[name=ventaEstado]").val($(this).data("estado"));
        $("[name=textObservacion]").val($(this).data("observacion"));
        $("[name=entradaCod]").val($(this).data("cod"));
        $("[name=venproveedor]").val($(this).data("proveedor"));
        cargarProductosVenta(codVenta);
    } else {
        $("[name=btnAgregarFactura]").css('display', '');
        $("[name=ventaCod]").val($(this).data("cod"));
        $("[name=fechaVenta]").val($(this).data("fecha"));
        $("[name=ventaUsuario]").val($(this).data("usunombre"));
        $("[name=ventaEstado]").val($(this).data("estado"));
        $("[name=textObservacion]").val($(this).data("observacion"));
        $("[name=entradaCod]").val($(this).data("cod"));
        cargarBodegas();
        cargarProductosVenta(codVenta);
    }
}

var cargarBodegas = function() {
    showLoading();
    $.ajax({
        url: BASE_URL + "Gestion/Productos/cargarBodegas",
        dataType: 'JSON',
        success: function(r) {
            var data = r.data;
            if (data.length > 0) {
                compBodega.empty();
                compBodega.append(modelFilaSeleccione);
                for (var i = 0; i < data.length; i++) {
                    compBodega.append(modelFilaSelect.format(
                        data[i]['BODID'],
                        data[i]['BODNOMBRE'],
                    ));
                }
            } else {
                mostrarError("No se encontró información de Bodegas");
                compBodega.empty();
            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}

var cargarProductosBodega = function() {
    var codBodega = compBodega.val();
    if (codBodega == "") {
        mostrarError("Por favor seleccione una Bodega.");
        cuerpoTablaProductos.empty();
        return;
    }
    var modelFilaProductos =
        '<tr>' +
        '   <td>{1} {4} {7}</td>' +
        '   <td>{2}</td>' +
        '   <td>{5}</td>' +
        '   <td>{6}</td>' +
        '   <td>{9}</td>' +
        '   <td><span class="icon icon-redo2 text-success" title="Agregar a factura" style="text-decoration:none; cursor:pointer; font-size:17px;" name="agregarProducto" data-detalle="{1} {3} - {2}" data-cod="{0}" data-nombre="{1}" data-nombrelab="{2}" data-prodetalle="{3}" data-procodigo="{4}" data-procantidad="{5}" data-proprecio="{6}" data-procompra="{8}" data-proiva="{10}"></span></td>' +
        '</tr>';
    showLoading();
    $.ajax({
        url: BASE_URL + "Compras/Menu/cargarProductosBodega",
        type: 'POST',
        data: {
            codBodega: codBodega
        },
        dataType: 'JSON',
        success: function(r) {
            var data = r.data;
            if (data.length > 0) {
                cuerpoTablaProductos.empty();
                for (var i = 0; i < data.length; i++) {
                    cuerpoTablaProductos.append(modelFilaProductos.format(
                        data[i]['PROID'], //0
                        data[i]['PRONOMBRE'], //1
                        data[i]['LABNOMBRE'], //2
                        data[i]['PRODETALLE'], //3
                        data[i]['PROCODIGO'], //4
                        data[i]['PROCANTIDAD'], //5
                        data[i]['PROPRECIOVENTA'], //6
                        data[i]['PRODETALLE'], //7
                        data[i]['PROCOMPRA'], //8
                        ((data[i]['PROIVA'] * data[i]['PROPRECIOVENTA']) / 100) + parseInt(data[i]['PROPRECIOVENTA'], 10), //9
                        i + 1 //11
                    ));
                }
                cuerpoTablaProductos.find("[name=agregarProducto]").on("click", modalCantidad);
            } else {
                mostrarError("No se encontró información de Productos");
                cuerpoTablaProductos.empty();
            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}

var modalCantidad = function() {
    $("[name=proCod]").val($(this).data("cod"));
    $("[name=precioVenta]").val($(this).data("proprecio"));
    $("[name=rollback]").val($(this).data("proprecio"));
    $("[name=detalleproentrada]").val($(this).data("detalle"));
    $("[name=costoentrada]").val($(this).data("procompra"));
    $("[name=precioactualVenta]").val($(this).data("proprecio"));
    $("[name=detproiva]").val($(this).data("proiva"));
    $("[name=cantidadentrada]").val('');
    $("[name=rentablidad]").val('');
    $("[name=modalCantidad]").modal("show");
    setTimeout(() => { $('#cantidadentrada').focus() }, 1000);
}

var agregarProducto = function() {
    var validate = $("[name = formularioAgregarProducto]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        if (parseInt(validate.data['cantidadVenta']) <= 0) {
            mostrarError("La cantidad no puede ser menor o igual a 0");
            return;
        }
        if (parseInt(validate.data['proiva']) <= 0 || parseInt(validate.data['proiva']) > 100) {
            mostrarError("IVA no valido");
            return;
        }
        $.ajax({
            url: BASE_URL + "Compras/Menu/agregarProducto",
            type: 'POST',
            data: {
                datos: validate.data
            },
            dataType: 'JSON',
            success: function(r) {
                if (r.data.success) {
                    mostrarOki(r.data.msg);
                    $("[name=modalCantidad]").modal("hide");
                    cargarProductosBodega();
                    cargarProductosVenta(validate.data['entradaCod']);
                    $("[name=cantidadentrada]").val('');
                    $("[name=costoentrada]").val('');
                    $("[name=precioVenta]").val('');
                    $("[name=inputBuscarProductoBodega]").val('');
                } else {
                    mostrarError(r.data.msg);
                }
                hideLoading();
            }
        }).fail(function() {
            mostrarError('Error al procesar, por favor intente nuevamente');
        }).always(function() {

        });
    }
}

var cargarProductosVenta = function(codVenta) {
    var modelFilaProductos =
        '<tr>' +
        '   <td>{2}</td>' +
        '   <td>{10}</td>' +
        '   <td>{11}</td>' +
        '   <td>{3}</td>' +
        '   <td>{4}</td>' +
        '   <td>{5}</td>' +
        '   <td>{13}</td>' +
        '   <td><span class="icon icon-bin text-danger" title="Eliminar" style="text-decoration:none; cursor:pointer; font-size:17px;" name="eliminarProducto" data-cod="{0}" data-cantidad="{3}" data-procod="{1}" data-precioantiguo="{8}" data-costoantiguo="{9}" data-rollbackiva="{12}"></span></td>' +
        '</tr>';
    showLoading();
    $.ajax({
        url: BASE_URL + "Compras/Menu/cargarProductosVenta",
        type: 'POST',
        data: {
            codVenta: codVenta
        },
        dataType: 'JSON',
        success: function(r) {
            var data = r.data.retorno;
            if (data.length > 0) {
                $("[name=ventaTotal]").val(r.data.totalVenta);
                cuerpoTablaProductosVenta.empty();
                for (var i = 0; i < data.length; i++) {
                    cuerpoTablaProductosVenta.append(modelFilaProductos.format(
                        data[i]['DETCCOD'], //0
                        data[i]['PROID'], //1
                        data[i]['PRONOMBRE'], //2
                        data[i]['DETCCANTIDADPRO'], //3
                        data[i]['DETCCOSTOPRO'], //4
                        data[i]['DETCPRECIOVENTAPRO'], //5
                        data[i]['DETCCOMCOD'], //6
                        data[i]['DETCCOSTOPROTOTAL'], //7
                        data[i]['ROLLBACK'], //8
                        data[i]['ROLLBACKCOM'], //9
                        data[i]['PRODETALLE'], //10
                        data[i]['LABNOMBRE'], //11
                        data[i]['ROLLBACKIVA'], //12
                        ((data[i]['DETPROIVA'] * data[i]['DETCPRECIOVENTAPRO']) / 100) + parseInt(data[i]['DETCPRECIOVENTAPRO'], 10), //13
                    ));
                }
                cuerpoTablaProductosVenta.find("[name=eliminarProducto]").on("click", eliminarProductosVenta);
            } else {
                cuerpoTablaProductosVenta.empty();
            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}


var eliminarProductosVenta = function() {
    var estado = $("[name=ventaEstado]").val();
    if (estado == "FACTURADO") {
        mostrarError("No se puede realizar ninguna acción en una venta facturada.");
        return;
    }
    var codVenta = $("[name=ventaCod]").val();
    var cantidad = $(this).data("cantidad");
    var detCod = $(this).data("cod");
    var proCod = $(this).data("procod");
    var precioAntiguo = $(this).data("precioantiguo");
    var costoantiguo = $(this).data("costoantiguo");
    var rollbackiva = $(this).data("rollbackiva");
    showLoading();
    $.ajax({
        url: BASE_URL + "Compras/Menu/eliminarProductosVenta",
        type: 'POST',
        data: {
            cantidad: cantidad,
            detCod: detCod,
            proCod: proCod,
            precioAntiguo: precioAntiguo,
            costoantiguo: costoantiguo,
            rollbackiva: rollbackiva
        },
        dataType: 'JSON',
        success: function(r) {
            if (r.data.success) {
                mostrarOki(r.data.msg);
                cargarProductosBodega();
                cargarProductosVenta(codVenta);
            } else {
                mostrarError(r.data.msg);
            }
            hideLoading();
        }
    }).fail(function() {
        mostrarError('Error al procesar, por favor intente nuevamente');
    }).always(function() {

    });
}

var guardarVenta = function() {
    bootbox.confirm({
        title: "Confirmaci&oacute;n del Sistema",
        message: "&iquest;Est&aacute; seguro que desea guardar la entrada de hoy?",
        buttons: {
            confirm: {
                label: "Si",
                className: "btn-success"
            },
            cancel: {
                label: "No",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result === true) {
                var codVenta = $("[name=ventaCod]").val();
                var observacion = $("[name=textObservacion]").val();
                var proveedor = $("[name=venproveedor]").val();
                showLoading();
                $.ajax({
                    url: BASE_URL + "Compras/Menu/guardarVenta",
                    type: 'POST',
                    data: {
                        codVenta: codVenta,
                        observacion: observacion,
                        proveedor: proveedor
                    },
                    dataType: 'JSON',
                    success: function(r) {
                        if (r.data.success) {
                            mostrarOki(r.data.msg);
                            $("[name=modalCrearVenta]").modal("hide");
                            cargarVentas(3);
                        } else {
                            mostrarError(r.data.msg);
                        }
                        hideLoading();
                    }
                }).fail(function() {
                    mostrarError('Error al procesar, por favor intente nuevamente');
                }).always(function() {

                });
            };
        }
    });
}

var rentabilidad = function() {
    var entrada = $("[name=costoentrada]").val();
    var salida = $("[name=precioVenta]").val();
    var operacion = (salida / entrada) * 100;
    $("[name=rentablidad]").val(parseInt(operacion));
}

$(document).ready(function() {

    $("[name=btnCrearVenta]").click(function() {
        crearVenta();
    });

    compBodega.change(function() {
        cargarProductosBodega();
    });

    $("[name=btnAgregarProducto]").click(function() {
        agregarProducto();
    });

    $("[name=btnAgregarFactura]").click(function() {
        guardarVenta();
    });

    $("[name=btnCargarFacturasAll]").click(function() {
        cargarVentas(1);
    });

    $("[name=btnCargarFacturasHoy]").click(function() {
        cargarVentas(2);
    });

    $("[name=btnCargarMisFacturas]").click(function() {
        cargarVentas(3);
    });

    selectLaboratorios();
    selectBodegas();

    $('#modalCantidad').keypress(function(e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
            agregarProducto();
            e.preventDefault();
            return false;
        }
    });

    cargarVentas(2);

});

//Crear Producto
$("[name =  crearProducto]").click(function() {
    $("#modalProductos").modal('show');

});

$("[name =  crearLaboratorio]").click(function() {
    $("#modalLaboratorios").modal('show');

});

var selectLaboratorios = function() {
    var cuerpo = $("[name=prolabid]");
    var listado = '<option value="{0}">{1}</option>';
    cuerpo.empty();
    $.ajax({
        url: BASE_URL + 'Compras/Menu/cargarLaboratorios',
        type: "POST",
        dataType: 'JSON',
        success: function(resp) {
            var data = resp.data;
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    cuerpo.append(listado.format(
                        data[i]['LABID'], //0
                        data[i]['LABNOMBRE'] //1
                    ));
                }
            }
        }
    }); // fin ajax
}

var selectBodegas = function() {
    var cuerpo = $("[name=probodid]");
    var listado = '<option value="{0}">{1}</option>';
    cuerpo.empty();
    $.ajax({
        url: BASE_URL + 'Compras/Menu/cargarBodegas',
        type: "POST",
        dataType: 'JSON',
        success: function(resp) {
            var data = resp.data;
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    cuerpo.append(listado.format(
                        data[i]['BODID'], //0
                        data[i]['BODNOMBRE'] //1
                    ));
                }
            }
        }
    }); // fin ajax
}

$("[name =  guardarProducto]").click(function() {
    var validate = $("[name = formularioNuevoProducto]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        var data = validate.data;
        $.ajax({
            url: BASE_URL + 'Compras/Menu/ingresarProducto',
            type: 'POST',
            data: {
                dataEv: data
            },
            dataType: 'JSON',
            success: function(resp) {
                var data = resp.data;
                if (data.type == "danger") {
                    mostrarError(data.msg);
                } else {
                    mostrarOki(data.msg);
                    $("#modalProductos").modal("hide");
                    $("[name=pronombre]").val('');
                    $("[name=prodetalle]").val('');
                    $("[name=procodigo]").val('');
                    $("[name=prolabid]").val('');
                    $("[name=proruta]").val('');
                    cargarProductosBodega();
                }
            }
        }); // fin ajax
    }
});


$("[name =  guardarLaboratorio]").click(function() {
    var validate = $("[name = formularioNuevoLaboratorio]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        var data = validate.data;
        $.ajax({
            url: BASE_URL + 'Compras/Menu/ingresarLaboratorio',
            type: 'POST',
            data: {
                dataEv: data
            },
            dataType: 'JSON',
            success: function(resp) {
                var data = resp.data;
                if (data.type == "danger") {
                    mostrarError(data.msg);
                } else {
                    mostrarOki(data.msg);
                    selectLaboratorios();
                    $("[name=labnombre]").val('');
                    $("#modalLaboratorios").modal("hide");
                }
            }
        }); // fin ajax
    }
});


function pulsar(e) {
    if (e.keyCode === 13 && !e.shiftKey) {
        e.preventDefault();
        var codigo = document.getElementById("inputBuscarProductoBodega").value
        var codBodega = compBodega.val();

        if (codigo == "" || codBodega == "") {
            mostrarError("Seleccionar una Bodega y un Producto")
        } else {
            showLoading();
            $.ajax({
                url: BASE_URL + "Compras/Menu/consultarCodPro",
                type: 'POST',
                data: {
                    codigo: codigo,
                    codBodega: codBodega
                },
                dataType: 'JSON',
                success: function(r) {
                    var data = r.data;
                    if (data.length == 1) {
                        $("[name=proCod]").val(data[0]['PROID']);
                        $("[name=detalleproentrada]").val(data[0]['INFO']);
                        $("[name=precioVenta]").val(data[0]['PROPRECIOVENTA']);
                        $("[name=costoentrada]").val(data[0]['PROCOMPRA']);
                        $("[name=cantidadentrada]").val('');
                        $("[name=rentablidad]").val('');
                        $("[name=modalCantidad]").modal("show");
                        setTimeout(() => { $('#cantidadentrada').focus() }, 1000);
                    } else {
                        mostrarError("Error en la seleccion del producto");
                    }
                    hideLoading();
                }
            })

        }
    }
}