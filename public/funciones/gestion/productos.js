//Extraer Productos-------------------------
var cargarProductos = function() {
    var cuerpo = $("[name = cuerpoTabla]");
    var listado = '<tr>' +
        '               <td>{1} {7} {5}</td>' +
        '               <td>{4}</td>' +
        '               <td>{8} / {10}</td>' +
        '               <td>{6}</td>' +
        '               <td>{9}</td>' +
        '               <td>{13}</td>' +
        '               <td>{11}</td>' +
        '               <td>{12}</td>' +
        '               <td>' +
        '<span name="btnEditar" title="Editar Producto" style="cursor:pointer;" data-proid="{0}" data-pronombre="{1}" data-labid="{2}" data-bodid="{3}" data-labnombre="{4}" data-prodetalle="{5}" data-procantidad="{6}" data-procodigo="{7}" data-bodnombre="{8}" data-proprecioventa="{9}" data-proruta="{10}" data-procompra="{11}" data-proiva="{12}" class="icon icon-pencil text-primary"> </span>' +
        '<span name="btnEliminar" title="Eliminar" style="cursor:pointer;" data-proid="{0}" class="icon icon-bin2 text-danger"> </span>' +
        '               </td>' +
        '            </tr>';


    $.ajax({
        url: BASE_URL + 'Gestion/Productos/obtenerProductos',
        dataType: 'JSON',
        success: function(resp) {
            var data = resp.data;
            if (data.length > 0) {
                cuerpo.empty();
                for (var i = 0; i < data.length; i++) {
                    cuerpo.append(listado.format(
                        data[i]['PROID'], //0
                        data[i]['PRONOMBRE'], //1
                        data[i]['LABID'], //2
                        data[i]['BODID'], //3
                        data[i]['LABNOMBRE'], //4
                        data[i]['PRODETALLE'], //5
                        data[i]['PROCANTIDAD'], //6
                        data[i]['PROCODIGO'], //7
                        data[i]['BODNOMBRE'], //8
                        data[i]['PROPRECIOVENTA'], //9
                        data[i]['PRORUTA'], //10
                        data[i]['PROCOMPRA'], //11
                        data[i]['PROIVA'], //12
                        ((data[i]['PROIVA'] * data[i]['PROPRECIOVENTA']) / 100) + parseInt(data[i]['PROPRECIOVENTA'], 10), //13
                        i + 1 //14
                    ));
                }
                cuerpo.find("[name=btnEditar]").on("click", editarProducto);
                cuerpo.find("[name=btnEliminar]").on("click", eliminarProducto);
            } else {
                mostrarOki('No se encontr&oacute; datos', 'danger');
            }
        }
    }); // fin ajax
}

//WarningStock
var stockBajo = function() {
        var cuerpo = $("[name = cuerpoTabla]");
        var listado = '<tr>' +
            '               <td>{1} {7} {5}</td>' +
            '               <td>{4}</td>' +
            '               <td>{8} / {10}</td>' +
            '               <td>{6}</td>' +
            '               <td>{9}</td>' +
            '               <td>{13}</td>' +
            '               <td>{11}</td>' +
            '               <td>{12}</td>' +
            '               <td>' +
            '<span name="btnEditar" title="Editar Producto" style="cursor:pointer;" data-proid="{0}" data-pronombre="{1}" data-labid="{2}" data-bodid="{3}" data-labnombre="{4}" data-prodetalle="{5}" data-procantidad="{6}" data-procodigo="{7}" data-bodnombre="{8}" data-proprecioventa="{9}" data-proruta="{10}" data-procompra="{11}" data-proiva="{12}" class="icon icon-pencil text-primary"> </span>' +
            '<span name="btnEliminar" title="Eliminar" style="cursor:pointer;" data-proid="{0}" class="icon icon-bin2 text-danger"> </span>' +
            '               </td>' +
            '            </tr>';


        $.ajax({
            url: BASE_URL + 'Gestion/Productos/stockBajo',
            dataType: 'JSON',
            success: function(resp) {
                var data = resp.data;
                if (data.length > 0) {
                    cuerpo.empty();
                    for (var i = 0; i < data.length; i++) {
                        cuerpo.append(listado.format(
                            data[i]['PROID'], //0
                            data[i]['PRONOMBRE'], //1
                            data[i]['LABID'], //2
                            data[i]['BODID'], //3
                            data[i]['LABNOMBRE'], //4
                            data[i]['PRODETALLE'], //5
                            data[i]['PROCANTIDAD'], //6
                            data[i]['PROCODIGO'], //7
                            data[i]['BODNOMBRE'], //8
                            data[i]['PROPRECIOVENTA'], //9
                            data[i]['PRORUTA'], //10
                            data[i]['PROCOMPRA'], //11
                            data[i]['PROIVA'], //12
                            ((data[i]['PROIVA'] * data[i]['PROPRECIOVENTA']) / 100) + parseInt(data[i]['PROPRECIOVENTA'], 10), //13
                            i + 1 //14
                        ));
                    }
                    cuerpo.find("[name=btnEditar]").on("click", editarProducto);
                    cuerpo.find("[name=btnEliminar]").on("click", eliminarProducto);
                } else {
                    mostrarOki('No se encontr&oacute; datos', 'danger');
                }
            }
        }); // fin ajax
    }
    //-----------------------------------------

function filtarTablaPersonas() {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("busqueda");
    filter = input.value.toUpperCase();
    table = document.getElementById("tablaPersonas");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

$(document).ready(function() {
    cargarProductos();
    selectLaboratorios();
    selectBodegas();

});

var rentabilidad = function() {
    var salida = $("[name=proprecioventa]").val();
    var entrada = $("[name=procompra]").val();
    var operacion = (salida / entrada) * 100;
    $("[name=rentablidad]").val(parseInt(operacion));
}

//Crear Producto
$("[name =  crearProducto]").click(function() {
    $("#modalProductos").modal('show');
});

$("[name =  stocks]").click(function() {
    stockBajo();
});

$("[name =  products]").click(function() {
    cargarProductos();
});

var selectLaboratorios = function() {
    var cuerpo = $("[name=prolabid]");
    var listado = '<option value="{0}">{1}</option>';
    cuerpo.empty();
    $.ajax({
        url: BASE_URL + 'Gestion/Productos/cargarLaboratorios',
        type: "POST",
        dataType: 'JSON',
        success: function(resp) {
            var data = resp.data;
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    cuerpo.append(listado.format(
                        data[i]['LABID'], //0
                        data[i]['LABNOMBRE'] //1
                    ));
                }
            }
        }
    }); // fin ajax
}

var selectBodegas = function() {
    var cuerpo = $("[name=probodid]");
    var listado = '<option value="{0}">{1}</option>';
    cuerpo.empty();
    $.ajax({
        url: BASE_URL + 'Gestion/Productos/cargarBodegas',
        type: "POST",
        dataType: 'JSON',
        success: function(resp) {
            var data = resp.data;
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    cuerpo.append(listado.format(
                        data[i]['BODID'], //0
                        data[i]['BODNOMBRE'] //1
                    ));
                }
            }
        }
    }); // fin ajax
}

$("[name =  guardarProducto]").click(function() {
    var validate = $("[name = formularioNuevoProducto]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        var data = validate.data;
        $.ajax({
            url: BASE_URL + 'Gestion/Productos/ingresarProducto',
            type: 'POST',
            data: {
                dataEv: data
            },
            dataType: 'JSON',
            success: function(resp) {
                var data = resp.data;
                if (data.type == "danger") {
                    mostrarError(data.msg);
                } else {
                    mostrarOki(data.msg);
                    $("#modalProductos").modal("hide");
                    borrarproducto();
                    cargarProductos();
                }
            }
        }); // fin ajax
    }
});

var borrarproducto = function() {
    $("[name= pronombre]").val('');
    $("[name= prodetalle]").val('');
    $("[name= procodigo]").val('');
    $("[name= procantidad]").val('');
    $("[name= proprecioventa]").val('');
    $("[name= procompra]").val('');
    $("[name= proruta]").val('');
    $("[name= rentablidad]").val('');
}

//Editar Producto

var editarProducto = function() {
    var editproid = $(this).data("proid");
    var editpronombre = $(this).data("pronombre");
    var editprodetalle = $(this).data("prodetalle");
    var editprocodigo = $(this).data("procodigo");
    var editprocantidad = $(this).data("procantidad");
    var editproprecioventa = $(this).data("proprecioventa");
    var editprolabid = $(this).data("labid");
    var editprobodid = $(this).data("bodid");
    var editprolabnombre = $(this).data("labnombre");
    var editprobodnombre = $(this).data("bodnombre");
    var editproruta = $(this).data("proruta");
    var editprocompra = $(this).data("procompra");
    var editiva = $(this).data("proiva");
    $("[name= editproid]").val(editproid);
    $("[name= editpronombre]").val(editpronombre);
    $("[name= editprodetalle]").val(editprodetalle);
    $("[name= editprocodigo]").val(editprocodigo);
    $("[name= editprocantidad]").val(editprocantidad);
    $("[name= editproprecioventa]").val(editproprecioventa);
    $("[name= editproruta]").val(editproruta);
    $("[name= editpropreciocompra]").val(editprocompra);
    $("[name= editproiva]").val(editiva);
    selectLaboratoriosedit(editprolabid, editprolabnombre);
    selectBodegasedit(editprobodid, editprobodnombre);
    $("#modalEditarProductos").modal('show');
}

var selectLaboratoriosedit = function(editprolabid, editprolabnombre) {
    var cuerpo = $("[name=editprolabid]");
    cuerpo.empty();
    $.ajax({
        url: BASE_URL + 'Gestion/Productos/cargarLaboratoriosEdit',
        type: "POST",
        data: {
            editprolabid: editprolabid
        },
        dataType: 'JSON',
        success: function(resp) {
            var data = resp.data;
            if (data.length > 0) {
                cuerpo.append('<option seleted="true" value="' + editprolabid + '">' + editprolabnombre + '</option>')
                for (var i = 0; i < data.length; i++) {
                    cuerpo.append('<option value="' + data[i]['LABID'] + '">' + data[i]['LABNOMBRE'] + '</option>');
                }
            }
        }
    }); // fin ajax
}

var selectBodegasedit = function(editprobodid, editprobodnombre) {
    var cuerpo = $("[name=editprobodid]");
    cuerpo.empty();
    $.ajax({
        url: BASE_URL + 'Gestion/Productos/cargarBodegasEdit',
        type: "POST",
        data: {
            editprobodid: editprobodid
        },
        dataType: 'JSON',
        success: function(resp) {
            var data = resp.data;
            if (data.length > 0) {
                cuerpo.append('<option seleted="true" value="' + editprobodid + '">' + editprobodnombre + '</option>')
                for (var i = 0; i < data.length; i++) {
                    cuerpo.append('<option value="' + data[i]['BODID'] + '">' + data[i]['BODNOMBRE'] + '</option>');
                }
            }
        }
    }); // fin ajax
}


$("[name=editarProducto]").click(function() {
    var validate = $("[name = formularioEditarProducto]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        var data = validate.data;
        $.ajax({
            url: BASE_URL + 'Gestion/Productos/editarProducto',
            type: 'POST',
            data: {

                dataEv: data
            },
            dataType: 'JSON',
            success: function(resp) {

                var data = resp.data;
                if (data.type == "danger") {
                    mostrarError(data.msg);
                } else {
                    mostrarOki(data.msg);
                    $("#modalEditarProductos").modal("hide");
                    cargarProductos();
                }

            }
        }); // fin ajax
    }
});


//Eliminar Producto

var eliminarProducto = function() {
    var proid = $(this).data("proid");
    cuepor = $("[name=cuerpoTabla]");
    bootbox.confirm({
        message: "Peticion para eliminar un producto",
        buttons: {
            confirm: {
                label: "Aceptar",
                className: "btn-success"
            },
            cancel: {
                label: "Cancelar",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result === true) {
                $.ajax({
                    // la URL para la petici&oacute;n
                    url: BASE_URL + "Gestion/Productos/eliminarProducto",
                    type: "POST",
                    data: {
                        proid: proid
                    },
                    dataType: "JSON",
                    success: function(resp) {
                        if (resp.data.type = "success") {
                            mostrarOki(resp.data.msg);
                            cuepor.empty();
                            cargarProductos();
                        } else {
                            mostrarError(resp.msg);
                        }

                    }
                });
            };
        }
    });
}