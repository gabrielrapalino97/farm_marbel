//Extraer Productos-------------------------
var cargarBodegas = function() {
    var cuerpo = $("[name = cuerpoTabla]");
    var listado = '<tr>' +
        '               <td>{2}</td>' +
        '               <td>{1}</td>' +
        '               <td>' +
        '<span name="btnEditar" title="Editar Bodega" style="cursor:pointer;" data-bodid="{0}" data-bodnombre="{1}"  class="icon icon-pencil text-primary"> </span>' +
        '<span name="btnEliminar" title="Eliminar" style="cursor:pointer;" data-bodid="{0}" class="icon icon-bin2 text-danger"> </span>' +
        '               </td>' +
        '            </tr>';

        
    $.ajax({
        url: BASE_URL + 'Gestion/Bodegas/obtenerBodegas',
        dataType: 'JSON',
        success: function(resp) {
            var data = resp.data;
            if (data.length > 0) {
                cuerpo.empty();
                for (var i = 0; i < data.length; i++) {
                    cuerpo.append(listado.format(
                        data[i]['BODID'], //0
                        data[i]['BODNOMBRE'], //1
                        i + 1 //2
                    ));
                }
                 cuerpo.find("[name=btnEditar]").on("click", editarBodega);
                 cuerpo.find("[name=btnEliminar]").on("click", eliminarBodega);
            } else {
                mostrarOki('No se encontr&oacute; datos', 'danger');
            }
        }
    }); // fin ajax
}

function filtarTablaPersonas() {
	// Declare variables
	var input, filter, table, tr, td, i, txtValue;
	input = document.getElementById("busqueda");
	filter = input.value.toUpperCase();
	table = document.getElementById("tablaPersonas");
	tr = table.getElementsByTagName("tr");

	// Loop through all table rows, and hide those who don't match the search query
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[1];
		if (td) {
			txtValue = td.textContent || td.innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
}

$(document).ready(function() {
    cargarBodegas();

});


//Crear Bodega
$("[name =  crearBodega]").click(function() {
    $("#modalBodegas").modal('show');

});

$("[name =  guardarBodega]").click(function() {
    var validate = $("[name = formularioNuevoBodega]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        var data = validate.data;
        $.ajax({
            url: BASE_URL + 'Gestion/Bodegas/ingresarBodega',
            type: 'POST',
            data: {
                dataEv: data
            },
            dataType: 'JSON',
            success: function(resp) {
                var data = resp.data;
                if (data.type == "danger") {
                    mostrarError(data.msg);
                } else {
                    mostrarOki(data.msg);
                    $("#modalBodegas").modal("hide");
                    borrarbodega();
                    cargarBodegas();
                }
            }
        }); // fin ajax
    }
});

var borrarbodega = function() {
    $("[name= bodnombre]").val('');
}

//Editar Bodega

var editarBodega = function() {
    var bodid = $(this).data("bodid");
    var bodnombre = $(this).data("bodnombre");
    $("[name= editbodnombre]").val(bodnombre);
    $("[name= editbodid]").val(bodid);
    $("#modalEditarBodega").modal('show');
}

$("[name=editarBodega]").click(function() {
    var validate = $("[name = formularioEditarBodega]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        var data = validate.data;
        $.ajax({
            url: BASE_URL + 'Gestion/Bodegas/editarBodega',
            type: 'POST',
            data: {

                dataEv: data
            },
            dataType: 'JSON',
            success: function(resp) {

                var data = resp.data;
                if (data.type == "danger") {
                    mostrarError(data.msg);
                } else {
                    mostrarOki(data.msg);
                    $("#modalEditarBodega").modal("hide");
                    cargarBodegas();
                }

            }
        }); // fin ajax
    }
});


//Eliminar Bodega

var eliminarBodega = function() {
    var bodid = $(this).data("bodid");
    cuepor = $("[name=cuerpoTabla]");
    bootbox.confirm({
        message: "Peticion para eliminar una Bodega",
        buttons: {
            confirm: {
                label: "Aceptar",
                className: "btn-success"
            },
            cancel: {
                label: "Cancelar",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result === true) {
                $.ajax({
                    // la URL para la petici&oacute;n
                    url: BASE_URL + "Gestion/Bodegas/eliminarBodega",
                    type: "POST",
                    data: {
                        bodid: bodid
                    },
                    dataType: "JSON",
                    success: function(resp) {
                        if (resp.data.type = "success") {
                            mostrarOki(resp.data.msg);
                            cuepor.empty();
                            cargarBodegas();
                        } else {
                            mostrarError(resp.msg);
                        }

                    }
                });
            };
        }
    });
}
