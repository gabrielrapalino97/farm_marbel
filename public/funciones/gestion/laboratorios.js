//Extraer Productos-------------------------
var cargarLaboratorios = function() {
    var cuerpo = $("[name = cuerpoTabla]");
    var listado = '<tr>' +
        '               <td>{2}</td>' +
        '               <td>{1}</td>' +
        '               <td>' +
        '<span name="btnEditar" title="Editar Laboratorio" style="cursor:pointer;" data-labid="{0}" data-labnombre="{1}"  class="icon icon-pencil text-primary"> </span>' +
        '<span name="btnEliminar" title="Eliminar" style="cursor:pointer;" data-labid="{0}" class="icon icon-bin2 text-danger"> </span>' +
        '               </td>' +
        '            </tr>';

        
    $.ajax({
        url: BASE_URL + 'Gestion/Laboratorios/obtenerLaboratorios',
        dataType: 'JSON',
        success: function(resp) {
            var data = resp.data;
            if (data.length > 0) {
                cuerpo.empty();
                for (var i = 0; i < data.length; i++) {
                    cuerpo.append(listado.format(
                        data[i]['LABID'], //0
                        data[i]['LABNOMBRE'], //1
                        i + 1 //2
                    ));
                }
                 cuerpo.find("[name=btnEditar]").on("click", editarLaboratorio);
                 cuerpo.find("[name=btnEliminar]").on("click", eliminarLaboratorio);
            } else {
                mostrarOki('No se encontr&oacute; datos', 'danger');
            }
        }
    }); // fin ajax
}

function filtarTablaPersonas() {
	// Declare variables
	var input, filter, table, tr, td, i, txtValue;
	input = document.getElementById("busqueda");
	filter = input.value.toUpperCase();
	table = document.getElementById("tablaPersonas");
	tr = table.getElementsByTagName("tr");

	// Loop through all table rows, and hide those who don't match the search query
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[1];
		if (td) {
			txtValue = td.textContent || td.innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
}

$(document).ready(function() {
    cargarLaboratorios();

});


//Crear Laboratorio
$("[name =  crearLaboratorio]").click(function() {
    $("#modalLaboratorios").modal('show');

});

$("[name =  guardarLaboratorio]").click(function() {
    var validate = $("[name = formularioNuevoLaboratorio]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        var data = validate.data;
        $.ajax({
            url: BASE_URL + 'Gestion/Laboratorios/ingresarLaboratorio',
            type: 'POST',
            data: {
                dataEv: data
            },
            dataType: 'JSON',
            success: function(resp) {
                var data = resp.data;
                if (data.type == "danger") {
                    mostrarError(data.msg);
                } else {
                    mostrarOki(data.msg);
                    $("#modalLaboratorios").modal("hide");
                    borrarlaboratorio();
                    cargarLaboratorios();
                }
            }
        }); // fin ajax
    }
});

var borrarlaboratorio = function() {
    $("[name= labnombre]").val('');
}

//Editar Laboratorio

var editarLaboratorio = function() {
    var labid = $(this).data("labid");
    var labnombre = $(this).data("labnombre");
    $("[name= editlabnombre]").val(labnombre);
    $("[name= editlabid]").val(labid);
    $("#modalEditarLaboratorio").modal('show');
}

$("[name=editarLaboratorio]").click(function() {
    var validate = $("[name = formularioEditarLaboratorio]").formValidation({
        returnData: true
    });
    if (validate.error === true) {
        mostrarError(validate.message);
    } else {
        var data = validate.data;
        $.ajax({
            url: BASE_URL + 'Gestion/Laboratorios/editarLaboratorio',
            type: 'POST',
            data: {

                dataEv: data
            },
            dataType: 'JSON',
            success: function(resp) {

                var data = resp.data;
                if (data.type == "danger") {
                    mostrarError(data.msg);
                } else {
                    mostrarOki(data.msg);
                    $("#modalEditarLaboratorio").modal("hide");
                    cargarLaboratorios();
                }

            }
        }); // fin ajax
    }
});


//Eliminar Laboratorio

var eliminarLaboratorio = function() {
    var labid = $(this).data("labid");
    cuepor = $("[name=cuerpoTabla]");
    bootbox.confirm({
        message: "Peticion para eliminar un laboratorio",
        buttons: {
            confirm: {
                label: "Aceptar",
                className: "btn-success"
            },
            cancel: {
                label: "Cancelar",
                className: "btn-danger"
            }
        },
        callback: function(result) {
            if (result === true) {
                $.ajax({
                    // la URL para la petici&oacute;n
                    url: BASE_URL + "Gestion/Laboratorios/eliminarLaboratorio",
                    type: "POST",
                    data: {
                        labid: labid
                    },
                    dataType: "JSON",
                    success: function(resp) {
                        if (resp.data.type = "success") {
                            mostrarOki(resp.data.msg);
                            cuepor.empty();
                            cargarLaboratorios();
                        } else {
                            mostrarError(resp.msg);
                        }

                    }
                });
            };
        }
    });
}
