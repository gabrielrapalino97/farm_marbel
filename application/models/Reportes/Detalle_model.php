<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Detalle_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function reporteGeneral($fecha)
    {
        if ($fecha == "" || $fecha == null || $fecha == "undefined") {
            $sqlProductos = sprintf("SELECT DV.DETVENCOD, 
                                    DV.DETCANTIDADVENTA, 
                                    (SELECT PRONOMBRE FROM Productos WHERE PROID = DV.DETPROCOD) AS PRONOMBRE,
                                    (SELECT PROCODIGO FROM Productos WHERE PROID = DV.DETPROCOD) AS PROCODIGO, 
                                    (SELECT PRODETALLE FROM Productos WHERE PROID = DV.DETPROCOD) AS PRODETALLE, 
                                    (SELECT BODNOMBRE FROM Bodegas INNER JOIN Productos ON Bodegas.BODID = Productos.BODID WHERE Productos.PROID = DV.DETPROCOD) AS BODNOMBRE,
                                    (SELECT LABNOMBRE FROM Laboratorios INNER JOIN Productos ON Laboratorios.LABID = Productos.BODID WHERE Productos.PROID = DV.DETPROCOD) AS LABNOMBRE,
                                    (SELECT USUNOMBRE FROM Usuarios INNER JOIN Ventas ON Usuarios.USUCOD = Ventas.VENUSUCOD WHERE Ventas.VENCOD = DV.DETVENCOD) AS USUNOMBRE
                                    FROM Detalle_ventas DV
                                    WHERE DV.DETVENCOD IN (SELECT V1.VENCOD FROM Ventas V1 WHERE V1.VENFECHA='%s' AND V1.VENESTADO=1)
                                    ", date('Y-m-d'));
        } else {
            $sqlProductos = sprintf("SELECT DV.DETVENCOD, 
                                    DV.DETCANTIDADVENTA, 
                                    (SELECT PRONOMBRE FROM Productos WHERE PROID = DV.DETPROCOD) AS PRONOMBRE,
                                    (SELECT PROCODIGO FROM Productos WHERE PROID = DV.DETPROCOD) AS PROCODIGO, 
                                    (SELECT PRODETALLE FROM Productos WHERE PROID = DV.DETPROCOD) AS PRODETALLE, 
                                    (SELECT BODNOMBRE FROM Bodegas INNER JOIN Productos ON Bodegas.BODID = Productos.BODID WHERE Productos.PROID = DV.DETPROCOD) AS BODNOMBRE,
                                    (SELECT LABNOMBRE FROM Laboratorios INNER JOIN Productos ON Laboratorios.LABID = Productos.BODID WHERE Productos.PROID = DV.DETPROCOD) AS LABNOMBRE,
                                    (SELECT USUNOMBRE FROM Usuarios INNER JOIN Ventas ON Usuarios.USUCOD = Ventas.VENUSUCOD WHERE Ventas.VENCOD = DV.DETVENCOD) AS USUNOMBRE
                                    FROM Detalle_ventas DV
                                    WHERE DV.DETVENCOD IN (SELECT V1.VENCOD FROM Ventas V1 WHERE V1.VENFECHA='%s' AND V1.VENESTADO=1)
                                    ", $fecha);
        }
        $result = $this->db->query($sqlProductos);
        $resultado = $result->result_array();
        $result->free_result();
        $this->db->close();
        $retornoProductos = array();
        foreach ($resultado as $key) {
            array_push($retornoProductos, array(
                "PRONOMBRE" => utf8_encode($key['PRONOMBRE']),
                "LABNOMBRE" => utf8_encode($key['LABNOMBRE']),
                "BODNOMBRE" => utf8_encode($key['BODNOMBRE']),
                "PRODETALLE" => utf8_encode($key['PRODETALLE']),
                "PROCODIGO" => $key['PROCODIGO'],
                "CANTIDAVENDIDA" => $key['DETCANTIDADVENTA'],
                "VENCOD" => $key['DETVENCOD'],
                "USUNOMBRE" => utf8_encode($key['USUNOMBRE'])
            ));
        }
        $this->db->close();
        return $retornoProductos;
    }
}
