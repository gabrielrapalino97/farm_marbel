<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sugerido_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function reporteGeneral($fecha)
    {
        if ($fecha == "" || $fecha == null || $fecha == "undefined") {
            $sqlProductos = sprintf("SELECT P.PRONOMBRE, P.PRODETALLE, P.PROCANTIDAD, SUM(DV.DETCANTIDADVENTA) AS CANTVENTA, B.BODNOMBRE, L.LABNOMBRE, P.PROCODIGO
                                    FROM Ventas V
                                        INNER JOIN Detalle_ventas DV ON (V.VENCOD=DV.DETVENCOD)
                                        INNER JOIN Productos P ON(DV.DETPROCOD=P.PROID)
                                        INNER JOIN Bodegas B ON (P.BODID=B.BODID)
                                        INNER JOIN Laboratorios L ON (P.LABID=L.LABID)
                                    GROUP BY P.PROID");
        } else {
            $sqlProductos = sprintf("SELECT P.PRONOMBRE, P.PRODETALLE, P.PROCANTIDAD, SUM(DV.DETCANTIDADVENTA) AS CANTVENTA, B.BODNOMBRE, L.LABNOMBRE, P.PROCODIGO
                                    FROM Ventas V
                                        INNER JOIN Detalle_ventas DV ON (V.VENCOD=DV.DETVENCOD)
                                        INNER JOIN Productos P ON(DV.DETPROCOD=P.PROID)
                                        INNER JOIN Bodegas B ON (P.BODID=B.BODID)
                                        INNER JOIN Laboratorios L ON (P.LABID=L.LABID)
                                    WHERE V.VENFECHA='%s'
                                    GROUP BY P.PROID", $fecha);
        }
        $result = $this->db->query($sqlProductos);
        $resultado = $result->result_array();
        $result->free_result();
        $this->db->close();
        $retornoProductos = array();
        foreach ($resultado as $key) {
            array_push($retornoProductos, array(
                "PRONOMBRE" => utf8_encode($key['PRONOMBRE']),
                "LABNOMBRE" => utf8_encode($key['LABNOMBRE']),
                "BODNOMBRE" => utf8_encode($key['BODNOMBRE']),
                "PRODETALLE" => utf8_encode($key['PRODETALLE']),
                "PROCODIGO" => $key['PROCODIGO'],
                "CANTIDADBODEGA" => $key['PROCANTIDAD'],
                "CANTIDAVENDIDA" => $key['CANTVENTA'],
                "SUGERIDO" => (intval($key['PROCANTIDAD']) < 30) ? (30 - intval($key['PROCANTIDAD'])) . " --> (30)" : 0
            ));
        }
        $this->db->close();
        return $retornoProductos;
    }
}
