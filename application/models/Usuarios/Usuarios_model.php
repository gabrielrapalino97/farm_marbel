<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function cargarRol()
    {
        $sql = sprintf("SELECT ROLCOD, ROLDETALLE FROM Roles");
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $retorno = array();
        foreach ($resultado as $key) {
            array_push($retorno, array(
                "ROLCOD" => $key['ROLCOD'],
                "ROLDETALLE" => utf8_encode($key['ROLDETALLE'])
            ));
        }
        $this->db->close();
        return $retorno;
    }

    public function cargarPermisos($codRol)
    {
        $sql = sprintf("SELECT P.PERCOD, P.PEROPCION
                        FROM Permisos P
                        WHERE P.PERCOD NOT IN (SELECT PEROLCOD 
                                                FROM  Permisos P INNER JOIN Per_roles PR ON PR.PERCOD=P.PERCOD
                                                                 INNER JOIN Roles R ON R.ROLCOD=PR.ROLCOD
                                                WHERE R.ROLCOD=$codRol)");
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $retorno = array();
        foreach ($resultado as $key) {
            array_push($retorno, array(
                "PERCOD" => $key['PERCOD'],
                "PEROPCION" => utf8_encode($key['PEROPCION'])
            ));
        }
        $this->db->close();
        return $retorno;
    }

    public function insertarUsuario($datos)
    {
        $sqlValidaUsuario = sprintf("SELECT USUCOD FROM Usuarios WHERE USUCEDULA='%s'", trim($datos['cedula']));
        $result = $this->db->query($sqlValidaUsuario);
        $resultadoValidaUsuario = $result->result_array();
        $result->free_result();
        if (count($resultadoValidaUsuario) > 0) {
            return array("msg" => "Este Usuario ya se encuentra registrado", "success" => false);
        }

        $sqlInserta = sprintf(
            "INSERT INTO `Usuarios`(
            `USUNOMBRE`,
            `USUAPELLIDO`,
            `USUCEDULA`,
            `USUEMAIL`,
            `USUTELEFONO`,
            `USUPASSWORD`,
            `USUESTADO`,
            `ROLCOD`
            ) VALUES (
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                %s,
                %s
            )",
            strtoupper(
                utf8_decode(trim($datos['nombre']))
            ),
            strtoupper(
                utf8_decode(trim($datos['apellido']))
            ),
            utf8_decode(trim($datos['cedula'])),
            utf8_decode(trim($datos['email'])),
            utf8_decode(trim($datos['telefono'])),
            md5('123'),
            1,
            $datos['rol']
        );
        $result = $this->db->query($sqlInserta);
        $this->db->close();
        if ($result) {
            return array("msg" => "Informaci&oacute;n procesada correctamente.", "success" => true);
        } else {
            return array("msg" => "Ocurrio un problema al procesar la informaci&oacute, intente nuevamente.", "success" => false);
        }
    }


    public function cargarDatosUsuarios($codigo)
    {
        $sql = sprintf("SELECT USUCOD, USUNOMBRE, USUAPELLIDO, USUCEDUlA, USUEMAIL, USUTELEFONO 
                        FROM Usuarios 
                        WHERE USUCEDULA='$codigo'");
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $retorno = array();
        foreach ($resultado as $key) {
            array_push($retorno, array(
                "USUCOD" => $key['USUCOD'],
                "USUNOMBRE" => utf8_encode($key['USUNOMBRE']),
                "USUAPELLIDO" => utf8_encode($key['USUAPELLIDO']),
                "USUCEDUlA" => utf8_encode($key['USUCEDUlA']),
                "USUEMAIL" => utf8_encode($key['USUEMAIL']),
                "USUTELEFONO" => utf8_encode($key['USUTELEFONO'])
            ));
        }
        $this->db->close();
        return $retorno;
    }

    public function actualizarUsuario($datos)
    {

        $sql = sprintf(
            "UPDATE Usuarios SET 
                                USUNOMBRE='%s', 
                                USUAPELLIDO='%s', 
                                USUCEDULA='%s',
                                USUEMAIL='%s',
                                USUTELEFONO='%s'
                                WHERE USUCOD=%s",
            strtoupper(
                utf8_decode(trim($datos['nombre']))
            ),
            strtoupper(
                utf8_decode(trim($datos['apellido']))
            ),
            utf8_decode(trim($datos['cedula'])),
            utf8_decode(trim($datos['email'])),
            utf8_decode(
                trim($datos['telefono'])
            ),
            $datos['cod']
        );
        $result = $this->db->query($sql);
        $this->db->close();
        if ($result) {
            return array("msg" => "Informaci&oacute;n procesada correctamente.", "success" => true);
        } else {
            return array("msg" => "Ocurrio un problema al procesar la informaci&oacute, intente nuevamente.", "success" => false);
        }
    }

    public function cambiarClave($datos, $codigo)
    {
        $sqlValidaClave = sprintf(
            "SELECT USUPASSWORD, USUCOD 
                                    FROM Usuarios 
                                    WHERE USUCEDULA='$codigo' 
                                        AND USUPASSWORD='%s'",
            md5(trim($datos['actual']))
        );
        $result = $this->db->query($sqlValidaClave);
        $resultadoValidaClave = $result->result_array();
        $result->free_result();
        if (count($resultadoValidaClave) > 0) {
            $sqlActualiza = sprintf(
                "UPDATE Usuarios SET USUPASSWORD='%s' WHERE USUCOD=%s",
                md5(trim($datos['nueva'])),
                $resultadoValidaClave[0]['USUCOD']
            );
            $result = $this->db->query($sqlActualiza);
            if ($result) {
                return array("msg" => "Informaci&oacute;n procesada correctamente.", "success" => true);
            } else {
                return array("msg" => "Ocurrio un problema al procesar la informaci&oacute, intente nuevamente.", "success" => false);
            }
        } else {
            return array("msg" => "La contrase&ntilde;a actual no coincide con la registrada en sistema", "success" => false);
        }
    }

    public function buscarUsuario($datos)
    {
        $sql = sprintf(
            "SELECT USUCOD, USUNOMBRE, USUAPELLIDO, USUCEDUlA, USUEMAIL, USUTELEFONO 
                        FROM Usuarios 
                        WHERE USUCEDULA='%s'",
            trim($datos['cedula'])
        );
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $retorno = array();
        foreach ($resultado as $key) {
            array_push($retorno, array(
                "USUCOD" => $key['USUCOD'],
                "USUNOMBRE" => utf8_encode($key['USUNOMBRE']),
                "USUAPELLIDO" => utf8_encode($key['USUAPELLIDO']),
                "USUCEDUlA" => utf8_encode($key['USUCEDUlA']),
                "USUEMAIL" => utf8_encode($key['USUEMAIL']),
                "USUTELEFONO" => utf8_encode($key['USUTELEFONO'])
            ));
        }
        $this->db->close();
        return $retorno;
    }

    public function restablacerClave($cod)
    {
        $sql = sprintf(
            "UPDATE Usuarios SET USUPASSWORD='%s', USUESTADO=1 WHERE USUCOD=%s",
            md5(trim('123')),
            $cod
        );
        $result = $this->db->query($sql);
        if ($result) {
            return array("msg" => "La contrase&ntilde;a del Usuario fue restablecida.", "success" => true);
        } else {
            return array("msg" => "Ocurrio un problema al procesar la informaci&oacute, intente nuevamente.", "success" => false);
        }
    }

    public function inhabilitarClave($cod)
    {
        $sql = sprintf(
            "UPDATE Usuarios SET USUESTADO=2 WHERE USUCOD=%s",
            $cod
        );
        $result = $this->db->query($sql);
        if ($result) {
            return array("msg" => "El Usuario fue Inhabilitado.", "success" => true);
        } else {
            return array("msg" => "Ocurrio un problema al procesar la informaci&oacute, intente nuevamente.", "success" => false);
        }
    }

    public function insertarRol($datos)
    {
        $sqlValidaRol = sprintf("SELECT ROLCOD FROM Roles WHERE ROLDETALLE='%s'", strtoupper(utf8_encode(trim($datos['nombre']))));
        $result = $this->db->query($sqlValidaRol);
        $resultadoValidaRol = $result->result_array();
        $result->free_result();
        if (count($resultadoValidaRol) > 0) {
            return array("msg" => "Este Rol ya se encuentra registrado", "success" => false);
        }

        $sqlInserta = sprintf(
            "INSERT INTO Roles(
                ROLDETALLE
            ) VALUES (
                '%s'
            )",
            strtoupper(
                utf8_decode(trim($datos['nombre']))
            )
        );
        $result = $this->db->query($sqlInserta);
        $this->db->close();
        if ($result) {
            return array("msg" => "Informaci&oacute;n procesada correctamente.", "success" => true);
        } else {
            return array("msg" => "Ocurrio un problema al procesar la informaci&oacute, intente nuevamente.", "success" => false);
        }
    }

    public function cargarPermisosRol($cod)
    {
        $sql = sprintf("SELECT P.PERCOD, P.PEROPCION, PR.PEROLCOD
                        FROM Permisos P INNER JOIN Per_roles PR ON PR.PERCOD=P.PERCOD
                        WHERE PR.ROLCOD=$cod");
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $retorno = array();
        foreach ($resultado as $key) {
            array_push($retorno, array(
                "PERCOD" => $key['PERCOD'],
                "PEROPCION" => utf8_encode($key['PEROPCION']),
                "PEROLCOD" => $key['PEROLCOD'],
                "ROL" => $cod
            ));
        }
        $this->db->close();
        return $retorno;
    }
	

    public function insertarPermisosRol($datos, $codRol)
    {
        foreach ($datos as $key => $value) {
            $sqlValidaPermiso = sprintf("SELECT PEROLCOD FROM Per_roles WHERE PERCOD=$value AND ROLCOD=$codRol");
            $result = $this->db->query($sqlValidaPermiso);
            $resultadoValidaPermiso = $result->result_array();
            $result->free_result();
            if (count($resultadoValidaPermiso) > 0) { } else {
                $sqlInserta = sprintf(
                    "INSERT INTO Per_roles(
                    PERCOD,
                    ROLCOD,
                    PEROLESTADO
                    ) VALUES (
                        %s,
                        %s,
                        1
                    )",
                    $value,
                    $codRol
                );
                $result = $this->db->query($sqlInserta);
            }
        }
        $this->db->close();
        return array("msg" => "Informaci&oacute;n procesada correctamente.", "success" => true, "codRol" => $codRol);
    }
}


