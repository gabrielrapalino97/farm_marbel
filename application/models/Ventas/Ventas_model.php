<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ventas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function cargarProductosBodega($codBodega)
    {
        $sql = sprintf("SELECT P.PROID, P.PRONOMBRE, L.LABNOMBRE, P.PRODETALLE, P.PROCODIGO, P.PROCANTIDAD, P.PROPRECIOVENTA, P.PROIVA 
                        FROM Productos P 
                            INNER JOIN Bodegas B ON (P.BODID=B.BODID)
                            INNER JOIN Laboratorios L ON (P.LABID=L.LABID)
                        WHERE B.BODID=%s
                            AND P.PROESTADO=1", $codBodega);
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $this->db->close();
        $retorno = array();
        foreach ($resultado as $key) {
            array_push($retorno, array(
                "PROID" => $key['PROID'],
                "PRONOMBRE" => utf8_encode($key['PRONOMBRE']),
                "LABNOMBRE" => utf8_encode($key['LABNOMBRE']),
                "PRODETALLE" => utf8_encode($key['PRODETALLE']),
                "PROCODIGO" => $key['PROCODIGO'],
                "PROCANTIDAD" => $key['PROCANTIDAD'],
                "PROPRECIOVENTA" => $key['PROPRECIOVENTA'],
                "PROIVA" => $key['PROIVA']
            ));
        }
        return $retorno;
    }

    public function agregarProducto($datos)
    {
        $retorno = registrarVentaProducto($datos['proCod'], $datos['cantidadVenta'], $datos['ventaCodVentaProducto']);
        return $retorno;
    }

    public function cargarProductosVenta($codVenta)
    {
        $sql = sprintf(
            "SELECT DV.DETCOD, CONCAT(P.PRONOMBRE, ' - ', P.PRODETALLE) AS PRONOMBRE, P.PROID, DV.DETCANTIDADVENTA, DV.DETTOTALVENTA, P.PROPRECIOVENTA, P.PROIVA
            FROM Detalle_ventas DV
                INNER JOIN Productos P ON (DV.DETPROCOD=P.PROID)
            WHERE DV.DETVENCOD=%s
            ORDER BY DV.DETCOD ASC",
            $codVenta
        );
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $this->db->close();
        $retorno = array();
        $totalVenta = 0;
        foreach ($resultado as $key) {
            $totalVenta = ($totalVenta + $key['DETTOTALVENTA']);
            array_push($retorno, array(
                "DETCOD" => $key['DETCOD'],
                "PROID" => $key['PROID'],
                "PRONOMBRE" => utf8_encode($key['PRONOMBRE']),
                "DETCANTIDADVENTA" => $key['DETCANTIDADVENTA'],
                "DETTOTALVENTA" => ($key['DETTOTALVENTA']),
                "PROPRECIOVENTA" => ($key['PROPRECIOVENTA']),
                "PROIVA" => ($key['PROIVA'])
            ));
        }
        return array("retorno" => $retorno, "totalVenta" => ($totalVenta));
    }
    
    public function eliminarProductosVenta($cantidad, $detCod, $proCod)
    {
        $retorno = elminarProductoVenta($cantidad, $detCod, $proCod);
        return $retorno;
    }

    public function crearVenta($cedula)
    {
        $codUsuario = obtenerCodigoUsuario($cedula);
        $sqlValidaVenta = sprintf(
            "SELECT V.VENCOD
            FROM Ventas V
            WHERE V.VENUSUCOD=%s
                AND V.VENESTADO=0",
            $codUsuario
        );
        $result = $this->db->query($sqlValidaVenta);
        $resultadoValidaVenta = $result->result_array();
        $result->free_result();
        if (count($resultadoValidaVenta) > 0) {
            $this->db->close();
            return array("msg" => "Ya tienes una venta creada.", "success" => false);
        } else {
            $sqlInserta = sprintf(
                "INSERT INTO `Ventas`(
                `VENOBSERVACION`, 
                `VENFECHA`, 
                `VENUSUCOD`, 
                `VENESTADO`) VALUES (
                    ' ',
                    '%s',
                    %s,
                    0
                )",
                date('Y-m-d'),
                $codUsuario
            );
            $result = $this->db->query($sqlInserta);
            $this->db->close();
            return ($result) ? array("msg" => "Acción realizada exitosamente.", "success" => true) : array("msg" => "Hubo un error al procesar la información.", "success" => false);
        }
    }

    public function cargarVentas($cedula, $funcion)
    {
        if ($funcion == 1) {
            $sql = sprintf("SELECT V.VENCOD, V.VENOBSERVACION, V.VENFECHA, CONCAT(U.USUNOMBRE, ' ', U.USUAPELLIDO) AS USUNOMBRE,
                            CASE 
                                WHEN V.VENESTADO=0 THEN 'NO FACTURADO'
                                WHEN V.VENESTADO=1 THEN 'FACTURADO'
                            END AS ESTADO
                            FROM Ventas V
                                INNER JOIN Usuarios U ON (V.VENUSUCOD=U.USUCOD)
                            ORDER BY V.VENESTADO ASC, V.VENFECHA DESC");
        }
        if ($funcion == 2) {
            $sql = sprintf(
                "SELECT V.VENCOD, V.VENOBSERVACION, V.VENFECHA, CONCAT(U.USUNOMBRE, ' ', U.USUAPELLIDO) AS USUNOMBRE,
                CASE 
                    WHEN V.VENESTADO=0 THEN 'NO FACTURADO'
                    WHEN V.VENESTADO=1 THEN 'FACTURADO'
                END AS ESTADO
                FROM Ventas V
                    INNER JOIN Usuarios U ON (V.VENUSUCOD=U.USUCOD)
                WHERE V.VENFECHA='%s'
                ORDER BY V.VENESTADO ASC, V.VENFECHA DESC",
                date('Y-m-d')
            );
        }
        if ($funcion == 3) {
            $codUsuario = obtenerCodigoUsuario($cedula);
            $sql = sprintf("SELECT V.VENCOD, V.VENOBSERVACION, V.VENFECHA, CONCAT(U.USUNOMBRE, ' ', U.USUAPELLIDO) AS USUNOMBRE,
                CASE 
                    WHEN V.VENESTADO=0 THEN 'NO FACTURADO'
                    WHEN V.VENESTADO=1 THEN 'FACTURADO'
                END AS ESTADO
                FROM Ventas V
                    INNER JOIN Usuarios U ON (V.VENUSUCOD=U.USUCOD)
                WHERE V.VENUSUCOD='%s'
                ORDER BY V.VENESTADO ASC", $codUsuario);
        }
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $retorno = array();
        foreach ($resultado as $key) {
            array_push($retorno, array(
                "VENCOD" => $key['VENCOD'],
                "VENOBSERVACION" => utf8_encode($key['VENOBSERVACION']),
                "VENFECHA" => $key['VENFECHA'],
                "USUNOMBRE" => utf8_encode($key['USUNOMBRE']),
                "ESTADO" => utf8_encode($key['ESTADO'])
            ));
        }
        $this->db->close();
        return $retorno;
    }

    public function guardarVenta($codVenta, $observacion)
    {
        $sqlActualiza = sprintf("UPDATE `Ventas` SET `VENOBSERVACION`='%s',`VENESTADO`= 1 WHERE VENCOD=$codVenta", utf8_encode(trim($observacion)));
        $result = $this->db->query($sqlActualiza);
        $this->db->close();
        return ($result) ? array("msg" => "Acción realizada exitosamente.", "success" => true) : array("msg" => "Hubo un error al procesar la información.", "success" => false);
    }

    public function consultarCodPro($codigo, $codBodega)
    {
        $sql = sprintf("SELECT CONCAT(P.PRONOMBRE, ' ' ,P.PRODETALLE, ' - ', (SELECT LABNOMBRE FROM Laboratorios WHERE LABID=P.LABID)) AS INFO, P.PROID
                        FROM Productos P
                        WHERE P.BODID=$codBodega AND  P.PROCODIGO = '$codigo' AND P.PROESTADO = 1
                        ");
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $this->db->close();
        $retorno = array();
        foreach ($resultado as $key) {
        array_push($retorno, array(
        "PROID" => $key['PROID'],
        "INFO" => $key['INFO']
        ));
        }
        return $retorno;
    }

    
    public function cargarResumenProductosVenta($cedula)
    {
        $cod = obtenerCodigoUsuario($cedula);
        $sql = sprintf(
            "SELECT DV.DETCOD, CONCAT(P.PRONOMBRE, ' - ', P.PRODETALLE) AS PRONOMBRE, P.PROID, DV.DETCANTIDADVENTA, DV.DETTOTALVENTA, P.PROPRECIOVENTA
            FROM Detalle_ventas DV
                INNER JOIN Ventas V ON (DV.DETVENCOD=V.VENCOD)
                INNER JOIN Productos P ON (DV.DETPROCOD=P.PROID)
            WHERE V.VENFECHA='%s' AND V.VENUSUCOD = %s
            ORDER BY DV.DETCOD ASC",
             date('Y-m-d'),
             $cod
        );
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $this->db->close();
        $retorno = array();
        $totalVenta = 0;
        foreach ($resultado as $key) {
            $totalVenta = ($totalVenta + $key['DETTOTALVENTA']);
            array_push($retorno, array(
                "DETCOD" => $key['DETCOD'],
                "PROID" => $key['PROID'],
                "PRONOMBRE" => utf8_encode($key['PRONOMBRE']),
                "DETCANTIDADVENTA" => $key['DETCANTIDADVENTA'],
                "DETTOTALVENTA" => number_format($key['DETTOTALVENTA']),
                "PROPRECIOVENTA" => number_format($key['PROPRECIOVENTA']),
            ));
        }
        return array("retorno" => $retorno, "totalVenta" => number_format($totalVenta));
    }
}