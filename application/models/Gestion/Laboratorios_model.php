<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laboratorios_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
  }

  public function obtenerLaboratorios(){
		$sql = $this->db->query("SELECT *
                             FROM Laboratorios
                             WHERE LABESTADO = 1
                             ORDER BY LABNOMBRE");
		$resultado = $sql->result_array();
		$sql->free_result();
		$this->db->close();
		return $resultado;
  }

  public function ingresarLaboratorio($parametros){
		$sql = "INSERT INTO `Laboratorios`(
                                    `LABNOMBRE`
                                    ) 
                                      VALUES (
                                        '$parametros[labnombre]'
                                    )";
		$r = $this->db->query($sql);
		$msg = "Error al guardar el laboratorio.";
		$success = false;
		$type = 'danger';
		if ($r === true) {
			$msg = "Guardado Exitoso.";
			$success = true;
			$type = 'success';
		}
		return array('msg' => $msg, 'success' => $success, 'type' => $type);
  }

  public function editarLaboratorio($parametros){
		$sql = "UPDATE `Laboratorios` 
						SET
              `LABNOMBRE`='$parametros[editlabnombre]'
						WHERE  
								`LABID`=$parametros[editlabid]";
		$r = $this->db->query($sql);
		$msg = "Error al editar Producto.";
		$success = false;
		$type = 'danger';
		if ($r === true) {
			$msg = "Edicion de producto realizado.";
			$success = true;
			$type = 'success';
		}
		return array('msg' => $msg, 'success' => $success, 'type' => $type);
  }
  public function eliminarLaboratorio($labid){
    $sql = "UPDATE `Laboratorios` SET `LABESTADO`= 0 WHERE LABID=$labid";
   $r = $this->db->query($sql);
   $msg = "Error al eliminar Laboratorio.";
   $success = false;
   $type = 'danger';
   if ($r === true) {
     $msg = "Eliminado de Laboratorio realizado.";
     $success = true;
     $type = 'success';
   }
   return array('msg' => $msg, 'success' => $success, 'type' => $type);
 }

}