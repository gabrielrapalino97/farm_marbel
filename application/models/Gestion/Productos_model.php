<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Productos_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
  }

  public function obtenerProductos(){
		$sql = $this->db->query("SELECT pr.*,lb.LABNOMBRE,bd.BODNOMBRE
                             FROM Productos pr INNER JOIN Laboratorios lb ON pr.LABID=lb.LABID INNER JOIN Bodegas bd ON pr.BODID=bd.BODID
                             WHERE PROESTADO=1
                             ORDER BY pr.PRONOMBRE");
		$resultado = $sql->result_array();
		$sql->free_result();
		$this->db->close();
		return $resultado;
  }

  public function stockBajo(){
		$sql = $this->db->query("SELECT pr.*,lb.LABNOMBRE,bd.BODNOMBRE
                             FROM Productos pr INNER JOIN Laboratorios lb ON pr.LABID=lb.LABID INNER JOIN Bodegas bd ON pr.BODID=bd.BODID
                             WHERE PROESTADO=1 AND pr.PROCANTIDAD <= 15
                             ORDER BY pr.PRONOMBRE");
		$resultado = $sql->result_array();
		$sql->free_result();
		$this->db->close();
		return $resultado;
  }

  public function cargarLaboratorios(){
    $sql = $this->db->query("SELECT *
                             FROM Laboratorios 
                             WHERE LABESTADO=1
                            ");
    $resultado = $sql->result_array();
    $sql->free_result();
    $this->db->close();
    return $resultado;
  }

  public function cargarBodegas(){
    $sql = $this->db->query("SELECT * 
                             FROM Bodegas
                             WHERE BODESTADO=1
                            ");
    $resultado = $sql->result_array();
    $sql->free_result();
    $this->db->close();
    return $resultado;
  }

  public function ingresarProducto($parametros){

    $sqlValidar = sprintf("SELECT `PROCODIGO` FROM `Productos` WHERE `PROCODIGO`='$parametros[procodigo]' AND `PROESTADO`= 1");
    $result = $this->db->query($sqlValidar);
    $resultadoValidar = $result->result_array();
    $result->free_result();
    if (count($resultadoValidar) > 1) {
        return array("msg" => "Este Codigo ya se encuentra registrado", "success" => false, 'type' => 'danger');
    }

		$sql = "INSERT INTO `Productos`(
                                    `PRONOMBRE`, 
                                    `LABID`, 
                                    `BODID`, 
                                    `PRODETALLE`, 
                                    `PROCODIGO`, 
                                    `PROCANTIDAD`, 
                                    `PROPRECIOVENTA`,
                                    `PROCOMPRA`,
                                    `PRORUTA`,
                                    `PROIVA`
                                    ) 
                                      VALUES (
                                        '$parametros[pronombre]',
                                        $parametros[prolabid],
                                        $parametros[probodid],
                                        '$parametros[prodetalle]',
                                        '$parametros[procodigo]',
                                        $parametros[procantidad],
                                        $parametros[proprecioventa],
                                        $parametros[procompra],
                                        '$parametros[proruta]',
                                        '$parametros[proiva]'
                                    )";
		$r = $this->db->query($sql);
		$msg = "Error al guardar producto.";
		$success = false;
		$type = 'danger';
		if ($r === true) {
			$msg = "Guardado Exitoso.";
			$success = true;
			$type = 'success';
		}
		return array('msg' => $msg, 'success' => $success, 'type' => $type);
  }

  public function cargarLaboratoriosEdit($editprolabid){
    $sql = $this->db->query("SELECT *
                             FROM Laboratorios
                             WHERE LABID<>$editprolabid AND LABESTADO=1
                            ");
    $resultado = $sql->result_array();
    $sql->free_result();
    $this->db->close();
    return $resultado;
  }

  public function cargarBodegasEdit($editprobodid){
    $sql = $this->db->query("SELECT *
                             FROM Bodegas
                             WHERE BODID=$editprobodid
                             UNION
                             SELECT *
                             FROM Bodegas
                             WHERE BODID<>$editprobodid AND BODESTADO=1
                            ");
    $resultado = $sql->result_array();
    $sql->free_result();
    $this->db->close();
    return $resultado;
  }
  
  public function editarProducto($parametros){

		$sql = "UPDATE `Productos` 
						SET
              `PRONOMBRE`='$parametros[editpronombre]',
              `LABID`=$parametros[editprolabid],
              `BODID`=$parametros[editprobodid],
              `PRODETALLE`='$parametros[editprodetalle]',
              `PROCODIGO`='$parametros[editprocodigo]',
              `PROCANTIDAD`=$parametros[editprocantidad],
              `PROCOMPRA`=$parametros[editpropreciocompra],
              `PRORUTA`='$parametros[editproruta]',
              `PROPRECIOVENTA`=$parametros[editproprecioventa],
              `PROIVA`=$parametros[editproiva]
						WHERE  
								`PROID`=$parametros[editproid]";
		$r = $this->db->query($sql);
		$msg = "Error al editar Producto.";
		$success = false;
		$type = 'danger';
		if ($r === true) {
			$msg = "Edicion de producto realizado.";
			$success = true;
			$type = 'success';
		}
		return array('msg' => $msg, 'success' => $success, 'type' => $type);
  }

  public function eliminarProducto($proid){
   	$sql = "UPDATE `Productos` SET `PROESTADO`=0 WHERE PROID=$proid";
		$r = $this->db->query($sql);
		$msg = "Error al eliminar Producto.";
		$success = false;
		$type = 'danger';
		if ($r === true) {
			$msg = "Eliminado de producto realizado.";
			$success = true;
			$type = 'success';
		}
		return array('msg' => $msg, 'success' => $success, 'type' => $type);
	}


}