<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bodegas_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
  }

  public function obtenerBodegas(){
		$sql = $this->db->query("SELECT *
                             FROM Bodegas
                             WHERE BODESTADO = 1
                             ORDER BY BODNOMBRE");
		$resultado = $sql->result_array();
		$sql->free_result();
		$this->db->close();
		return $resultado;
  }

  public function ingresarBodega($parametros){
		$sql = "INSERT INTO `Bodegas`(
                                    `BODNOMBRE`
                                    ) 
                                      VALUES (
                                        '$parametros[bodnombre]'
                                    )";
		$r = $this->db->query($sql);
		$msg = "Error al guardar el Bodega.";
		$success = false;
		$type = 'danger';
		if ($r === true) {
			$msg = "Guardado Exitoso.";
			$success = true;
			$type = 'success';
		}
		return array('msg' => $msg, 'success' => $success, 'type' => $type);
  }

  public function editarBodega($parametros){
		$sql = "UPDATE `Bodegas` 
						SET
              `BODNOMBRE`='$parametros[editbodnombre]'
						WHERE  
								`BODID`=$parametros[editbodid]";
		$r = $this->db->query($sql);
		$msg = "Error al editar Producto.";
		$success = false;
		$type = 'danger';
		if ($r === true) {
			$msg = "Edicion de producto realizado.";
			$success = true;
			$type = 'success';
		}
		return array('msg' => $msg, 'success' => $success, 'type' => $type);
  }
  public function eliminarBodega($bodid){
    $sql = "UPDATE `Bodegas` SET `BODESTADO`= 0 WHERE BODID=$bodid";
   $r = $this->db->query($sql);
   $msg = "Error al eliminar Bodega.";
   $success = false;
   $type = 'danger';
   if ($r === true) {
     $msg = "Eliminado de Bodega realizado.";
     $success = true;
     $type = 'success';
   }
   return array('msg' => $msg, 'success' => $success, 'type' => $type);
 }

}