<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Persona_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function ingresarPersona($parametros)
	{
		$sql = "INSERT INTO `Personas`(`PERNIT`,
                                    `PERRAZON`,
                                    `PERCORREO`,
                                    `PERDIRECCION`,
                                    `PERTELEFONO`)
                                    VALUES (
                                        $parametros[nit],
                                        '$parametros[nombreRazon]',
                                        '$parametros[correo]',
                                        '$parametros[direccion]',
                                        '$parametros[telefono]'
                                    )";
		$r = $this->db->query($sql);
		$msg = "Se ha producido un error al guardar la informaci&oacute;n, intentelo nuevamente.";
		$success = false;
		$type = 'danger';
		if ($r === true) {
			$msg = "Acci&oacute;n realizada &eacute;xitosamente.";
			$success = true;
			$type = 'success';
		}
		return array('msg' => $msg, 'success' => $success, 'type' => $type);
	}

	public function actualizarPersona($data)
	{
		$sql = "UPDATE `Personas` 
						SET
								`PERNIT`=$data[nitEditarPersona],
								`PERRAZON`='$data[nombreEditarPersona]',
								`PERCORREO`='$data[correoEditarPersona]',
								`PERDIRECCION`='$data[direccionEditarPersona]',
								`PERTELEFONO`='$data[telefonoEditarPersona]'
						WHERE  
								`PERCOD`=$data[cod]";
		$r = $this->db->query($sql);
		$msg = "Se ha producido un error al guardar la informaci&oacute;n, intentelo nuevamente.";
		$success = false;
		$type = 'danger';
		if ($r === true) {
			$msg = "Acci&oacute;n realizada &eacute;xitosamente.";
			$success = true;
			$type = 'success';
		}
		return array('msg' => $msg, 'success' => $success, 'type' => $type);
	}

	public function obtenerPersonas()
	{
		$sql = $this->db->query("SELECT ap.PERCOD, ap.PERNIT, ap.PERRAZON, ap.PERCORREO, ap.PERDIRECCION, ap.PERTELEFONO
                            FROM Personas ap 
                            WHERE ap.PERESTADO=1
                            ORDER BY ap.PERRAZON");
		$resultado = $sql->result_array();
		$sql->free_result();
		$this->db->close();
		$retorno=array();
		foreach ($resultado as $key) {
			array_push($retorno, array(
				"PERCOD"=>$key['PERCOD'],
				"PERNIT"=>$key['PERNIT'],
				"PERRAZON"=>$key['PERRAZON'],
				"PERCORREO"=>$key['PERCORREO'],
				"PERDIRECCION"=>$key['PERDIRECCION'],
        "PERTELEFONO"=>$key['PERTELEFONO']
			));
		}
		return $retorno;
	}

  public function eliminarPersona($idPersona)
	{
		$sql = "UPDATE `Personas` 
						SET
								`PERESTADO`=2
						WHERE  
								`PERCOD`=$idPersona";
		$r = $this->db->query($sql);
		$msg = "Se ha producido un error al guardar la informaci&oacute;n, intentelo nuevamente.";
		$success = false;
		$type = 'danger';
		if ($r === true) {
			$msg = "Acci&oacute;n realizada &eacute;xitosamente.";
			$success = true;
			$type = 'success';
		}
		return array('msg' => $msg, 'success' => $success, 'type' => $type);
	}
}
