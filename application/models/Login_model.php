<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends CI_Model
{
    public function obtenerPermisos($usuario)
    {
        $sql = sprintf("SELECT *
                        FROM usuario u INNER JOIN Permisos p on p.PERID=pu.PERCOD
                        WHERE u.Nickname='$usuario'");
        $query = $this->db->query($sql);
        $data = $query->result_array();
        $query->free_result();
    }

    public function login($usuario, $clave)
    {
        if ($usuario == null || trim($usuario) == '' || $clave == null || trim($clave) == '') {
            return FALSE;
        }

        $query = sprintf("SELECT `USUESTADO`
                          FROM `Usuarios` 
                          WHERE USUCEDULA=$usuario");
		$result = $this->db->query($query);
		$resultado = $result->result_array();
		$result->free_result();
		if ($resultado[0]['USUESTADO'] == 2) {
			$sql = "SELECT * FROM abg_servicio WHERE SEDECOD=0";
			return false;
		}

        $result = FALSE;
        $sql = sprintf("SELECT * FROM Usuarios WHERE USUCEDULA='$usuario' AND USUPASSWORD='%s'", md5($clave));
        $query = $this->db->query($sql);
        $data = $query->result_array();
		$query->free_result();
        if (count($data) > 0) {
            foreach ($data as $campos) {
                $this->session->set_userdata('usuario', $campos['USUNOMBRE'] . " " . $campos['USUAPELLIDO']);
                $this->session->set_userdata('auth', TRUE);
                $this->session->set_userdata('codigo', $campos['USUCEDULA']);
                $this->session->set_userdata('expira', time() + (1440*60));
            }
            $result = TRUE;
        }
        return $result;
    }

    public function logout($url = '/..')
    {
        $this->session->sess_destroy();
        if ($url !== false) {
            redirect(base_url(), "refresh");
        }
    }

    public function getUsuario()
    {
        return $this->session->userdata("usuario");
    }
}
