<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Compras_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function cargarProductosBodega($codBodega)
    {
        $sql = sprintf("SELECT P.PROID, P.PRONOMBRE, L.LABNOMBRE, P.PRODETALLE, P.PROCODIGO, P.PROCANTIDAD, P.PROPRECIOVENTA, L.LABNOMBRE, P.PROCOMPRA, P.PROIVA
                        FROM Productos P 
                            INNER JOIN Bodegas B ON (P.BODID=B.BODID)
                            INNER JOIN Laboratorios L ON (P.LABID=L.LABID)
                        WHERE B.BODID=%s
                            AND P.PROESTADO=1", $codBodega);
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $this->db->close();
        $retorno = array();
        foreach ($resultado as $key) {
            array_push($retorno, array(
                "PROID" => $key['PROID'],
                "PRONOMBRE" => utf8_encode($key['PRONOMBRE']),
                "LABNOMBRE" => utf8_encode($key['LABNOMBRE']),
                "PRODETALLE" => utf8_encode($key['PRODETALLE']),
                "PROCODIGO" => $key['PROCODIGO'],
                "PROCANTIDAD" => $key['PROCANTIDAD'],
                "PROPRECIOVENTA" => $key['PROPRECIOVENTA'],
                "PROCOMPRA" => $key['PROCOMPRA'],
                "PROIVA" => $key['PROIVA']
            ));
        }
        return $retorno;
    }

    public function agregarProducto($datos)
    {
        $retorno = registrarCompraProducto($datos['proCod'], $datos['cantidadentrada'], $datos['entradaCod'], $datos['costoentrada'], $datos['precioVenta'], $datos['detproiva']);
        return $retorno;
    }
    
    public function cargarProductosVenta($codVenta)
    {
        $sql = sprintf(
            "SELECT DC.*, CONCAT(P.PRONOMBRE, ' - ', P.PROCODIGO) AS PRONOMBRE, P.PROID,P.PROPRECIOVENTA, P.PROCOMPRA, P.PRODETALLE,(SELECT LABNOMBRE FROM Laboratorios WHERE LABID=P.LABID) AS LABNOMBRE
            FROM Detalle_compras DC
                INNER JOIN Productos P ON (DC.DETCPROCOD=P.PROID)
            WHERE DC.DETCCOMCOD=%s
            ORDER BY DC.DETCCOD ASC",
            $codVenta
        );
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $this->db->close();
        $retorno = array();
        $totalVenta = 0;
        foreach ($resultado as $key) {
            $totalVenta = ($totalVenta + $key['DETCCOSTOPROTOTAL']);
            array_push($retorno, array(
                "DETCCOD" => $key['DETCCOD'],
                "PROID" => $key['PROID'],
                "PRONOMBRE" => utf8_encode($key['PRONOMBRE']),
                "DETCCANTIDADPRO" => $key['DETCCANTIDADPRO'],
                "DETCCOSTOPRO" => $key['DETCCOSTOPRO'],
                "DETCPRECIOVENTAPRO" => $key['DETCPRECIOVENTAPRO'],
                "DETCCOMCOD" => $key['DETCCOMCOD'],
                "DETCCOSTOPROTOTAL" => number_format($key['DETCCOSTOPROTOTAL']),
                "ROLLBACK" =>  $key['ROLLBACK'],
                "ROLLBACKCOM" =>  $key['ROLLBACKCOM'],
                "PRODETALLE" =>  $key['PRODETALLE'],
                "LABNOMBRE" =>  $key['LABNOMBRE'],
                "ROLLBACKIVA" =>  $key['ROLLBACKIVA'],
                "DETPROIVA" =>  $key['DETPROIVA']
            ));
        }
        return array("retorno" => $retorno, "totalVenta" => number_format($totalVenta));
    }

    public function eliminarProductosVenta($cantidad, $detCod, $proCod, $precioAntiguo, $costoantiguo, $rollbackiva)
    {
        $retorno = elminarProductoCompra($cantidad, $detCod, $proCod, $precioAntiguo, $costoantiguo, $rollbackiva);
        return $retorno;
    }

    public function crearVenta($cedula)
    {
        $codUsuario = obtenerCodigoUsuario($cedula);
        $sqlValidaVenta = sprintf(
            "SELECT C.COMCOD
            FROM Compras C
            WHERE C.COMUSUCOD=%s
                AND C.COMESTADO=0",
            $codUsuario
        );
        $result = $this->db->query($sqlValidaVenta);
        $resultadoValidaVenta = $result->result_array();
        $result->free_result();
        if (count($resultadoValidaVenta) > 0) {
            $this->db->close();
            return array("msg" => "Ya tienes una entrada creada.", "success" => false);
        } else {
            $sqlInserta = sprintf(
                "INSERT INTO `Compras`(
                `COMOBSERVACION`, 
                `COMFECHA`, 
                `COMUSUCOD`, 
                `COMESTADO`) VALUES (
                    ' ',
                    '%s',
                    %s,
                    0
                )",
                date('Y-m-d'),
                $codUsuario
            );
            $result = $this->db->query($sqlInserta);
            $this->db->close();
            return ($result) ? array("msg" => "Acción realizada exitosamente.", "success" => true) : array("msg" => "Hubo un error al procesar la información.", "success" => false);
        }
    }

    public function cargarVentas($cedula, $funcion)
    {
        if ($funcion == 1) {
            $sql = sprintf("SELECT C.COMCOD, C.COMOBSERVACION, C.COMFECHA, CONCAT(U.USUNOMBRE, ' ', U.USUAPELLIDO) AS USUNOMBRE, C.COMPROVEEDOR,
                            CASE 
                                WHEN C.COMESTADO=0 THEN 'NO FACTURADO'
                                WHEN C.COMESTADO=1 THEN 'FACTURADO'
                            END AS ESTADO
                            FROM Compras C
                                INNER JOIN Usuarios U ON (C.COMUSUCOD=U.USUCOD)
                            ORDER BY C.COMESTADO ASC");
        }
        if ($funcion == 2) {
            $sql = sprintf(
                "SELECT C.COMCOD, C.COMOBSERVACION, C.COMFECHA, CONCAT(U.USUNOMBRE, ' ', U.USUAPELLIDO) AS USUNOMBRE, C.COMPROVEEDOR,
                CASE 
                    WHEN C.COMESTADO=0 THEN 'NO FACTURADO'
                    WHEN C.COMESTADO=1 THEN 'FACTURADO'
                END AS ESTADO
                FROM Compras C
                    INNER JOIN Usuarios U ON (C.COMUSUCOD=U.USUCOD)
                WHERE C.COMFECHA='%s'
                ORDER BY C.COMESTADO ASC",
                date('Y-m-d')
            );
        }
        if ($funcion == 3) {
            $codUsuario = obtenerCodigoUsuario($cedula);
            $sql = sprintf("SELECT C.COMCOD, C.COMOBSERVACION, C.COMFECHA, CONCAT(U.USUNOMBRE, ' ', U.USUAPELLIDO) AS USUNOMBRE, C.COMPROVEEDOR,
                CASE 
                    WHEN C.COMESTADO=0 THEN 'NO FACTURADO'
                    WHEN C.COMESTADO=1 THEN 'FACTURADO'
                END AS ESTADO
                FROM Compras C
                    INNER JOIN Usuarios U ON (C.COMUSUCOD=U.USUCOD)
                WHERE C.COMUSUCOD='%s'
                ORDER BY C.COMESTADO ASC", $codUsuario);
        }
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $retorno = array();
        foreach ($resultado as $key) {
            array_push($retorno, array(
                "COMCOD" => $key['COMCOD'],
                "COMOBSERVACION" => utf8_encode($key['COMOBSERVACION']),
                "COMFECHA" => $key['COMFECHA'],
                "USUNOMBRE" => utf8_encode($key['USUNOMBRE']),
                "ESTADO" => utf8_encode($key['ESTADO']),
                "COMPROVEEDOR" => $key['COMPROVEEDOR']
            ));
        }
        $this->db->close();
        return $retorno;
    }

    public function guardarVenta($codVenta, $observacion, $proveedor)
    {
        $sqlActualiza = sprintf("UPDATE `Compras` SET `COMOBSERVACION`='%s',`COMESTADO`= 1, `COMPROVEEDOR`='%s'  WHERE COMCOD=$codVenta", utf8_encode(trim($observacion)),$proveedor);
        $result = $this->db->query($sqlActualiza);
        $this->db->close();
        return ($result) ? array("msg" => "Acción realizada exitosamente.", "success" => true) : array("msg" => "Hubo un error al procesar la información.", "success" => false);
    }

    public function cargarLaboratorios(){
        $sql = $this->db->query("SELECT *
                                 FROM Laboratorios 
                                 WHERE LABESTADO=1
                                 ORDER BY LABNOMBRE
                                ");
        $resultado = $sql->result_array();
        $sql->free_result();
        $this->db->close();
        return $resultado;
      }
    
      public function cargarBodegas(){
        $sql = $this->db->query("SELECT * 
                                 FROM Bodegas
                                 WHERE BODESTADO=1
                                 ORDER BY BODNOMBRE
                                ");
        $resultado = $sql->result_array();
        $sql->free_result();
        $this->db->close();
        return $resultado;
      }
    
      public function ingresarProducto($parametros){

        $sqlValidar = sprintf("SELECT `PROCODIGO` FROM `Productos` WHERE `PROCODIGO`='$parametros[procodigo]' AND `PROESTADO`= 1");
        $result = $this->db->query($sqlValidar);
        $resultadoValidar = $result->result_array();
        $result->free_result();
        if (count($resultadoValidar) > 1) {
            return array("msg" => "Este Codigo ya se encuentra registrado", "success" => false, 'type' => 'danger');
        }

            $sql = "INSERT INTO `Productos`(
                                        `PRONOMBRE`, 
                                        `LABID`, 
                                        `BODID`, 
                                        `PRODETALLE`, 
                                        `PROCODIGO`, 
                                        `PROCANTIDAD`, 
                                        `PROPRECIOVENTA`,
                                        `PROCOMPRA`,
                                        `PRORUTA`,
                                        `PROIVA`
                                        ) 
                                          VALUES (
                                            '$parametros[pronombre]',
                                            $parametros[prolabid],
                                            $parametros[probodid],
                                            '$parametros[prodetalle]',
                                            '$parametros[procodigo]',
                                            0,
                                            0,
                                            0,
                                            '$parametros[proruta]',
                                            0
                                        )";
            $r = $this->db->query($sql);
            $msg = "Error al guardar producto.";
            $success = false;
            $type = 'danger';
            if ($r === true) {
                $msg = "Guardado Exitoso.";
                $success = true;
                $type = 'success';
            }
            return array('msg' => $msg, 'success' => $success, 'type' => $type);
      }

      
      public function ingresarLaboratorio($parametros){
        $sql = "INSERT INTO `Laboratorios`(
                                    `LABNOMBRE`
                                    ) 
                                      VALUES (
                                        '$parametros[labnombre]'
                                    )";
        $r = $this->db->query($sql);
        $msg = "Error al guardar producto.";
        $success = false;
        $type = 'danger';
        if ($r === true) {
            $msg = "Guardado Exitoso.";
            $success = true;
            $type = 'success';
        }
        return array('msg' => $msg, 'success' => $success, 'type' => $type);
  }
  
  public function consultarCodPro($codigo, $codBodega)
    {
        $sql = sprintf("SELECT CONCAT(P.PRONOMBRE, ' ' ,P.PRODETALLE, ' - ', (SELECT LABNOMBRE FROM Laboratorios WHERE LABID=P.LABID)) AS INFO, P.PROID,  P.PROPRECIOVENTA, P.PROCOMPRA
                        FROM Productos P
                        WHERE P.BODID=$codBodega AND  P.PROCODIGO = '$codigo' AND P.PROESTADO = 1
                        ");
        $result = $this->db->query($sql);
        $resultado = $result->result_array();
        $result->free_result();
        $this->db->close();
        $retorno = array();
        foreach ($resultado as $key) {
        array_push($retorno, array(
        "PROID" => $key['PROID'],
        "INFO" => $key['INFO'],
        "PROPRECIOVENTA" => $key['PROPRECIOVENTA'],
        "PROCOMPRA" => $key['PROCOMPRA']
        ));
        }
        return $retorno;
    }

}   
    