<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class productos_models extends CI_Model {
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function insertar($nombre, $valor, $cantidad){
    $sql="INSERT INTO `productos`(`pro_nombre`, `pro_precio`, `pro_cantidad`) VALUES ('$nombre', $valor, $cantidad)";
    $r=$this->db->query($sql);

    $msg="Se ha producido un error al guardar la informaci&oacute;n, intentelo nuevamente.";
    $success=false;
    $type='danger';
    if ($r===true) {
      $msg="Acci&oacute;n realizada &eacute;xitosamente.";
      $success=true;
      $type='success';
    }
    return array('msg'=>$msg,'success'=>$success,'type'=>$type);
  }

  public function actualizar($id, $nombre, $valor, $cantidad){
    $sql="UPDATE `productos` SET `pro_nombre`='$nombre',`pro_precio`=$valor,`pro_cantidad`=$cantidad WHERE pro_id = $id";
    $r=$this->db->query($sql);

    $msg="Se ha producido un error al actualizar la informaci&oacute;n, intentelo nuevamente.";
    $success=false;
    $type='danger';
    if ($r===true) {
      $msg="Acci&oacute;n realizada &eacute;xitosamente.";
      $success=true;
      $type='success';
    }
    return array('msg'=>$msg,'success'=>$success,'type'=>$type);
  }

  public function obtener_producto(){
    $sql =$this->db->query("SELECT p.pro_id AS id_producto, p.pro_nombre AS nombre_producto, p.pro_precio AS valor_producto, p.pro_cantidad as cantidad
                            FROM productos p");
    $resultado = $sql->result_array();
    $sql->free_result();
    return $resultado;
  }

  public function eliminar($id){
    $sql="DELETE FROM `productos` WHERE pro_id = $id";
    $r=$this->db->query($sql);

    $msg="Se ha producido un error al guardar la informaci&oacute;n, intentelo nuevamente.";
    $success=false;
    $type='danger';
    if ($r===true) {
      $msg="Acci&oacute;n realizada &eacute;xitosamente.";
      $success=true;
      $type='success';
    }
    return array('msg'=>$msg,'success'=>$success,'type'=>$type);
  }


}