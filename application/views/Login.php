<!DOCTYPE html>
<html lang="en">

<head>
	<title>Login - Plataforma</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>public/login/images/icons/icon.png" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/login/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/login/vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/login/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/login/vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/login/css/main.css">
	<!--===============================================================================================-->

	<script type="text/javascript">
		var BASE_URL = "<?php echo base_url(); ?>";
		var FECHA_ACTUAL = "<?php echo date("Y-m-d"); ?>";
	</script>
</head>

<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?php echo base_url(); ?>public/login/images/img-01.png" alt="IMG">
				</div>

				<div class="">
					<span class="login100-form-title">
						Bienvenido
					</span>

					<div class="wrap-input100">
						<input class="input100" type="text" name="codigo" placeholder="N° Documento">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100">
						<input class="input100" type="password" name="contra" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<div class="container-login100-form-btn">
						<input class="login100-form-btn text-center" style="cursor:pointer;" type="button" value="Iniciar" name="iniciarSesion">
					</div>
					
				</div>
			</div>
		</div>
	</div>




	<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>public/login/vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>public/login/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url(); ?>public/login/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>public/login/vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>public/login/vendor/tilt/tilt.jquery.min.js"></script>
	<script>
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
	<!--===============================================================================================-->
	<script>
		$.getScript('<?php echo base_url(); ?>assets/js/shared/Util.js');
		$.getScript('<?php echo base_url(); ?>assets/js/shared/helpers.js');
		$.getScript("<?php echo base_url(); ?>assets/js/modulos/login.js");
	</script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.growl.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.bootstrap-growl.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootbox.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-notify.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.base64.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/loading.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart/Chart.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart/Chart.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart/utils.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-treeview.min.js"></script>
</body>

</html>
