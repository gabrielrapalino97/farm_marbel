<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<title>Plataforma</title>
<div class="col-xs-12 col-sm-12 col-md-12" id="panelHome" style="margin-top:10px;">
    <div class="panel panel-primary home">
        <div class="panel-heading">
            <h3 class="panel-title"><a href="<?php echo  base_url('Home'); ?>"
                    style="text-decoration:none;"></a>Plataforma
            </h3>
        </div>
        <div class="panel-body" autoCal="true" formulacal="height-70" style="overflow-y:auto;">
            <div id="opciones" autoCal="true" formulacal="height-100">
                <!--CONTENIDO  -->
                <div class="container">
                    <div class="row">

                        <?php if (permisosModulos('WEB_OPERAR')) { ?>
                        <div align="center" class="col-xs-6 col-sm-3 col-md-3 col-lg-3 img"
                            style="min-height: 150px; max-height:150px">
                            <div style="padding-top:10px"></div>
                            <a href="<?php echo  base_url("Ventas/Menu"); ?>" style="text-decoration:none; color:#333;">
                                <font style="cursor: pointer;">
                                    <img src="<?php echo  base_url(); ?>public/img/venta.png" alt="" height="80"
                                        width="80">
                                    <div>Ventas</div>
                                </font>
                            </a>
                        </div>
                        <?php } ?>

                        <?php if (permisosModulos('WEB_ADMINISTRAR')) { ?>
                        <div align="center" class="col-xs-6 col-sm-3 col-md-3 col-lg-3 img"
                            style="min-height: 150px; max-height:150px">
                            <div style="padding-top:10px"></div>
                            <a href="<?php echo  base_url("Compras/Menu"); ?>"
                                style="text-decoration:none; color:#333;">
                                <font style="cursor: pointer;">
                                    <img src="<?php echo  base_url(); ?>public/img/entradas.png" alt="" height="80"
                                        width="80">
                                    <div>Entrada de Productos</div>
                                </font>
                            </a>
                        </div>
                        <?php } ?>

                        <?php if (permisosModulos('WEB_ADMINISTRAR')) { ?>
                        <div align="center" class="col-xs-6 col-sm-3 col-md-3 col-lg-3 img"
                            style="min-height: 150px; max-height:150px">
                            <div style="padding-top:10px"></div>
                            <a href="<?php echo  base_url("Gestion/Home_gestion"); ?>"
                                style="text-decoration:none; color:#333;">
                                <font style="cursor: pointer;">
                                    <img src="<?php echo  base_url(); ?>public/img/inv.png" alt="" height="80"
                                        width="80">
                                    <div>Gestion</div>
                                </font>
                            </a>
                        </div>
                        <?php } ?>

                        <?php if (permisosModulos('WEB_ADMINISTRAR')) { ?>
                        <div align="center" class="col-xs-6 col-sm-3 col-md-3 col-lg-3 img"
                            style="min-height: 150px; max-height:150px">
                            <div style="padding-top:10px"></div>
                            <a href="<?php echo  base_url("Persona/Persona"); ?>"
                                style="text-decoration:none; color:#333;">
                                <font style="cursor: pointer;">
                                    <img src="<?php echo  base_url(); ?>public/img/man.png" alt="" height="80"
                                        width="80">
                                    <div>Clientes</div>
                                </font>
                            </a>
                        </div>
                        <?php } ?>

                        <?php if (permisosModulos('WEB_ADMINISTRAR')) { ?>
                        <div align="center" class="col-xs-6 col-sm-3 col-md-3 col-lg-3 img"
                            style="min-height: 150px; max-height:150px">
                            <div style="padding-top:10px"></div>
                            <a href="<?php echo  base_url("Reportes/Home_reporte"); ?>"
                                style="text-decoration:none; color:#333;">
                                <font style="cursor: pointer;">
                                    <img src="<?php echo  base_url(); ?>public/img/vercuali.png" alt="" height="80"
                                        width="80">
                                    <div>Reportes</div>
                                </font>
                            </a>
                        </div>
                        <?php } ?>

                        <?php if (permisosModulos('WEB_ADMINISTRAR')) { ?>
                        <div align="center" class="col-xs-6 col-sm-3 col-md-3 col-lg-3 img"
                            style="min-height: 150px; max-height:150px">
                            <div style="padding-top:10px"></div>
                            <a href="<?php echo  base_url("Usuarios/Menu"); ?>"
                                style="text-decoration:none; color:#333;">
                                <font style="cursor: pointer;">
                                    <img src="<?php echo  base_url(); ?>public/img/certificado.png" alt="" height="80"
                                        width="80">
                                    <div>Administrar Usuarios</div>
                                </font>
                            </a>
                        </div>
                        <?php } ?>


                    </div>

                    <div class="row">

                    </div>
                </div>
                <!--CONTENIDO  -->
            </div>
        </div>
    </div>
</div>