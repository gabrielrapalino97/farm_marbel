<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<title>Ingreso de Productos</title>
<div class="col-xs-12 col-sm-12 col-md-12" id="panelHome" style="margin-top:10px;">
    <div class="panel panel-primary home">
        <div class="panel-heading">
            <h3 class="panel-title"><a href="<?php echo  base_url('Home'); ?>" style="text-decoration:none;"><i
                        style="margin-right:30px;" class="icon icon-undo2"></i></a> Administraci&oacute;n de Entradas
            </h3>
        </div>
        <div class="panel-body" autoCal="true" formulacal="height-70" style="overflow-y:auto;">
            <div id="opciones" autoCal="true" formulacal="height-100">
                <!-- Contenido -->
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="btn-group" role="group" aria-label="...">
                                <button type="button" class="btn btn-warning" name="btnCrearVenta">Crear
                                    Entrada</button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="btn-group" role="group" aria-label="...">
                                <button type="button" class="btn btn-success" name="btnCargarFacturasAll">Todas las
                                    facturas</button>
                                <button type="button" class="btn btn-primary" name="btnCargarFacturasHoy">Facturas
                                    hoy</button>
                                <button type="button" class="btn btn-info" name="btnCargarMisFacturas">Mis
                                    facturas</button>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="table-responsive" autoCal="true" formulacal="height-280"
                                style="overflow-y: auto; border: 1px solid #eeeeee;">
                                <table class="table table-sm table-hover" name="tablaVentas" id="tablaVentas">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">FECHA</th>
                                            <th scope="col">USUARIO</th>
                                            <th scope="col">PROVEEDOR</th>
                                            <th scope="col">ESTADO</th>
                                            <th scope="col">OPCION</th>
                                        </tr>
                                    </thead>
                                    <tbody name="cuerpoTablaVentas">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Contenido -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL VER VENTA -->
<div class="modal fade" id="modalCrearVenta" name="modalCrearVenta" data-backdrop="static" data-keyboard="true"
    tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width:99%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Ingreso</h4>
            </div>
            <div class="modal-body" autoCal="true" formulacal="height-120" style="overflow-y: auto; color:#337ab7;">
                <div class="container">
                    <input type="hidden" name="ventaCod">
                    <div class="row">
                        <div class="col-12 col-sm-3 col-md-3 form-group">
                            <label for="">FECHA DE CREACION</label>
                            <input type="text" readonly name="fechaVenta" class="form-control">
                        </div>
                        <div class="col-12 col-sm-3 col-md-3 form-group">
                            <label for="">USUARIO</label>
                            <input type="text" readonly name="ventaUsuario" class="form-control">
                        </div>
                        <div class="col-12 col-sm-3 col-md-3 form-group">
                            <label for="">TOTAL ACTUAL</label>
                            <input type="text" readonly name="ventaTotal" class="form-control">
                        </div>
                        <div class="col-12 col-sm-3 col-md-3 form-group">
                            <label for="">ESTADO</label>
                            <input type="text" readonly name="ventaEstado" class="form-control">
                        </div>
                        <div class="col-12 col-sm-3 col-md-3 form-group">
                            <label for="">PROVEEDOR</label>
                            <input type="text" name="venproveedor" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 form-group">
                            <label for="">OBSERVACION</label>
                            <textarea name="textObservacion" cols="15" maxlength="50" class="form-control"
                                rows="3"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 col-sm-3 col-md-3 form-group">
                            <button name="crearProducto" type="button" class="btn btn-primary btn-lg btn-block">Crear
                                Producto</button>
                        </div>
                    </div>

                    <div name="cuerpoBodegaProducto">
                        <div class="row">
                            <div class="col-12 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <label for="">Bodega</label>
                                    <select name="sBodega" class="form-control">
                                        <option value="">Seleccione</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                <h4 class="text-muted text-left">Productos en Bodega</h4>
                                <input type="text" name="inputBuscarProductoBodega" id="inputBuscarProductoBodega"
                                    class="form-control" placeholder="Buscar..."
                                    onkeyup="doSearch('tablaProductosBodega','inputBuscarProductoBodega')"
                                    onkeypress="pulsar(event)">
                                <div class="table-responsive" autoCal="true" formulacal="height-250"
                                    style="overflow-y: auto; border: 1px solid #eeeeee;">
                                    <table class="table table-sm table-hover" name="tablaProductosBodega"
                                        id="tablaProductosBodega">
                                        <thead>
                                            <tr>
                                                <th scope="col">Nombre Productos</th>
                                                <th scope="col">Detalle</th>
                                                <th scope="col">Cantidad</th>
                                                <th scope="col">Precio</th>
                                                <th scope="col">P.+IVA</th>
                                                <th scope="col">Opci&oacute;n</th>
                                            </tr>
                                        </thead>
                                        <tbody name="cuerpoTablaProductos">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                <h4 class="text-muted text-left">Productos registrados en la Entrada</h4>
                                <input type="text" name="inputBuscarProductoVenta" id="inputBuscarProductoVenta"
                                    class="form-control" placeholder="Buscar..."
                                    onkeyup="doSearch('tablaProductosVenta','inputBuscarProductoVenta')">
                                <div class="table-responsive" autoCal="true" formulacal="height-250"
                                    style="overflow-y: auto; border: 1px solid #eeeeee;">
                                    <table class="table table-sm table-hover" name="tablaProductosVenta"
                                        id="tablaProductosVenta">
                                        <thead>
                                            <tr>
                                                <th scope="col">Nombre Producto</th>
                                                <th scope="col">Detalle</th>
                                                <th scope="col">Laboratorio</th>
                                                <th scope="col">Cantidad</th>
                                                <th scope="col">Costo</th>
                                                <th scope="col">P. Venta</th>
                                                <th scope="col">P. Venta+IVA</th>
                                                <!-- <th scope="col">Total</th> -->
                                                <th scope="col">Opci&oacute;n</th>
                                            </tr>
                                        </thead>
                                        <tbody name="cuerpoTablaProductosVenta">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" name="btnAgregarFactura">Guardar Compra</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL VER VENTA -->


<!-- MODAL VER VENTA -->
<div class="modal fade" id="modalCantidad" name="modalCantidad" data-backdrop="static" data-keyboard="true"
    tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Cantidad</h4>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form name="formularioAgregarProducto">
                        <input type="hidden" name="proCod" data-validation="false">
                        <input type="hidden" name="entradaCod" data-validation="false">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label for="">Producto Detalle</label>
                                    <input type="text" name="detalleproentrada" id="detalleproentrada"
                                        class="form-control" readonly=»readonly» data-validation="false">
                                </div>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="">Cantidad a Ingresar</label>
                                    <input type="number" name="cantidadentrada" id="cantidadentrada"
                                        data-error="Digite el campo Cantidad a Ingresar" class="form-control">
                                </div>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="">Costo del Producto</label>
                                    <input type="number" name="costoentrada" id="costoentrada"
                                        data-error="Digite el campo Costo del Producto" class="form-control"
                                        onkeyup="rentabilidad()">
                                </div>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="">Precio de Venta</label>
                                    <input type="number" name="precioVenta" id="precioVenta"
                                        data-error="Digite el campo Precio de Venta" class="form-control"
                                        onkeyup="rentabilidad()">
                                </div>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">IVA: <span class="text-danger">*</span></label>
                                <input type="number" placeholder="0" name="detproiva" id="detproiva" class="form-control"
                                    data-error="Valor minimo de IVA es 0">
                            </div>
                        </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label for="">Rentabilidad</label>
                                    <input type="number" readonly name="rentablidad" id="rentablidad"
                                        data-error="Digite el campo Precio de Venta" class="form-control">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" name="btnAgregarProducto">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL VER VENTA -->


<!-- MODAL INGREAR NUEVO PRODUCTO -->
<div class="modal fade" id="modalProductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    style="overflow-y: scroll;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header text-white" style="background:#0F7A87;">
                <h3 class="modal-title" id="exampleModalLabel" style="color:white;">Ingreso de Productos</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" autoCal="true" formulacal="height -250" style="overflow-y:auto;">
                <form name="formularioNuevoProducto">
                    <div class="row">
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Nombre del Producto: <span class="text-danger">*</span></label>
                                <input type="text" name="pronombre" class="form-control"
                                    data-error="Complete el campo Nombre del Producto">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Detalle del Producto: <span class="text-danger">*</span></label>
                                <input type="text" name="prodetalle" class="form-control"
                                    data-error="Complete el campo Detalle del Producto">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Codigo: <span class="text-danger">*</span></label>
                                <input type="text" name="procodigo" class="form-control"
                                    data-error="Complete el campo Codigo">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Laboratorio: <span class="text-danger">*</span></label>
                                <select type="text" name="prolabid" class="form-control"
                                    data-error="Seleccione un Laboratorio"></select>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Bodega: <span class="text-danger">*</span></label>
                                <select type="text" name="probodid" class="form-control"
                                    data-error="Seleccione una Bodega"></select>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Ruta: <span class="text-danger">*</span></label>
                                <input type="text" name="proruta" class="form-control" data-validation="false">
                            </div>
                        </div>
                        <div class="col-3 col-sm-3 col-md-3 form-group">
                            <button name="crearLaboratorio" type="button" class="btn btn-primary btn-lg btn-block">Crear
                                Laboratorio</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" name="guardarProducto">Guardar</button>
            </div>
        </div>
    </div>
</div>


<!-- MODAL INGREAR NUEVO LABORATORIO -->
<div class="modal fade" id="modalLaboratorios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    style="overflow-y: scroll;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header text-white" style="background:#0F7A87;">
                <h3 class="modal-title" id="exampleModalLabel" style="color:white;">Ingreso de Laboratorios</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" autoCal="true" formulacal="height -400" style="overflow-y:auto;">
                <form name="formularioNuevoLaboratorio">
                    <div class="row">
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Nombre del Laboratorio: <span class="text-danger">*</span></label>
                                <input type="text" name="labnombre" class="form-control"
                                    data-error="Complete el campo Nombre del Laboratorio">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" name="guardarLaboratorio">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script>
$.getScript("<?php echo  base_url(); ?>public/funciones/compras/Menu.js");

function doSearch(tabla, input) {
    var tableReg = document.getElementById(tabla);
    var searchText = $("[name=" + input + "]").val().toLowerCase();
    var cellsOfRow = "";
    var found = false;
    var compareWith = "";

    // Recorremos todas las filas con contenido de la tabla
    for (var i = 1; i < tableReg.rows.length; i++) {
        cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        found = false;
        // Recorremos todas las celdas
        for (var j = 0; j < cellsOfRow.length && !found; j++) {
            compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            // Buscamos el texto en el contenido de la celda
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                found = true;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            // si no ha encontrado ninguna coincidencia, esconde la
            // fila de la tabla
            tableReg.rows[i].style.display = 'none';
        }
    }
}
</script>