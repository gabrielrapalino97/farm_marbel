<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<title>Plataforma</title>
<div class="col-xs-12 col-sm-12 col-md-12" id="panelHome" style="margin-top:10px;">
    <div class="panel panel-primary home">
        <div class="panel-heading">
            <h3 class="panel-title"><a href="<?php echo  base_url('Home'); ?>" style="text-decoration:none;"><i
                        style="margin-right:30px;" class="icon icon-undo2"></i></a>
                Reportes </h3>
        </div>
        <div class="panel-body" autoCal="true" formulacal="height-70" style="overflow-y:auto;">
            <div id="opciones" autoCal="true" formulacal="height-100">
                <!--CONTENIDO  -->
                <div class="container">
                    <div class="row">


                        <?php if (permisosModulos('WEB_ADMINISTRAR')) { ?>
                        <div align="center" class="col-xs-6 col-sm-3 col-md-3 col-lg-3 img"
                            style="min-height: 150px; max-height:150px">
                            <div style="padding-top:10px"></div>
                            <a href="<?php echo  base_url("Reportes/Sugerido"); ?>"
                                style="text-decoration:none; color:#333;">
                                <font style="cursor: pointer;">
                                    <img src="<?php echo  base_url(); ?>public/img/vercuali.png" alt="" height="80"
                                        width="80">
                                    <div>Reporte Sugerido</div>
                                </font>
                            </a>
                        </div>
                        <?php } ?>

                        <?php if (permisosModulos('WEB_ADMINISTRAR')) { ?>
                        <div align="center" class="col-xs-6 col-sm-3 col-md-3 col-lg-3 img"
                            style="min-height: 150px; max-height:150px">
                            <div style="padding-top:10px"></div>
                            <a href="<?php echo  base_url("Reportes/Detalle"); ?>"
                                style="text-decoration:none; color:#333;">
                                <font style="cursor: pointer;">
                                    <img src="<?php echo  base_url(); ?>public/img/report2.png" alt="" height="80"
                                        width="80">
                                    <div>Reporte Detalle</div>
                                </font>
                            </a>
                        </div>
                        <?php } ?>


                    </div>

                    <div class="row">

                    </div>
                </div>
                <!--CONTENIDO  -->
            </div>
        </div>
    </div>
</div>