<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<title>Inicio</title>
<div class="col-xs-12 col-sm-12 col-md-12" id="panelHome" style="margin-top:10px;">
    <div class="panel panel-primary home">
        <div class="panel-heading">
            <h3 class="panel-title"><a href="<?php echo  base_url('Reportes/Home_reporte'); ?>" style="text-decoration:none;"><i style="margin-right:30px;" class="icon icon-undo2"></i></a> Reportes
            </h3>
        </div>
        <div class="panel-body" autoCal="true" formulacal="height-70" style="overflow-y:auto;">
            <div id="opciones" autoCal="true" formulacal="height-100">
                <!-- Contenido -->
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <input type="date" name="fechaReporte" class="form-control">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="btn-group" role="group" aria-label="...">
                                <button type="button" class="btn btn-success" name="btnCargarReporte">Cargar Reporte</button>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <input type="text" name="inputBuscarProductosSugeridos" id="inputBuscar" class="form-control" placeholder="Buscar..." onkeyup="doSearch('tablaProductosSugerido','inputBuscarProductosSugeridos')">
                            <div class="table-responsive" autoCal="true" formulacal="height-280" style="overflow-y: auto; border: 1px solid #eeeeee;">
                                <table class="table table-sm table-hover" name="tablaProductosSugerido" id="tablaProductosSugerido">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">NOMBRE PRODUCTO</th>
                                            <th scope="col">DETALLE PRODUCTO</th>
                                            <th scope="col">LABORATORIO</th>
                                            <th scope="col">BODEGA</th>
                                            <th scope="col">CANTIDAD BODEGA</th>
                                            <th scope="col">CANTIDAD VENDIDA</th>
                                            <th scope="col">SUGERIDO DE COMPRA</th>
                                        </tr>
                                    </thead>
                                    <tbody name="cuerpoTablaProductosSugeridos">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Contenido -->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $.getScript("<?php echo  base_url(); ?>public/funciones/Reportes/Sugerido.js");

    function doSearch(tabla, input) {
        var tableReg = document.getElementById(tabla);
        var searchText = $("[name=" + input + "]").val().toLowerCase();
        var cellsOfRow = "";
        var found = false;
        var compareWith = "";

        // Recorremos todas las filas con contenido de la tabla
        for (var i = 1; i < tableReg.rows.length; i++) {
            cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
            found = false;
            // Recorremos todas las celdas
            for (var j = 0; j < cellsOfRow.length && !found; j++) {
                compareWith = cellsOfRow[j].innerHTML.toLowerCase();
                // Buscamos el texto en el contenido de la celda
                if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                    found = true;
                }
            }
            if (found) {
                tableReg.rows[i].style.display = '';
            } else {
                // si no ha encontrado ninguna coincidencia, esconde la
                // fila de la tabla
                tableReg.rows[i].style.display = 'none';
            }
        }
    }
</script>