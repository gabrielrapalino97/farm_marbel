<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<title>Gestion Clientes</title>
<div class="col-xs-12 col-sm-12 col-md-12" id="panelHome" style="margin-top:10px;">
    <div class="panel panel-primary home">
        <div class="panel-heading">
            <h3 class="panel-title"><a href="<?php echo  base_url('Home'); ?>" style="text-decoration:none;"><i style="margin-right:30px;" class="icon icon-undo2"></i></a> Clientes </h3>
        </div>
        <div class="panel-body" autoCal="true" formulacal="height-70" style="overflow-y:auto;">
            <div id="opciones" autoCal="true" formulacal="height-100">
				<div class="container">
					<div class="row">
               
					<!--CONTENIDO  -->
							<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3" style="width:100%;">
								<div class="content">
									<a href="" id="btnPanel" class="btn btn-block" data-toggle="modal"
										data-target="#modalPersona" style="background: #214FCD; color:white; " ><span class="icon icon-plus"
											style="float:left; margin-top:4px; color:white;"></span> Nueva Persona</a>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-12 " style="width:100%;">
								<input type="text" name="busqueda" id="busqueda" class="form-control"
									placeholder="Buscar por Nombre..." onkeyup="filtarTablaPersonas()">
							</div>
							<div class="col-12 col-sm-12 col-md-12 " style="width:100%;">
								<div class="table-responsive" style="height: 67%; overflow: auto;">
									<table class="table table-sm" name="tablaPersonas" id="tablaPersonas">
										<h4 class="text-center">Tabla Personas Ingresadas</h4>
										<thead>
											<tr>
												<th scope="col">#</th>
												<th scope="col">Nombre</th>
												<th scope="col">NIT</th>
												<th scope="col">Direccion</th>
												<th scope="col">Correo</th>
												<th scope="col">Telefono</th>
												<th scope="col">Acciones</th>
											</tr>
										</thead>
										<tbody name="cuerpoTabla">
										</tbody>
									</table>
								</div>
							</div>
							<!--CONTENIDO  -->
							
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL INGREAR NUEVO CLIENTE -->
<div class="modal fade" id="modalPersona" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    style="overflow-y: scroll;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header text-white" style="background:#0F7A87;">
                <h3 class="modal-title" id="exampleModalLabel" style="color:white;">Ingreso de Clientes</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"  autoCal="true" formulacal="height-200" style="overflow-y:auto;">
                <form name="formularioNuevaPersona">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">Nombre/Razón: <span class="text-danger">*</span></label>
                                <input type="text" name="nombreRazon" class="form-control"
                                    data-error="Complete el campo Nombre/Razón">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">NIT: <span class="text-danger">*</span></label>
                                <input type="text" name="nit" class="form-control" data-error="Complete el campo NIT">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">Correo: </label>
                                <input type="text" name="correo" class="form-control" data-validation="false">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">Dirección: </label>
                                <input type="text" name="direccion" class="form-control" data-validation="false">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">Telefono: </label>
                                <input type="number" name="telefono" class="form-control" data-validation="false">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" name="guardarPersona">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL INGRESAR NUEVO CLIENTE -->

<!-- MODAL EDITAR PERSONA -->

<div class="modal fade" id="modalEditarPersona" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    style="overflow-y: scroll;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-white" style="background:#214FCD;">
                <h3 class="modal-title" id="exampleModalLabel" style="color:white;">Edicion de Personas</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="formularioEditarPersona">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">Nombre/Razón: </label>
                                <input type="text" name="nombreEditarPersona" class="form-control"
                                    data-error="Complete el campo Nombre/Razón">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">NIT: </label>
                                <input type="number" name="nitEditarPersona" class="form-control"
                                    data-error="Complete el campo NIT">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">Correo: </label>
                                <input type="text" name="correoEditarPersona" class="form-control"
                                    data-validation="false">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">Dirección: </label>
                                <input type="text" name="direccionEditarPersona" class="form-control"
                                    data-validation="false">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">Telefono: </label>
                                <input type="number" name="telefonoEditarPersona" class="form-control"
                                    data-validation="false">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <input type="number" style="visibility:hidden" readonly=»readonly» name="cod"
                                    class="form-control" data-validation="false">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" name="actualizarEditarPersona">Actualizar</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL EDITAR PERSONA -->

<script>
$.getScript("<?php echo base_url(); ?>public/funciones/persona/persona.js");
</script>
