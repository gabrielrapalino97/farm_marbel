<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<title>Productos de Inventario</title>
<div class="col-xs-12 col-sm-12 col-md-12" id="panelHome" style="margin-top:10px;">
    <div class="panel panel-primary home">
        <div class="panel-heading">
            <h3 class="panel-title"><a href="<?php echo  base_url('Gestion/Home_gestion'); ?>"
                    style="text-decoration:none;"><i style="margin-right:30px;" class="icon icon-undo2"></i></a>
                Productos </h3>
        </div>
        <div class="panel-body" autoCal="true" formulacal="height-70" style="overflow-y:auto;">
            <div id="opciones" autoCal="true" formulacal="height-100">
                <div class="container">
                    <div class="row">

                        <!--CONTENIDO  -->
                        <div class="col-lg-4 col-xl-4">
                            <button name="crearProducto" type="button" class="btn btn-primary btn-lg btn-block">Crear
                                Producto</button>
                        </div>
                        <div class="col-lg-2 col-xl-2">
                            <button name="stocks" type="button" class="btn btn-warning btn-lg btn-block">Filtro - Stock Bajo</button>
                        </div>
                        <div class="col-lg-2 col-xl-2">
                            <button name="products" type="button"
                                class="btn btn-primary btn-lg btn-success">Productos</button>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 " style="width:100%;">
                            <br><br>
                            <input type="text" name="busqueda" id="busqueda" class="form-control"
                                placeholder="Buscar por Nombre o Codigo" onkeyup="filtarTablaPersonas()">
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 " style="width:100%;">
                            <div class="table-responsive" style="height: 67%; overflow: auto;">
                                <table class="table table-sm" name="tablaPersonas" id="tablaPersonas">
                                    <h4 class="text-center">Tabla Productos Ingresados</h4>
                                    <thead>
                                        <tr>
                                            <th scope="col">Descripcion</th>
                                            <th scope="col">Laboratorio</th>
                                            <th scope="col">Bodega</th>
                                            <th scope="col">Cantidad</th>
                                            <th scope="col">Precio Venta</th>
                                            <th scope="col">Precio+IVA</th>
                                            <th scope="col">Precio Compra</th>
                                            <th scope="col">IVA</th>
                                            <th scope="col">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody name="cuerpoTabla">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--CONTENIDO  -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- MODAL INGREAR NUEVO PRODUCTO -->
<div class="modal fade" id="modalProductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    style="overflow-y: scroll;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header text-white" style="background:#0F7A87;">
                <h3 class="modal-title" id="exampleModalLabel" style="color:white;">Ingreso de Productos</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" autoCal="true" formulacal="height -170" style="overflow-y:auto;">
                <form name="formularioNuevoProducto">
                    <div class="row">
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Nombre del Producto: <span class="text-danger">*</span></label>
                                <input type="text" name="pronombre" class="form-control"
                                    data-error="Complete el campo Nombre del Producto">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Detalle del Producto: <span class="text-danger">*</span></label>
                                <input type="text" name="prodetalle" class="form-control"
                                    data-error="Complete el campo Detalle del Producto">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Codigo: <span class="text-danger">*</span></label>
                                <input type="text" name="procodigo" class="form-control"
                                    data-error="Complete el campo Codigo">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Laboratorio: <span class="text-danger">*</span></label>
                                <select type="text" name="prolabid" class="form-control"
                                    data-error="Seleccione un Laboratorio"></select>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Bodega: <span class="text-danger">*</span></label>
                                <select type="text" name="probodid" class="form-control"
                                    data-error="Seleccione una Bodega"></select>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Cantidad: <span class="text-danger">*</span></label>
                                <input type="number" name="procantidad" class="form-control"
                                    data-error="Complete el campo Cantidad">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="">Precio de Venta: <span class="text-danger">*</span></label>
                                <input type="number" onkeyup="rentabilidad()" name="proprecioventa" class="form-control"
                                    data-error="Complete el campo Precio de Venta">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Precio Compra/Entrada: <span class="text-danger">*</span></label>
                                <input type="number" onkeyup="rentabilidad()" name="procompra" class="form-control"
                                    data-validation="false">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">IVA: <span class="text-danger">*</span></label>
                                <input type="number" placeholder="0" name="proiva" class="form-control"
                                    data-error="Valor minimo de IVA es 0">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Ruta: <span class="text-danger">*</span></label>
                                <input type="text" name="proruta" class="form-control" data-validation="false">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Rentabilidad</label>
                                <input type="number" readonly name="rentablidad" id="rentablidad"
                                    data-validation="false" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" name="guardarProducto">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL EDITAR PRODUCTO -->
<div class="modal fade" id="modalEditarProductos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    style="overflow-y: scroll;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header text-white" style="background:#0F7A87;">
                <h3 class="modal-title" id="exampleModalLabel" style="color:white;">Modificacion de Producto</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" autoCal="true" formulacal="height -180" style="overflow-y:auto;">
                <form name="formularioEditarProducto">
                    <div class="row">
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Nombre del Producto: <span class="text-danger">*</span></label>
                                <input type="text" name="editpronombre" class="form-control"
                                    data-error="Complete el campo Nombre del Producto">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Detalle del Producto: <span class="text-danger">*</span></label>
                                <input type="text" name="editprodetalle" class="form-control"
                                    data-error="Complete el campo Detalle del Producto">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Codigo: <span class="text-danger">*</span></label>
                                <input type="text" name="editprocodigo" class="form-control"
                                    data-error="Complete el campo Codigo">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Laboratorio: <span class="text-danger">*</span></label>
                                <select type="text" name="editprolabid" class="form-control"
                                    data-error="Seleccione un Laboratorio"></select>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Bodega: <span class="text-danger">*</span></label>
                                <select type="text" name="editprobodid" class="form-control"
                                    data-error="Seleccione una Bodega"></select>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Cantidad: <span class="text-danger">*</span></label>
                                <input type="number" name="editprocantidad" class="form-control"
                                    data-error="Complete el campo Cantidad">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Precio de Venta: <span class="text-danger">*</span></label>
                                <input type="number" name="editproprecioventa" class="form-control"
                                    data-error="Complete el campo Precio de Venta">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Precio Compra/Entrada: <span class="text-danger">*</span></label>
                                <input type="number" name="editpropreciocompra" class="form-control"
                                    data-validation="false">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">IVA: <span class="text-danger">*</span></label>
                                <input type="number" name="editproiva" class="form-control"
                                    data-error="Valor minimo de IVA es 0">
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Ruta: <span class="text-danger">*</span></label>
                                <input type="text" name="editproruta" class="form-control" data-validation="false">
                                <input type="number" style="visibility:hidden" readonly=»readonly» id="editproid"
                                    name="editproid" class="form-control" data-validation="false">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" name="editarProducto">Guardar</button>
            </div>
        </div>
    </div>
</div>


<script>
$.getScript("<?php echo base_url(); ?>public/funciones/gestion/productos.js");
</script>