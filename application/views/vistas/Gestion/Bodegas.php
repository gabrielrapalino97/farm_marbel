<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<title>Gestion de Bodegas</title>
<div class="col-xs-12 col-sm-12 col-md-12" id="panelHome" style="margin-top:10px;">
    <div class="panel panel-primary home">
        <div class="panel-heading">
            <h3 class="panel-title"><a href="<?php echo  base_url('Gestion/Home_gestion'); ?>" style="text-decoration:none;"><i
                        style="margin-right:30px;" class="icon icon-undo2"></i></a>
                Bodegas </h3>
        </div>
        <div class="panel-body" autoCal="true" formulacal="height-70" style="overflow-y:auto;">
            <div id="opciones" autoCal="true" formulacal="height-100" >
                <div class="container">
                    <div class="row">

                        <!--CONTENIDO  -->
                        <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                            <button name="crearBodega" type="button" class="btn btn-primary btn-lg btn-block">Crear
                                Bodega</button>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 " style="width:100%;">
                            <br><br>
                            <input type="text" name="busqueda" id="busqueda" class="form-control"
                                placeholder="Buscar por Nombre" onkeyup="filtarTablaPersonas()">
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 " style="width:100%;">
                            <div class="table-responsive" style="height: 67%; overflow: auto;">
                                <table class="table table-sm" name="tablaPersonas" id="tablaPersonas">
                                    <h4 class="text-center">Tabla Bodegas Ingresadas</h4>
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody name="cuerpoTabla">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--CONTENIDO  -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- MODAL INGREAR NUEVO BODEGA -->
<div class="modal fade" id="modalBodegas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    style="overflow-y: scroll;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header text-white" style="background:#0F7A87;">
                <h3 class="modal-title" id="exampleModalLabel" style="color:white;">Ingreso de Bodegas</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" autoCal="true" formulacal="height -250" style="overflow-y:auto;">
                <form name="formularioNuevoBodega">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">Nombre del Bodega: <span class="text-danger">*</span></label>
                                <input type="text" name="bodnombre" class="form-control"
                                    data-error="Complete el campo Nombre del Bodega">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" name="guardarBodega">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL EDITAR BODEGA -->
<div class="modal fade" id="modalEditarBodega" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    style="overflow-y: scroll;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width:90%;">
        <div class="modal-content">
            <div class="modal-header text-white" style="background:#0F7A87;">
                <h3 class="modal-title" id="exampleModalLabel" style="color:white;">Editar Bodega</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" autoCal="true" formulacal="height -250" style="overflow-y:auto;">
                <form name="formularioEditarBodega">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                                <label for="">Nombre del Bodega: <span class="text-danger">*</span></label>
                                <input type="text" name="editbodnombre" class="form-control"
                                    data-error="Complete el campo Nombre del Bodega">
                                <input type="number" style="visibility:hidden" readonly=»readonly» id="editbodid"
                                    name="editbodid" class="form-control" data-validation="false">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" name="editarBodega">Guardar</button>
            </div>
        </div>
    </div>
</div>



<script>
$.getScript("<?php echo base_url(); ?>public/funciones/gestion/bodegas.js");
</script>