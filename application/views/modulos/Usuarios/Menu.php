<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<title>Administracion de Usuarios</title>
<div class="col-xs-12 col-sm-12 col-md-12" id="panelHome" style="margin-top:10px;">
    <div class="panel panel-primary home">
        <div class="panel-heading">
            <h3 class="panel-title"><a href="<?php echo  base_url('index.php/Home'); ?>" style="text-decoration:none;"><i style="margin-right:30px;" class="icon icon-undo2"></i></a> Administraci&oacute;n de Usuarios </h3>
        </div>
        <div class="panel-body" autoCal="true" formulacal="height-70" style="overflow-y:auto;">
            <div id=" opciones" autoCal="true" formulacal="height-100">
                <!-- Contenido -->
                <div class="container">
                    <div class="row">
                        <div align="center" class="col-xs-12 col-sm-2 col-md-2 img">
                            <a href="" name="btnModal" data-toggle="modal" data-target="#modalAgregarUsuario" style="text-decoration:none; color:#333;">
                                <font style="cursor: pointer;">
                                    <img src="<?php echo  base_url(); ?>public/img/iconos/login/agregar.png" alt="" height="60" width="60">
                                    <div>Agregar Usuario</div>
                                </font>
                            </a>
                        </div>

                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="height:40px">
                                    <h3 class="panel-title" >Configuraci&oacute;n del Usuario</h3>
                                </div>
                                <div class="panel-body" autoCal="true" formulaCal="height-300" style="padding: 0px; overflow-y: auto;">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 col-md-6" style="margin-top:5px;">
                                                <form name="formularioBuscarUsuario">
                                                    <div class="form-group">
                                                        <label for="">Cedula: <span class="text-danger">*</span></label>
                                                        <input type="number" id="cedula" name="cedula" class="form-control" data-error="Complete el campo Cedula" placeholder="Buscar usuario">
                                                    </div>
                                                </form>
                                            </div><br>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top:5px;">
                                                <table class="table">
                                                    <tbody name="cuerpoUsuario"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <div class="panel panel-primary">
                                <div class="panel-heading" style="height:40px">
                                    <h3 class="panel-title" >
                                        <!--<span class="icon icon-plus" data-toggle="modal" data-target="#modalAgregarRol" name="agregarRol" style="margin-right:35px;-->
                                        <!--                                    cursor:pointer;">-->
                                        <!--</span>-->
                                        Roles
                                    </h3>
                                </div>
                                <div class="panel-body" autoCal="true" formulaCal="height-300" style="padding: 0px; overflow-y: auto;">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top:5px;">
                                                <table class="table">
                                                    <tbody name="cuerpoRoles"></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAgregarUsuario" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width:90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Agregar Usuario</h4>
                </div>
                <div class="modal-body" id="" style="overflow-y:auto;" autoCal="true" formulacal="height-180">
                    <div class="container">
                        <form name="formularioAgregarUsuario">
                            <div class="row">
                                <div class="col-xs-12 col-sm-3 col-md-3 form-group">
                                    <label for="">Nombres: <span class="text-danger">*</span></label>
                                    <input type="text" name="nombre" class="form-control" data-error="Complete el campo Nombre">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-3 form-group">
                                    <label for="">Apellidos: <span class="text-danger">*</span></label>
                                    <input type="text" name="apellido" class="form-control" data-error="Complete el campo Apellido">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-3 form-group">
                                    <label for="">Codigo de Identificacion: <span class="text-danger">*</span></label>
                                    <input type="number" name="cedula" class="form-control" data-error="Complete el campo Cedula">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-3 form-group">
                                    <label for="">Email: <span class="text-danger">*</span></label>
                                    <input type="email" name="email" class="form-control" data-validation="false">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-3 col-md-3 form-group">
                                    <label for="">Telefono: <span class="text-danger">*</span></label>
                                    <input type="number" name="telefono" class="form-control" data-error="Complete el campo Telefono">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-3 form-group">
                                    <label for="">Rol: <span class="text-danger">*</span></label>
                                    <select name="rol" class="form-control" data-error="Complete el campo Rol"></select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success" name="btnGuardarUsuario" id="btnGuardarUsuario" value="Guardar">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->

    <!-- Modal -->
    <div class="modal fade" id="modalAgregarEmpresa" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width:90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Agregar Empresa</h4>
                </div>
                <div class="modal-body" id="" style="overflow-y:auto;" autoCal="true" formulacal="height-400">
                    <div class="container">
                        <form name="formularioAgregarEmpresa">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                                    <label for="">Nombre/Razon: <span class="text-danger">*</span></label>
                                    <input type="text" name="razon" class="form-control" data-error="Complete el campo Nombre/Razon">
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                                    <label for="">NIT: <span class="text-danger">*</span></label>
                                    <input type="number" name="nit" class="form-control" data-error="Complete el campo Cedula">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success" name="btnGuardarEmpresa" id="btnGuardarEmpresa" value="Guardar">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->


    <!-- Modal -->
    <div class="modal fade" id="modalAgregarRol" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Agregar Rol</h4>
                </div>
                <div class="modal-body" id="">
                    <div class="container">
                        <form name="formularioRol">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                                    <label for="">Nombre: <span class="text-danger">*</span></label>
                                    <input type="text" name="nombre" class="form-control" data-error="Complete el campo Nombre">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success" name="btnAgregarRol" id="btnAgregarRol" value="Guardar">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->

    <!-- Modal -->
    <div class="modal fade" name="modalAgregarPermisoRol" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Agregar Permiso</h4>
                </div>
                <div class="modal-body" id="" style="overflow-y:auto;" autocal="true" formulacal="height-250">
                    <div class="container">
                        <input type="hidden" name="codRol" value="">
                        <form name="formularioAgregarPermisoRol">
                            <div class="row" name="cuerpoPermisosRolSeleccionar">
                                <!-- <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                                    <table>
                                        <tbody name="cuerpoPermisosRolSeleccionar">

                                        </tbody>
                                    </table>
                                </div> -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-success" name="btnAgregarPermisoRol" id="btnAgregarPermisoRol" value="Guardar">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <script>
        $.getScript("<?php echo  base_url(); ?>public/funciones/usuarios/usuarios.js");
    </script>
