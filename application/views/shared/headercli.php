<!DOCTYPE html>
<html lang="es-CO">

<head>

    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>public/login/images/icons/icon.png" />
    <!-- <link href="<?php echo base_url(); ?>assets/img/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" /> -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.1.0.min.js" type="text/javascript">
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js" type="text/javascript"></script>
    <link href="<?php echo base_url('public/Reporte/css.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.growl.js" type="text/javascript">
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.bootstrap-growl.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootbox.min.js" type="text/javascript">
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-notify.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.base64.js" type="text/javascript">
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/loading.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart/Chart.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart/utils.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-treeview.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/bootbox.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery.formValidation.js"></script>

    <!-- Version compilada y comprimida del JavaScript de Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/jquery.formValidation.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    </link>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/iconmoon.css">
    </link>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/estilos.css">
    </link>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/table.css">
    </link>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/daterangepicker.css">
    </link>

    <!-- Select2 combobox-->
    <link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
    <link href="<?php echo base_url('public/Reporte/css.css') ?>" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        var BASE_URL = "<?php echo base_url(); ?>";
        var FECHA_ACTUAL = "<?php echo date("Y-m-d"); ?>";
        var APP_USER = "<?php echo $this->session->userdata('usuario'); ?>";
    </script>
</head>

<br>
<br>
<br>
<br>
