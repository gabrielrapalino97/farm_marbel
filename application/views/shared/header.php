<!DOCTYPE html>
<html lang="es-CO">

<head>

    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>public/login/images/icons/icon.png" />
    <!-- <link href="<?php echo base_url(); ?>assets/img/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" /> -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.1.0.min.js" type="text/javascript">
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js" type="text/javascript"></script>
    <link href="<?php echo base_url('public/Reporte/css.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.growl.js" type="text/javascript">
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.bootstrap-growl.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootbox.min.js" type="text/javascript">
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-notify.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.base64.js" type="text/javascript">
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/loading.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart/Chart.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Chart/utils.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-treeview.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/bootbox.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery.formValidation.js"></script>

    <!-- Version compilada y comprimida del JavaScript de Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/jquery.formValidation.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    </link>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/iconmoon.css">
    </link>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/estilos.css">
    </link>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/table.css">
    </link>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/daterangepicker.css">
    </link>

    <!-- Select2 combobox-->
    <link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
    <link href="<?php echo base_url('public/Reporte/css.css') ?>" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/jquery.growl.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        var BASE_URL = "<?php echo base_url(); ?>";
        var FECHA_ACTUAL = "<?php echo date("Y-m-d"); ?>";
        var APP_USER = "<?php echo $this->session->userdata('usuario'); ?>";
    </script>
</head>

<body>
    <header class="header">
        <div class="jumbotron col-xs-12 col-sm-12 col-md-12" style="height:75px;" name="cabeceraApp">
           
            <?php if ($this->session->userdata('auth')) { ?>
                <div align="center" class="col-xs-3 col-sm-4 col-md-4">

                </div>
                <div align="right" class="col-xs-10 col-sm-9 col-md-9">
                    <div align="right" class="col-xs-5 col-sm-4 col-md-6">
                        <table style="width: 100%;
                            overflow: hidden;
                            height: 52px;
                            margin-top: 5px;">
                            <tbody>
                                <tr>
                                    <td style="text-align:right;width: 100%;">
                                        <?php if (isset($titulo) && $titulo != null && $titulo != "") { ?>
                                            <font style="font-size:18px;"><?php echo $titulo; ?></font>
                                        <?php } else { ?>
                                            <font style="font-size:18px;">&iexcl;Bienvenido!</font><br>
                                            <font style="font-size:13px;">
                                                <!-- <?php echo $this->session->userdata('usuario'); ?> -->
                                            </font>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div align="left" class="col-xs-7 col-sm-8 col-md-6 hidden-print">
                        <table style="width: 100%;
                            border-left: 0.5px solid #adadad;
                            height: 52px;
                            margin-top: 5px;">
                            <tbody>
                                <tr>
                                    <td style="padding-left: 5px;text-align:center;width: 30%;">
                                        <a style="cursor:pointer;" name="miCuenta" title="Mi cuenta" data-toggle="modal" data-target="#modalMiCuenta">
                                            <img style="width: 52px;height: 52px;" src="<?php echo base_url(); ?>public/img/iconos/login/user.png" class="img-circle">
                                        </a>
                                    </td>
                                    <td style="text-align:left;width: 70%;">
                                        <font name="infoUser" style="font-size:11px;">Usuario:
                                            <?php echo $this->session->userdata('usuario'); ?></font>
                                        <br>
                                        <font name="codigo" style="font-size:11px;">C&oacute;digo:
                                            <?php echo $this->session->userdata('codigo'); ?></font>
                                        <br>
                                        <a name="btnCerrarSession" style="font-size:13px;color: black !important; text-decoration:none;">
                                            Cerrar sesi&oacute;n
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modalMiCuenta" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
            <div class="modal-dialog" style="width:90%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Informaci&oacute;n de cuenta</h4>
                    </div>
                    <div class="modal-body" id="" style="overflow-y:auto;" autoCal="true" formulacal="height-180">
                        <div class="container">
                            <div class="row">
                                <div align="center" class="col-xs-12 col-sm-2 col-md-2 img">
                                    <a href="" data-toggle="modal" data-target="#modalCambioClave" style="text-decoration:none; color:#333;">
                                        <font style="cursor: pointer;">
                                            <img src="<?php echo  base_url(); ?>public/img/iconos/login/pass.png" alt="" height="60" width="60">
                                            <div>Cambiar contrase&ntilde;a</div>
                                        </font>
                                    </a>
                                </div>
                            </div>
                            <form name="formularioMiCuenta">
                                <hr>
                                <div class="row">
                                    <input type="hidden" name="cod" value="">
                                    <div class="col-xs-12 col-sm-3 col-md-3 form-group">
                                        <label for="">Nombre: <span class="text-danger">*</span></label>
                                        <input type="text" name="nombre" class="form-control" data-error="Complete el campo Nombre">
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 form-group">
                                        <label for="">Apellido: <span class="text-danger">*</span></label>
                                        <input type="text" name="apellido" class="form-control" data-error="Complete el campo Apellido">
									</div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 form-group">
                                        <label for="">Email: <span class="text-danger">*</span></label>
                                        <input type="email" name="email" class="form-control" data-validation="false">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-3 form-group">
                                        <label for="">Telefono: <span class="text-danger">*</span></label>
                                        <input type="number" name="telefono" class="form-control" data-error="Complete el campo Telefono">
                                    </div>
                                </div>
                                <input type="hidden" name="cedu" value="">
                                    <div class="col-xs-12 col-sm-3 col-md-3 form-group" style="display: none" >
                                        <label for="">Cedula: <span class="text-danger">*</span></label>
                                        <input type="number" name="cedula" class="form-control" data-error="Complete el campo Cedula">
                                    </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-success" name="btnActualizarUsuario" id="btnActualizarUsuario" value="Actualizar informaci&oacute;n">
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->


        <div class="modal fade" id="modalCambioClave" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog">
            <div class="modal-dialog" style="width: 700px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 name="titulo" class="modal-title">Cambio de contraseña</h4>
                    </div>
                    <div class="modal-body">
                        <form name="formularioCambioClave">
                            <div class="form-group">
                                <div>
                                    <label for="current_password">Contraseña actual</label> <span class="required">*</span>
                                    <div class="">
                                        <input autocomplete="false" class="form-control" type="password" name="actual" data-error="Complete el campo Contraseña actual">
                                    </div>
                                </div>
                                <div style="padding-top: 10px;"></div>
                                <div>
                                    <label for="new_password">Nueva contraseña</label> <span class="required">*</span>
                                    <div class="">
                                        <input autocomplete="false" data-error="Complete el campo Nueva contraseña" class="form-control" type="password" name="nueva">
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top: 20px;"></div>
                        </form>
                        <div>
                            <button name="btnCambiar" class="btn btn-success">Cambiar</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    <?php
    } else {
        ?>
        <script>
            window.location = "<?php echo base_url_index(); ?>login/logout";
        </script>
    <?php } ?>
    <div class="separador-r-header col-xs-12 col-sm-12 col-md-12" style="display:none;" name="separadorHeader">
        <div class=""></div>
    </div>
    </header>
    <script type="text/javascript">
        $('[name=btnCerrarSession]').on('click', function() {
            window.location = "<?php echo base_url_index(); ?>login/logout";
        })
    </script>
    <script>
        $.getScript("<?php echo base_url(); ?>public/funciones/usuarios/micuenta.js");
    </script>
