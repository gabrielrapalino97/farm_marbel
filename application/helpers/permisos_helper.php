<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('permisosModulos')) {

  function permisosModulos($opcion)
  {
    $CI = &get_instance();
    $codigo = strtoupper($CI->session->userdata['codigo']);
    $sql = sprintf(
      "SELECT DISTINCT COUNT(*) AS CAN
                    FROM Usuarios U INNER JOIN Roles R ON (R.ROLCOD=U.ROLCOD)
                            INNER JOIN Per_roles PR ON (PR.ROLCOD=R.ROLCOD)
                            INNER JOIN Permisos P ON (P.PERCOD=PR.PERCOD)
                    WHERE U.USUCEDULA='%s' AND PR.PEROLESTADO=1 AND P.PEROPCION='%s'",
      trim($codigo),
      trim(strtoupper($opcion))
    );
    $resultado = $CI->db->query($sql);
    $resultado = $resultado->result_array();
    if (intval($resultado[0]['CAN']) > 0) {
      return true;
    } else {
      return false;
    }
  }
}

if (!function_exists('obtenerCodigoUsuario')) {

  function obtenerCodigoUsuario($cedula)
  {
    $CI = &get_instance();
    $sql = sprintf("SELECT USUCOD FROM Usuarios WHERE USUCEDULA='%s'", $cedula);
    $resultado = $CI->db->query($sql);
    $resultado = $resultado->result_array();
    return $resultado[0]['USUCOD'];
  }
}
