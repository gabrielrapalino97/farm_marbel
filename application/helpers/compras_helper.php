<?php

defined('BASEPATH') or exit('No direct script access allowed');


if (!function_exists('registrarCompraProducto')) {

    function registrarCompraProducto($codProducto, $cantidad, $codcompra,$costo,$precioventa, $proiva)
    {
        $CI = &get_instance();

        $validator = sprintf("SELECT COUNT(*) AS TOTAL FROM Detalle_compras WHERE DETCCOMCOD='%s' AND DETCPROCOD='%s'", $codcompra,$codProducto);
        $res = $CI->db->query($validator);
        $res = $res->result_array();
        $validator2 = $res[0]['TOTAL'];
        if ($validator2 >=1) {
            return array("msg" => "Ya se ha insertado este producto.", "succees" => false);
        }

        $sql = sprintf("SELECT PROCANTIDAD, PROPRECIOVENTA, PROCOMPRA, PROIVA FROM Productos WHERE PROID='%s'", $codProducto);
        $resultado = $CI->db->query($sql);
        $resultado = $resultado->result_array();

        $cantidadBodega = $resultado[0]['PROCANTIDAD'];
        $precio = $resultado[0]['PROPRECIOVENTA'];
        $preciocom = $resultado[0]['PROCOMPRA'];
        $rbproiva = $resultado[0]['PROIVA'];


        $totalCompra = ($cantidad * $costo);

        $sqlInserta = sprintf(
            "INSERT INTO `Detalle_compras`(
            `DETCPROCOD`, 
            `DETCCANTIDADPRO`, 
            `DETCCOSTOPRO`, 
            `DETCPRECIOVENTAPRO`, 
            `DETCCOMCOD`, 
            `DETCCOSTOPROTOTAL`, 
            `ROLLBACK`,
            `ROLLBACKCOM`,
            `DETPROIVA`,
            `ROLLBACKIVA`) VALUES (
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s,
                %s
            )",
            $codProducto,
            $cantidad,
            $costo,
            $precioventa,
            $codcompra,
            $totalCompra,
            $precio,
            $preciocom,
            $proiva,
            $rbproiva
        );
        $result = $CI->db->query($sqlInserta);
        if ($result) {
            $nuevaCantidad = ($cantidadBodega + $cantidad);
            $sqlActualiza = sprintf("UPDATE `Productos` SET `PROCANTIDAD`=%s, `PROPRECIOVENTA`=%s, `PROCOMPRA`=%s, `PROIVA`=%s WHERE PROID=%s", $nuevaCantidad, $precioventa, $costo, $proiva, $codProducto);
            $result = $CI->db->query($sqlActualiza);
            return ($result) ? array("msg" => "Acción realizada exitosamente.", "success" => true) : array("msg" => "Hubo un error al procesar la información.", "success" => false);
        }
    }
}

if (!function_exists('elminarProductoCompra')) {

    function elminarProductoCompra($cantidad, $detCod, $proCod, $precioAntiguo, $costoantiguo, $rollbackiva)
    {
        $CI = &get_instance();
        $sqlElimina = sprintf("DELETE FROM `Detalle_compras` WHERE DETCCOD='%s'", $detCod);
        $result = $CI->db->query($sqlElimina);

        if ($result) {
            $sql = sprintf("SELECT PROCANTIDAD FROM Productos WHERE PROID='%s'", $proCod);
            $resultado = $CI->db->query($sql);
            $resultado = $resultado->result_array();

            $cantidadBodega = $resultado[0]['PROCANTIDAD'];
            $nuevaCantidad = ($cantidadBodega - $cantidad);

            $sqlActualiza = sprintf("UPDATE `Productos` SET `PROCANTIDAD`=%s, `PROPRECIOVENTA`=%s, `PROCOMPRA`=%s, `PROIVA`=%s WHERE PROID=%s", $nuevaCantidad, $precioAntiguo, $costoantiguo, $rollbackiva, $proCod);
            $result = $CI->db->query($sqlActualiza);
            return ($result) ? array("msg" => "Acción realizada exitosamente.", "success" => true) : array("msg" => "Hubo un error al procesar la información.", "success" => false);
        }
    }
}
