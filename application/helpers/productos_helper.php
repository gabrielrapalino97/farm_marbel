<?php

defined('BASEPATH') or exit('No direct script access allowed');


if (!function_exists('registrarVentaProducto')) {

    function registrarVentaProducto($codProducto, $cantidadVenta, $codVenta)
    {
        $CI = &get_instance();
        $sql = sprintf("SELECT PROCANTIDAD, PROPRECIOVENTA, PROIVA  FROM Productos WHERE PROID='%s'", $codProducto);
        $resultado = $CI->db->query($sql);
        $resultado = $resultado->result_array();

        $cantidadBodega = $resultado[0]['PROCANTIDAD'];
        $precio = $resultado[0]['PROPRECIOVENTA'];
        $iva = $resultado[0]['PROIVA'];
        $total = $precio; #(($iva*$precio)/100)+$precio;
        
        if ($cantidadBodega < $cantidadVenta) {
            return array("msg" => "La cantidad en bodega es insuficiente para esta venta.", "succees" => false);
        }
        $totalVenta = ($cantidadVenta * $total);

        $sqlInserta = sprintf(
            "INSERT INTO `Detalle_ventas`(
            `DETPROCOD`, 
            `DETCANTIDADVENTA`, 
            `DETTOTALVENTA`, 
            `DETVENCOD`) VALUES (
                %s,
                %s,
                %s,
                %s
            )",
            $codProducto,
            $cantidadVenta,
            $totalVenta,
            $codVenta
        );
        $result = $CI->db->query($sqlInserta);
        if ($result) {
            $nuevaCantidad = ($cantidadBodega - $cantidadVenta);
            $sqlActualiza = sprintf("UPDATE `Productos` SET `PROCANTIDAD`=%s WHERE PROID=%s", $nuevaCantidad, $codProducto);
            $result = $CI->db->query($sqlActualiza);
            return ($result) ? array("msg" => "Acción realizada exitosamente.", "success" => true) : array("msg" => "Hubo un error al procesar la información.", "success" => false);
        }
    }
}

if (!function_exists('elminarProductoVenta')) {

    function elminarProductoVenta($cantidad, $detCod, $proCod)
    {
        $CI = &get_instance();
        $sqlElimina = sprintf("DELETE FROM `Detalle_ventas` WHERE DETCOD='%s'", $detCod);
        $result = $CI->db->query($sqlElimina);

        if ($result) {
            $sql = sprintf("SELECT PROCANTIDAD FROM Productos WHERE PROID='%s'", $proCod);
            $resultado = $CI->db->query($sql);
            $resultado = $resultado->result_array();

            $cantidadBodega = $resultado[0]['PROCANTIDAD'];
            $nuevaCantidad = ($cantidadBodega + $cantidad);

            $sqlActualiza = sprintf("UPDATE `Productos` SET `PROCANTIDAD`=%s WHERE PROID=%s", $nuevaCantidad, $proCod);
            $result = $CI->db->query($sqlActualiza);
            return ($result) ? array("msg" => "Acción realizada exitosamente.", "success" => true) : array("msg" => "Hubo un error al procesar la información.", "success" => false);
        }
    }
}
