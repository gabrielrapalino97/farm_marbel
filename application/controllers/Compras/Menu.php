<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Compras/Compras_model');
    }

    public function index()
    {
        $this->load->view('shared/header');
        $this->load->view('vistas/Compras/Menu');
        $this->load->view('shared/footer');
    }

    public function cargarProductosBodega()
    {
        $codBodega = $this->input->post("codBodega");
        $data = $this->Compras_model->cargarProductosBodega($codBodega);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function agregarProducto()
    {
        $datos = $this->input->post("datos");
        $data = $this->Compras_model->agregarProducto($datos);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function cargarProductosVenta()
    {
        $codVenta = $this->input->post("codVenta");
        $data = $this->Compras_model->cargarProductosVenta($codVenta);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function eliminarProductosVenta()
    {
        $cantidad = $this->input->post("cantidad");
        $detCod = $this->input->post("detCod");
        $proCod = $this->input->post("proCod");
        $precioAntiguo = $this->input->post("precioAntiguo");
        $costoantiguo = $this->input->post("costoantiguo");
        $rollbackiva = $this->input->post("rollbackiva");
        $data = $this->Compras_model->eliminarProductosVenta($cantidad, $detCod, $proCod, $precioAntiguo, $costoantiguo, $rollbackiva);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function guardarVenta()
    {
        $codVenta = $this->input->post("codVenta");
        $observacion = $this->input->post("observacion");
        $proveedor = $this->input->post("proveedor");
        $data = $this->Compras_model->guardarVenta($codVenta, $observacion, $proveedor);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function crearVenta()
    {
        $CI = &get_instance();
        $cedula = strtoupper($CI->session->userdata['codigo']);
        $data = $this->Compras_model->crearVenta($cedula);
        echo json_encode(array("data" => $data));
    }

    public function cargarVentas()
    {
        $CI = &get_instance();
        $cedula = strtoupper($CI->session->userdata['codigo']);
        $funcion = $this->input->post("funcion");
        $data = $this->Compras_model->cargarVentas($cedula, $funcion);
        echo json_encode(array("data" => $data));
    }

    
  public function cargarLaboratorios(){
    $data = $this->Compras_model->cargarLaboratorios();
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function cargarBodegas(){
    $data = $this->Compras_model->cargarBodegas();
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function ingresarProducto() {
    $parametros = $this->input->post("dataEv");
    $data = $this->Compras_model->ingresarProducto($parametros);
    echo json_encode(
			array(
            'data' => $data
      )
    );
  }
  public function ingresarLaboratorio() {
    $parametros = $this->input->post("dataEv");
    $data = $this->Compras_model->ingresarLaboratorio($parametros);
    echo json_encode(
        array(
            'data' => $data
        )
      );
    }

    public function consultarCodPro()
    {
        $codigo = $this->input->post("codigo");
        $codBodega = $this->input->post("codBodega");
        $data = $this->Compras_model->consultarCodPro($codigo, $codBodega);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }


}
