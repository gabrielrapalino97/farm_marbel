<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ventas/Ventas_model');
    }

    public function index()
    {
        $this->load->view('shared/header');
        $this->load->view('vistas/Ventas/Menu');
        $this->load->view('shared/footer');
    }

    public function cargarProductosBodega()
    {
        $codBodega = $this->input->post("codBodega");
        $data = $this->Ventas_model->cargarProductosBodega($codBodega);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function agregarProducto()
    {
        $datos = $this->input->post("datos");
        $data = $this->Ventas_model->agregarProducto($datos);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function cargarProductosVenta()
    {
        $codVenta = $this->input->post("codVenta");
        $data = $this->Ventas_model->cargarProductosVenta($codVenta);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function eliminarProductosVenta()
    {
        $cantidad = $this->input->post("cantidad");
        $detCod = $this->input->post("detCod");
        $proCod = $this->input->post("proCod");
        $data = $this->Ventas_model->eliminarProductosVenta($cantidad, $detCod, $proCod);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function guardarVenta()
    {
        $codVenta = $this->input->post("codVenta");
        $observacion = $this->input->post("observacion");
        $data = $this->Ventas_model->guardarVenta($codVenta, $observacion);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function crearVenta()
    {
        $CI = &get_instance();
        $cedula = strtoupper($CI->session->userdata['codigo']);
        $data = $this->Ventas_model->crearVenta($cedula);
        echo json_encode(array("data" => $data));
    }

    public function cargarVentas()
    {
        $CI = &get_instance();
        $cedula = strtoupper($CI->session->userdata['codigo']);
        $funcion = $this->input->post("funcion");
        $data = $this->Ventas_model->cargarVentas($cedula, $funcion);
        echo json_encode(array("data" => $data));
    }

    public function consultarCodPro()
    {
        $codigo = $this->input->post("codigo");
        $codBodega = $this->input->post("codBodega");
        $data = $this->Ventas_model->consultarCodPro($codigo, $codBodega);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }

    public function cargarResumenProductosVenta()
    {
        $cedula = ($this->session->userdata['codigo']);
        $data = $this->Ventas_model->cargarResumenProductosVenta($cedula);
        echo json_encode(
            array(
                "data" => $data
            )
        );
    }
}
