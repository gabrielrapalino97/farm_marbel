<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home_gestion extends CI_Controller
{

    public function index()
    {
        $this->load->view('shared/header');
        $this->load->view('vistas/Gestion/Home_gestion');
        $this->load->view('shared/footer');
    }
}
