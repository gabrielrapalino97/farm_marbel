<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laboratorios extends CI_Controller
{
  public function __construct()
  {
		parent::__construct();
		// if (!$this->session->userdata("logueado")) {
		// 	redirect(base_url());
		// }
    $this->load->helper("url");
    $this->load->model("Gestion/Laboratorios_model");
  }
	
  public function index()
  {
		$this->load->view('shared/header');
    $this->load->view('vistas/Gestion/Laboratorios');
    $this->load->view('shared/footer');
  }

  public function obtenerLaboratorios(){
    $data = $this->Laboratorios_model->obtenerLaboratorios();
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function ingresarLaboratorio() {
		$parametros = $this->input->post("dataEv");
    $data = $this->Laboratorios_model->ingresarLaboratorio($parametros);
    echo json_encode(
			array(
				'data' => $data
      )
    );
  }

  public function editarLaboratorio(){
    $parametros = $this->input->post("dataEv");
    $data = $this->Laboratorios_model->editarLaboratorio($parametros);
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }   

  public function eliminarLaboratorio(){
    $labid = $this->input->post("labid");
    $data = $this->Laboratorios_model->eliminarLaboratorio($labid);
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

}