<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Bodegas extends CI_Controller
{
  public function __construct()
  {
		parent::__construct();
		// if (!$this->session->userdata("logueado")) {
		// 	redirect(base_url());
		// }
    $this->load->helper("url");
    $this->load->model("Gestion/Bodegas_model");
  }
	
  public function index()
  {
		$this->load->view('shared/header');
    $this->load->view('vistas/Gestion/Bodegas');
    $this->load->view('shared/footer');
  }

  public function obtenerBodegas(){
    $data = $this->Bodegas_model->obtenerBodegas();
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function ingresarBodega() {
		$parametros = $this->input->post("dataEv");
    $data = $this->Bodegas_model->ingresarBodega($parametros);
    echo json_encode(
			array(
				'data' => $data
      )
    );
  }

  public function editarBodega(){
    $parametros = $this->input->post("dataEv");
    $data = $this->Bodegas_model->editarBodega($parametros);
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }   

  public function eliminarBodega(){
    $bodid = $this->input->post("bodid");
    $data = $this->Bodegas_model->eliminarBodega($bodid);
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

}