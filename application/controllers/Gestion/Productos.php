<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Productos extends CI_Controller
{
  public function __construct()
  {
		parent::__construct();
		// if (!$this->session->userdata("logueado")) {
		// 	redirect(base_url());
		// }
    $this->load->helper("url");
    $this->load->model("Gestion/Productos_model");
  }
	
  public function index()
  {
		$this->load->view('shared/header');
    $this->load->view('vistas/Gestion/Productos');
    $this->load->view('shared/footer');
  }

  public function obtenerProductos(){
    $data = $this->Productos_model->obtenerProductos();
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function stockBajo(){
    $data = $this->Productos_model->stockBajo();
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function cargarLaboratorios(){
    $data = $this->Productos_model->cargarLaboratorios();
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function cargarBodegas(){
    $data = $this->Productos_model->cargarBodegas();
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function ingresarProducto() {
		$parametros = $this->input->post("dataEv");
    $data = $this->Productos_model->ingresarProducto($parametros);
    echo json_encode(
			array(
				'data' => $data
      )
    );
  }

  public function cargarLaboratoriosEdit(){
    $editprolabid = $this->input->post("editprolabid");
    $data = $this->Productos_model->cargarLaboratoriosEdit($editprolabid);
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function cargarBodegasEdit(){
    $editprobodid = $this->input->post("editprobodid");
    $data = $this->Productos_model->cargarBodegasEdit($editprobodid);
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function editarProducto(){
    $parametros = $this->input->post("dataEv");
    $data = $this->Productos_model->editarProducto($parametros);
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

  public function eliminarProducto(){
    $proid = $this->input->post("proid");
    $data = $this->Productos_model->eliminarProducto($proid);
    echo json_encode(
      array(
        'data' => $data
      )
    );
  }

    

}