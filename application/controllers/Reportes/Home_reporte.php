<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home_reporte extends CI_Controller
{

    public function index()
    {
        $this->load->view('shared/header');
        $this->load->view('vistas/Reportes/Home_reporte');
        $this->load->view('shared/footer');
    }
}
