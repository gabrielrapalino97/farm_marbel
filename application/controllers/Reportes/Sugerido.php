<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Sugerido extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Reportes/Sugerido_model');
    }

    public function index()
    {
        $this->load->view('shared/header');
        $this->load->view('vistas/Reportes/Sugerido');
        $this->load->view('shared/footer');
    }

    public function reporteGeneral()
    {
        $fecha = $this->input->post("fecha");
        $data = $this->Sugerido_model->reporteGeneral($fecha);
        echo json_encode(array("data" => $data));
    }
}
