<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Persona extends CI_Controller
{
  public function __construct()
  {
		parent::__construct();
		// if (!$this->session->userdata("logueado")) {
		// 	redirect(base_url());
		// }
    $this->load->helper("url");
    $this->load->model("Persona/Persona_model");
  }
	
  public function index()
  {
		$this->load->view('shared/header');
    $this->load->view('vistas/Persona/Persona');
    $this->load->view('shared/footer');
  }
	
  public function ingresarPersona()
  {
		$parametros = $this->input->post("dataEv");
    $data = $this->Persona_model->ingresarPersona($parametros);
    echo json_encode(
			array(
				'data' => $data
				)
			);
		}
		
		public function actualizarPersona()
		{
			$data = $this->input->post("dataEv");
			$data = $this->Persona_model->actualizarPersona($data);
			echo json_encode(
				array(
					'data' => $data
					)
				);
			}
			
			public function eliminarPersona()
			{
				$idPersona = $this->input->post("idPersona");
				$data = $this->Persona_model->eliminarPersona($idPersona);
				echo json_encode(
					array(
						'data' => $data
						)
					);
				}
				
				public function obtenerPersonas()
				{
					$data = $this->Persona_model->obtenerPersonas();
					echo json_encode(
						array(
        'data' => $data
      )
    );
  }  
}
