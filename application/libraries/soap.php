<?php
class soap
{
    private $client=null;
    private $resultado=array();
    private $configSoap=array();

    function __construct() {
        $CI = &get_instance();
        $CI->config->load('soap_cucuta');
        $this->configSoap=$CI->config->item('soap_cucuta');
        $this->client = new SoapClient($this->configSoap["host"]);
    }

    function query($sql){

        $params = array(
            "baseDatos" => $this->configSoap["baseDatos"],
            "usuario" => $this->configSoap["usuario"],
            "password" => $this->configSoap["password"],
            "schema" => $this->configSoap["schema"],
            "sql" => $sql
        );

        if(stripos($sql, "INSERT") || stripos($sql, "UPDATE") || stripos($sql, "DELETE")){
            $response = $this->client->__soapCall("query", array($params));
            $this->resultado=json_decode($response->return, true);

        }else {
            $response = $this->client->__soapCall("consultar", array($params));
            $this->resultado=json_decode($response->return, true);
        }

        return $this;
    }

    function result_array(){
        if($this->resultado["success"]==false){
            return array();
        }//var_dump($this->resultado);
        return $this->resultado["data"];
    }
    function free_result(){
        $this->resultado=array();
    }
    function close(){
        $this->resultado=array();
    }
}
