<?php

require_once APPPATH . '/third_party/tcpdf/tcpdf.php';
require_once APPPATH . '/third_party/fpdi/fpdi.php';

class Pdf_Proveedores extends FPDI {

    private $fechaInicio;
    private $fechaFin;
    private $usuario;
    private $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    private $meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");

    function __construct() {
        parent::__construct();
    }

    public function setFechas($fechaInicio_, $fechaFin_) {
        $this->fechaInicio = $fechaInicio_;
        $this->fechaFin = $fechaFin_;
    }

    public function setUsuario($usuario_) {
        $this->usuario = $usuario_;
    }

    // Cabecera de página
    function Header() {
        $this->Image(BASEPATH . '../assets/img/logo3.png', 10, 10, 53, 10,
                'PNG', '', '', false, 150, '', false, false, 1, false, false,
                false);
        $this->SetFont("helvetica", "B", 10);
        $this->Cell(0, 4, '', '', 0, 'R', 0, '', 0);
        $this->Ln();
        $this->Ln();
        $this->Cell(0, 4, 'LISTADO DE PROVEEDORES        ', '', 0, 'R', 0, '', 0);
        $this->Ln();
        $this->SetFont("helvetica", "B", 10);
        $this->Cell(0, 4, 'APROBADOS        ', '', 0, 'R', 0, '', 0);
        $this->Ln();
        $fechaInicio_ = new DateTime($this->fechaInicio);
        $fechaInicio_ = $fechaInicio_->format('d/m/Y');
        $fechaFin_ = new DateTime($this->fechaFin);
        $fechaFin_ = $fechaFin_->format('d/m/Y');
        $this->SetFont("helvetica", "", 7);
        $this->Cell(120, 1, '', '', 0, 'C', 0, '', 0);
        $this->Cell(30, 4, 'DEL ' . $fechaInicio_ . ' AL ' . $fechaFin_, '', 0,
                'C', 0, '', 0);
        $this->SetFont("helvetica", "B", 5);
        $html = '<br><br><hr style="width:750px;"></hr>';
        $this->writeHTML($html, false, false, false, false, 'L');
        $this->Cell(0, 4,
                'FG-F-06 Version 2 Vigente desde 22-06-2015 / Page ' . $this->PageNo() . ' de ' . $this->getAliasNbPages() . '      ',
                '', 0, 'R', 0, '', 0);
//        $html = '<div>FG-F-06 Version 2 Vigente desde 22-06-2015 Pag.' . $this->PageNo() . '</div>';
//        $this->writeHTML($html, true, false, false, false, 'R');
    }

    function Footer() {
        $this->SetY(-15);
        $this->SetFont("helvetica", "", 7);
//        $fecha = new DateTime(date("Y-m-d"));
//        $fecha = $fecha->format('D, j, M, Y');
        $fecha = $this->dias[date('w')].", ".date('d').", ".$this->meses[date('n')-1]. ", ".date('Y') ;
        $html = '<p> <strong>Usuario Generador: </strong>' . strtoupper($this->usuario) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Fecha de Generación  del Listado: </strong> '.$fecha.'</p>';
        $this->writeHTML($html, false, false, false, false, 'L');
    }

}
